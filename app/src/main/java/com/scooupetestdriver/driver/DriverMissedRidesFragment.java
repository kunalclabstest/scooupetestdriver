package com.scooupetestdriver.driver;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetestdriver.driver.datastructure.MissedRideInfo;
import com.scooupetestdriver.driver.retrofit.RestClient;
import com.scooupetestdriver.driver.utils.AppStatus;
import com.scooupetestdriver.driver.utils.DateOperations;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class DriverMissedRidesFragment extends Fragment {

	ProgressBar progressBar;
	TextView textViewInfoDisplay;
	ListView listView;
	
	DriverMissedRidesListAdapter driverMissedRidesListAdapter;
	
	RelativeLayout main;

	ArrayList<MissedRideInfo> missedRideInfos = new ArrayList<MissedRideInfo>();
	

	
	public DriverMissedRidesFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		missedRideInfos.clear();
		View rootView = inflater.inflate(R.layout.fragment_list, container, false);

		main = (RelativeLayout) rootView.findViewById(R.id.main);
		main.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		ASSL.DoMagic(main);

		progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
		textViewInfoDisplay = (TextView) rootView.findViewById(R.id.textViewInfoDisplay); textViewInfoDisplay.setTypeface(Data.getFont(getActivity()));
		listView = (ListView) rootView.findViewById(R.id.listView);
		
		driverMissedRidesListAdapter = new DriverMissedRidesListAdapter();
		listView.setAdapter(driverMissedRidesListAdapter);
		
		progressBar.setVisibility(View.GONE);
		textViewInfoDisplay.setVisibility(View.GONE);
		
		textViewInfoDisplay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getMissedRidesAsync(getActivity());
			}
		});
		
		getMissedRidesAsync(getActivity());

		return rootView;
	}

	public void updateListData(String message, boolean errorOccurred){
		if(errorOccurred){
			textViewInfoDisplay.setText(message);
			textViewInfoDisplay.setVisibility(View.VISIBLE);
			
			missedRideInfos.clear();
			driverMissedRidesListAdapter.notifyDataSetChanged();
		}
		else{
			if(missedRideInfos.size() == 0){
				textViewInfoDisplay.setText(message);
				textViewInfoDisplay.setVisibility(View.VISIBLE);
			}
			else{
				textViewInfoDisplay.setVisibility(View.GONE);
			}
			driverMissedRidesListAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public void onDestroy() {

		super.onDestroy();
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	class ViewHolderDriverMissedRides {
		TextView textViewMissedRideFrom, textViewMissedRideFromValue, textViewMissedRideTime, textViewMissedRideCustomerName, textViewMissedRideCustomerNameValue,
				textViewMissedRideCustomerDistance, textViewMissedRideCustomerDistanceValue;
		LinearLayout relative;
		int id;
	}

	class DriverMissedRidesListAdapter extends BaseAdapter {
		LayoutInflater mInflater;
		ViewHolderDriverMissedRides holder;
		public DriverMissedRidesListAdapter() {
			mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return missedRideInfos.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				holder = new ViewHolderDriverMissedRides();
				convertView = mInflater.inflate(R.layout.list_item_missed_rides, null);
				
				holder.textViewMissedRideFrom = (TextView) convertView.findViewById(R.id.textViewMissedRideFrom); holder.textViewMissedRideFrom.setTypeface(Data.getFont(getActivity()), Typeface.BOLD);
				holder.textViewMissedRideFromValue = (TextView) convertView.findViewById(R.id.textViewMissedRideFromValue); holder.textViewMissedRideFromValue.setTypeface(Data.getFont(getActivity()));
				
				holder.textViewMissedRideTime = (TextView) convertView.findViewById(R.id.textViewMissedRideTime); holder.textViewMissedRideTime.setTypeface(Data.getFont(getActivity()));
				
				holder.textViewMissedRideCustomerName = (TextView) convertView.findViewById(R.id.textViewMissedRideCustomerName); holder.textViewMissedRideCustomerName.setTypeface(Data.getFont(getActivity()), Typeface.BOLD);
				holder.textViewMissedRideCustomerNameValue = (TextView) convertView.findViewById(R.id.textViewMissedRideCustomerNameValue); holder.textViewMissedRideCustomerNameValue.setTypeface(Data.getFont(getActivity()));
				
				holder.textViewMissedRideCustomerDistance = (TextView) convertView.findViewById(R.id.textViewMissedRideCustomerDistance); holder.textViewMissedRideCustomerDistance.setTypeface(Data.getFont(getActivity()), Typeface.BOLD);
				holder.textViewMissedRideCustomerDistanceValue = (TextView) convertView.findViewById(R.id.textViewMissedRideCustomerDistanceValue); holder.textViewMissedRideCustomerDistanceValue.setTypeface(Data.getFont(getActivity()));
				
				
				
				holder.relative = (LinearLayout) convertView.findViewById(R.id.relative); 
				
				holder.relative.setTag(holder);
				
				holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
				ASSL.DoMagic(holder.relative);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolderDriverMissedRides) convertView.getTag();
			}
			
			
			MissedRideInfo missedRideInfo = missedRideInfos.get(position);
			
			holder.id = position;
			
			holder.textViewMissedRideFromValue.setText(missedRideInfo.pickupLocationAddress);
			holder.textViewMissedRideTime.setText(DateOperations.convertDate(DateOperations.utcToLocal(missedRideInfo.timestamp)));
			
			holder.textViewMissedRideCustomerNameValue.setText(missedRideInfo.customerName);
			if (missedRideInfo.customerDistance.equalsIgnoreCase("1")) {
				holder.textViewMissedRideCustomerDistanceValue.setText(missedRideInfo.customerDistance+" "+getString(R.string.distance_unit));
			} else {
				holder.textViewMissedRideCustomerDistanceValue.setText(missedRideInfo.customerDistance+" "+getString(R.string.distance_unit)+"s");
			}
			return convertView;
		}
	}
	


	/**
	 * ASync for get rides from server
	 */
	public void getMissedRidesAsync(final Activity activity) {

			if (AppStatus.getInstance(activity).isOnline(activity)) {
				progressBar.setVisibility(View.VISIBLE);
				textViewInfoDisplay.setVisibility(View.GONE);

				RestClient.getApiService().getMissedRidesAsync(Data.userData.accessToken, new Callback<String>() {
					@Override
					public void success(String responseString, Response response) {
						Log.e("Server response", "response = " + response);
						try {
							JSONObject jObj = new JSONObject(responseString);
							if(!jObj.isNull("error")){
								String errorMessage = jObj.getString("error");
								if(Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())){
									HomeActivity.logoutUser(activity);
								}
								else{
									updateListData("Some error occurred. Tap to retry", true);
								}
							}
							else{


								JSONArray missedRidesData = jObj.getJSONArray("missed_rides");
								missedRideInfos.clear();
								DecimalFormat decimalFormat = new DecimalFormat("#.#");
								DecimalFormatSymbols custom=new DecimalFormatSymbols();
								custom.setDecimalSeparator('.');
								decimalFormat.setDecimalFormatSymbols(custom);

								if(missedRidesData.length() > 0){
									for(int i=missedRidesData.length()-1; i>=0; i--){
										JSONObject rideData = missedRidesData.getJSONObject(i);
										Log.e("rideData"+i, "="+rideData);
										double distance=(rideData.getDouble("distance")*1000)*Double.parseDouble(getString(R.string.distance_conversion_factor));
										if(rideData.has("user_name")){
											missedRideInfos.add(new MissedRideInfo(rideData.getString("engagement_id"),
													rideData.getString("pickup_location_address"),
													rideData.getString("timestamp"),
													rideData.getString("user_name"),
													decimalFormat.format(distance)));
										}
										else{
											missedRideInfos.add(new MissedRideInfo(rideData.getString("engagement_id"),
													rideData.getString("pickup_location_address"),
													rideData.getString("timestamp"),
													"",
													decimalFormat.format(distance)));
										}
									}
								}
								updateListData("No missed rides currently", false);
							}
						}  catch (Exception exception) {
							exception.printStackTrace();
							updateListData("Some error occurred. Tap to retry", true);
						}
						progressBar.setVisibility(View.GONE);
					}

					@Override
					public void failure(RetrofitError error) {
						Log.e("request fail", error.toString());
						progressBar.setVisibility(View.GONE);
						updateListData("Some error occurred. Tap to retry", true);
					}
				});
			}
			else {
				updateListData("No Internet connection. Tap to retry", true);
			}

	}


}
