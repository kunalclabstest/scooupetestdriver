package com.scooupetestdriver.driver;

public interface AppInterruptHandler {
	
	public void onNewRideRequest();
	
	public void onCancelRideRequest(String engagementId, boolean acceptedByOtherDriver);
	
	public void onRideRequestTimeout(String engagementId);
	
	public void onChangeStatePushReceived();

	public void forceStateChange();
	
	
}
