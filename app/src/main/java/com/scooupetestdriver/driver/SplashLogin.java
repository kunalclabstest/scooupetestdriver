package com.scooupetestdriver.driver;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.scooupetestdriver.driver.customlayouts.DialogPopup;
import com.scooupetestdriver.driver.datastructure.ApiResponseFlags;
import com.scooupetestdriver.driver.locationfiles.DriverLocationDispatcher;
import com.scooupetestdriver.driver.locationfiles.LocationFetcher;
import com.scooupetestdriver.driver.locationfiles.LocationUpdate;
import com.scooupetestdriver.driver.retrofit.RestClient;
import com.scooupetestdriver.driver.utils.AppStatus;
import com.scooupetestdriver.driver.utils.BaseActivity;
import com.scooupetestdriver.driver.utils.DeviceTokenGenerator;
import com.scooupetestdriver.driver.utils.FlurryEventLogger;
import com.scooupetestdriver.driver.utils.HttpRequester;
import com.scooupetestdriver.driver.utils.IDeviceTokenReceiver;
import com.scooupetestdriver.driver.utils.Log;
import com.scooupetestdriver.driver.utils.Utils;
import com.scooupetestdriver.driver.utils.Validation;
import com.scooupetestdriver.driver.utils.Validation.TextType;
import com.facebook.Session;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class SplashLogin extends BaseActivity implements LocationUpdate{
	
	TextView title;
	Button backBtn;
	AutoCompleteTextView emailEt;
	EditText passwordEt;
	Button signInBtn, forgotPasswordBtn, facebookSignInBtn;
	TextView extraTextForScroll;
	TextView orText;

	static String emailFromForgotPassword="";
	
	
	
	LinearLayout relative;
	
	boolean loginDataFetched = false, facebookRegister = false, sendToOtpScreen = false;
	int otpFlag = 0; String phoneNoOfLoginAccount = "";
	

	
	public static boolean isSystemPackage(PackageInfo pkgInfo) {
		return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
				: false;
	}
	
	
	
	
	String enteredEmail = "";
	

	
	public void resetFlags(){
		loginDataFetched = false;
		facebookRegister = false;
		sendToOtpScreen = false;
		otpFlag = 0;
	}
	
	// *****************************Used for flurry work***************//
	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
		FlurryAgent.onEvent("Login started");
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_login);
		
		resetFlags();
		
		enteredEmail = "";
		
		relative = (LinearLayout) findViewById(R.id.relative);
		new ASSL(SplashLogin.this, relative, 1134, 720, false);
		
		title = (TextView) findViewById(R.id.title); title.setTypeface(Data.getFont(getApplicationContext()));
		backBtn = (Button) findViewById(R.id.backBtn); backBtn.setTypeface(Data.getFont(getApplicationContext()));
		
		emailEt = (AutoCompleteTextView) findViewById(R.id.emailEt); emailEt.setTypeface(Data.getFont(getApplicationContext()));
		passwordEt = (EditText) findViewById(R.id.passwordEt); passwordEt.setTypeface(Data.getFont(getApplicationContext()));
		
		signInBtn = (Button) findViewById(R.id.signInBtn); signInBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
		forgotPasswordBtn = (Button) findViewById(R.id.forgotPasswordBtn); forgotPasswordBtn.setTypeface(Data.getFont(getApplicationContext()),Typeface.BOLD);
		facebookSignInBtn = (Button) findViewById(R.id.facebookSignInBtn); facebookSignInBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
		
		extraTextForScroll = (TextView) findViewById(R.id.extraTextForScroll);
		
		orText = (TextView) findViewById(R.id.orText); orText.setTypeface(Data.getFont(getApplicationContext()));
		

		Validation validation = new Validation(); 
		validation.setValidationFilter(TextType.Password, passwordEt);
		
		
		String[] emails = Database.getInstance(this).getEmails();
		Database.getInstance(this).close();
		
		Database2.getInstance(SplashLogin.this).updateDriverServiceFast("no");
		Database2.getInstance(SplashLogin.this).close();
		
		ArrayAdapter<String> adapter;
		
		if (emails == null) {																			// if emails from database are not null
			emails = new String[]{};
			adapter = new ArrayAdapter<String>(this, R.layout.dropdown_textview, emails);
		} else {																						// else reinitializing emails to be empty
			adapter = new ArrayAdapter<String>(this, R.layout.dropdown_textview, emails);
		}
		
		adapter.setDropDownViewResource(R.layout.dropdown_textview);					// setting email array to EditText DropDown list
		emailEt.setAdapter(adapter);
		emailEt.setText(emailFromForgotPassword);
		emailEt.clearFocus();
		emailFromForgotPassword="";
		
		
		
		signInBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String email = emailEt.getText().toString().trim();
				String password = passwordEt.getText().toString().trim();
				if("".equalsIgnoreCase(email)){
					emailEt.requestFocus();
					emailEt.setError("Please enter email");
				}
				else{
					if("".equalsIgnoreCase(password)){
						passwordEt.requestFocus();
						passwordEt.setError("Please enter password");
					}
					else{
						if(isEmailValid(email)){
							Utils.hideSoftKeyboard(SplashLogin.this, emailEt);
							enteredEmail = email;
							sendLoginValues(SplashLogin.this, email, password);
							FlurryEventLogger.emailLoginClicked(email);
						}
						else{
							emailEt.requestFocus();
							emailEt.setError("Please enter valid email");
						}
					}
				}
			}
		});
		
		
		
		
		forgotPasswordBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ForgotPasswordScreen.emailAlready = emailEt.getText().toString();
				startActivity(new Intent(SplashLogin.this, ForgotPasswordScreen.class));
				overridePendingTransition(R.anim.right_in, R.anim.right_out);
				finish();
			}
		});
		
		backBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				performBackPressed();
			}
		});

		passwordEt.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
				int result = actionId & EditorInfo.IME_MASK_ACTION;
				switch (result) {
					case EditorInfo.IME_ACTION_DONE:
						signInBtn.performClick();
						break;

					case EditorInfo.IME_ACTION_NEXT:
						break;

					default:
				}
				return true;
			}
		});



		final View activityRootView = findViewById(R.id.mainLinear);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {

					@Override
					public void onGlobalLayout() {
						Rect r = new Rect();
						// r will be populated with the coordinates of your view
						// that area still visible.
						activityRootView.getWindowVisibleDisplayFrame(r);

						int heightDiff = activityRootView.getRootView()
								.getHeight() - (r.bottom - r.top);
						if (heightDiff > 100) { // if more than 100 pixels, its
												// probably a keyboard...

							/************** Adapter for the parent List *************/

							ViewGroup.LayoutParams params_12 = extraTextForScroll
									.getLayoutParams();

							params_12.height = (int)(heightDiff);

							extraTextForScroll.setLayoutParams(params_12);
							extraTextForScroll.requestLayout();

						} else {

							ViewGroup.LayoutParams params = extraTextForScroll
									.getLayoutParams();
							params.height = 0;
							extraTextForScroll.setLayoutParams(params);
							extraTextForScroll.requestLayout();

						}
					}
				});
		
		
		
		try {																						// to get AppVersion, OS version, country code and device name
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			Data.appVersion = pInfo.versionCode;
			Log.i("appVersion", Data.appVersion + "..");
			Data.osVersion = android.os.Build.VERSION.RELEASE;
			Log.i("osVersion", Data.osVersion + "..");
			Data.country = getApplicationContext().getResources().getConfiguration().locale.getCountry();
			Log.i("countryCode", Data.country + "..");
			Data.deviceName = (android.os.Build.MANUFACTURER + android.os.Build.MODEL).toString();
			Log.i("deviceName", Data.deviceName + "..");
		} catch (Exception e) {
			Log.e("error in fetching appversion and gcm key", ".." + e.toString());
		}
		
		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		
		try {
			if(getIntent().hasExtra("back_from_otp") && !RegisterScreen.facebookLogin){
				emailEt.setText(OTPConfirmScreen.emailRegisterData.emailId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		new DeviceTokenGenerator(this).generateDeviceToken(this, new IDeviceTokenReceiver() {
			
			@Override
			public void deviceTokenReceived(final String regId) {
				Data.deviceToken = regId;
				Log.e("deviceToken in IDeviceTokenReceiver", Data.deviceToken + "..");
			}
		});
		
		
	}
	

	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(Data.locationFetcher == null){
			Data.locationFetcher = new LocationFetcher(SplashLogin.this, 1000, 1);
		}
		
		
		int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
		if(resp != ConnectionResult.SUCCESS){
			Log.e("Google Play Service Error ","="+resp);
			new DialogPopup().showGooglePlayErrorAlert(SplashLogin.this);
		}
		else{
			new DialogPopup().showLocationSettingsAlert(SplashLogin.this);
		}
		
	}
	
	@Override
	protected void onPause() {
		try{
			if(Data.locationFetcher != null){
				Data.locationFetcher.destroy();
				Data.locationFetcher = null;
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		super.onPause();
		
	}
	
	
	@Override
	public void onBackPressed() {
		performBackPressed();
		super.onBackPressed();
	}
	
	
	public void performBackPressed(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					if(Session.getActiveSession() != null){
						Session.getActiveSession().closeAndClearTokenInformation();
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}).start();
		Intent intent = new Intent(SplashLogin.this, SplashNewActivity.class);
		intent.putExtra("no_anim", "yes");
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
	}

	
	
	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}
	
	
	/**
	 * ASync for login from server
	 */
	public void sendLoginValues(final Activity activity, final String emailId, final String password) {
		if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
			resetFlags();
			DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
			

		
			if(Data.locationFetcher != null){
				Data.latitude = Data.locationFetcher.getLatitude();
				Data.longitude = Data.locationFetcher.getLongitude();
			}
			Log.i("Server uRL", "=" + Data.SERVER_URL);
			Log.i("email", "=" + emailId);
			Log.i("password", "=" + password);
			Log.e("device_token", "=" + Data.deviceToken);
			Log.i("latitude", "=" + Data.latitude);
			Log.i("longitude", "=" + Data.longitude);
			Log.i("country", "=" + Data.country);
			Log.i("device_name", "=" + Data.deviceName);
			Log.i("app_version", "=" + Data.appVersion);
			Log.i("os_version", "=" + Data.osVersion);
			Log.i("unique_device_id", "=" + Data.uniqueDeviceId);
			RestClient.getApiService().sendLoginValues(emailId, password, Data.DEVICE_TYPE, Data.uniqueDeviceId, Data.deviceToken,
					""+Data.latitude,""+ Data.longitude, Data.country, Data.deviceName, Data.appVersion, Data.osVersion, new Callback<String>() {
						@Override
						public void success(String responseString, Response response) {
							Log.i("Server response", "response = " + response);
							try {
								JSONObject jObj = new JSONObject(responseString);
//								boolean newUpdate = SplashNewActivity.checkIfUpdate(jObj, activity);
//								if(!newUpdate){
								int flag = jObj.getInt("flag");
								if(ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag){
									DialogPopup.dismissLoadingDialog();
									HomeActivity.logoutUser(activity);

								}
								else if(ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag){
									String errorMessage = jObj.getString("error");
									DialogPopup.dismissLoadingDialog();
									new DialogPopup().alertPopup(activity, "", errorMessage);

								}
								else if(ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag){
									DialogPopup.dismissLoadingDialog();
									String message = jObj.getString("message");
									new DialogPopup().alertPopup(activity, "", message);
								}
								else if(ApiResponseFlags.INCORRECT_PASSWORD.getOrdinal() == flag){
									DialogPopup.dismissLoadingDialog();
									String errorMessage = jObj.getString("error");
									new DialogPopup().alertPopup(activity, "", errorMessage);
								}
								else if(ApiResponseFlags.CUSTOMER_LOGGING_IN.getOrdinal() == flag){
									DialogPopup.dismissLoadingDialog();
									String errorMessage = jObj.getString("error");
									new DialogPopup().alertPopup(activity, "", errorMessage);
//										SplashNewActivity.sendToCustomerAppPopup("Alert", errorMessage, activity);
								}
								else if(ApiResponseFlags.LOGIN_SUCCESSFUL.getOrdinal() == flag){
									new JSONParser().parseLoginData(activity, responseString);
									Database.getInstance(SplashLogin.this).insertEmail(emailId);
									Database.getInstance(SplashLogin.this).close();
									accessTokenLogin(activity);
//										loginDataFetched = true;
								}else if(ApiResponseFlags.SENT_OTP_ERROR.getOrdinal() == flag){
									phoneNoOfLoginAccount = (jObj.has("phone_no"))?(jObj.getString("phone_no")):"";
									otpFlag = 0;
									sendIntentToOtpScreen();
//                                        sendToOtpScreen = true;

//                                        new DialogPopup().alertPopup(activity, "", jObj.getString("error"));
									DialogPopup.dismissLoadingDialog();
								}
								else{
									String errorMessage=Data.SERVER_ERROR_MSG;
									try {
										errorMessage = jObj.getString("error");
									}catch(Exception e){
										errorMessage =  Data.SERVER_ERROR_MSG;
									}

									new DialogPopup().alertPopup(activity, "", errorMessage);
									DialogPopup.dismissLoadingDialog();
								}
//								}
							}  catch (Exception exception) {
								exception.printStackTrace();
								new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
								DialogPopup.dismissLoadingDialog();
							}

						}

						@Override
						public void failure(RetrofitError error) {
							Log.e("request fail email login", error.toString());
							DialogPopup.dismissLoadingDialog();
							new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
						}
					});
		}
		else {
			new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
		}

	}


	
	/**
	 * Send intent to otp screen by making required data objects
	 *  flag 0 for email, 1 for Facebook
	 */
	public void sendIntentToOtpScreen(){
		if(0 == otpFlag){
			RegisterScreen.facebookLogin = false;
			OTPConfirmScreen.intentFromRegister = false;
			OTPConfirmScreen.emailRegisterData = new EmailRegisterData("", enteredEmail, phoneNoOfLoginAccount, "", "","");
			startActivity(new Intent(SplashLogin.this, OTPConfirmScreen.class));
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.right_out);
		}
		else if(1 == otpFlag){
			RegisterScreen.facebookLogin = true;
			OTPConfirmScreen.intentFromRegister = false;
			OTPConfirmScreen.facebookRegisterData = new FacebookRegisterData(phoneNoOfLoginAccount, "", "");
			startActivity(new Intent(SplashLogin.this, OTPConfirmScreen.class));
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.right_out);
		}
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			super.onActivityResult(requestCode, resultCode, data);
			Session.getActiveSession().onActivityResult(this, requestCode,
					resultCode, data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Log.i("Data.isOpenGooglePlay", "=="+Data.isOpenGooglePlay);
		if(hasFocus && Data.isOpenGooglePlay)
		{
			Data.isOpenGooglePlay=false;
		}
		else if(hasFocus && loginDataFetched){
			
			Database2.getInstance(SplashLogin.this).updateDriverLastLocationTime();
			Database2.getInstance(SplashLogin.this).close();
	        
			Map<String, String> articleParams = new HashMap<String, String>();
			articleParams.put("username", Data.userData.userName);
			FlurryAgent.logEvent("App Login", articleParams);
			
			loginDataFetched = false;

			startActivity(new Intent(SplashLogin.this, HomeActivity.class));
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.right_out);
		}
		else if(hasFocus && facebookRegister){
			facebookRegister = false;
			RegisterScreen.facebookLogin = true;
			startActivity(new Intent(SplashLogin.this, RegisterScreen.class));
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.right_out);
		}
		else if(hasFocus && sendToOtpScreen){
			sendIntentToOtpScreen();
		}
		
	}

	
	@Override
	protected void onDestroy() {
		super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
	}
	

	@Override
	public void onLocationChanged(Location location, int priority) {
		Data.latitude = location.getLatitude();
		Data.longitude = location.getLongitude();
		new DriverLocationDispatcher().saveLocationToDatabase(SplashLogin.this, location);
	}
	
	
	
	/**
	 * ASync for access token login from server
	 */
	public void accessTokenLogin(final Activity activity) {
		
		SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
		final String accessToken = pref.getString(Data.SP_ACCESS_TOKEN_KEY, "");
		Log.i("accessToken", "=="+accessToken);
		if(!"".equalsIgnoreCase(accessToken)){
			if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
				
//				DialogPopup.showLoadingDialog(activity, "Loading...");
				
				if(Data.locationFetcher != null){
					Data.latitude = Data.locationFetcher.getLatitude();
					Data.longitude = Data.locationFetcher.getLongitude();
				}

				Double latitude=0.0,longitude=0.0;
				final String serviceRestartOnReboot = Database2.getInstance(activity).getDriverServiceRun();
				if(!Database2.NO.equalsIgnoreCase(serviceRestartOnReboot)){
					latitude=Data.latitude;
					longitude=Data.longitude;
				}
				Database2.getInstance(activity).close();

				Log.i("accessToken", "=" + accessToken);
				Log.i("device_token", Data.deviceToken);
				Log.i("latitude", ""+latitude);
				Log.i("longitude", ""+longitude);
				Log.i("app_version", ""+Data.appVersion);
				Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

				RestClient.getApiService().accessTokenLogin(accessToken, Data.deviceToken, latitude, longitude, Data.appVersion, Data.DEVICE_TYPE, Data.uniqueDeviceId, new Callback<String>() {
					@Override
					public void success(String responseString, Response response) {
						Log.e("Server response of access_token", "response = " + response);

						try {
							JSONObject jObj = new JSONObject(responseString);
							boolean newUpdate=false;
							if(jObj.has("login"))
							{
								newUpdate = SplashNewActivity.checkIfUpdate(jObj.getJSONObject("login"), activity);
							}
							if(!newUpdate){
								int flag = jObj.getInt("flag");
								if(ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag){
									DialogPopup.dismissLoadingDialog();
									HomeActivity.logoutUser(activity);
								}
								else if(ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag){
									DialogPopup.dismissLoadingDialog();
									String errorMessage = jObj.getString("error");
									new DialogPopup().alertPopup(activity, "", errorMessage);
								}
								else if(ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag){
									DialogPopup.dismissLoadingDialog();
									String message = jObj.getString("message");
									new DialogPopup().alertPopup(activity, "", message);
								}
								else if(ApiResponseFlags.LOGIN_SUCCESSFUL.getOrdinal() == flag){
									new AccessTokenDataParseAsync(activity, responseString, accessToken).execute();
								}
								else{
									DialogPopup.dismissLoadingDialog();
									new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
								}
							}
							else{
								DialogPopup.dismissLoadingDialog();
							}
						}  catch (Exception exception) {
							exception.printStackTrace();
							new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
							DialogPopup.dismissLoadingDialog();
						}

					}

					@Override
					public void failure(RetrofitError error) {
						Log.e("request fail via access token", error.toString());
						DialogPopup.dismissLoadingDialog();
						new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
						DialogPopup.dismissLoadingDialog();
					}
				});
			}
			else {
				new DialogPopup().alertPopup(activity, "",getString(R.string.check_internet_message));
			}
		}

	}
	
	
	class AccessTokenDataParseAsync extends AsyncTask<String, Integer, String>{
		
		Activity activity;
		String response, accessToken;
		
		public AccessTokenDataParseAsync(Activity activity, String response, String accessToken){
			this.activity = activity;
			this.response = response;
			this.accessToken = accessToken;
		}
		
		@Override
		protected String doInBackground(String... params) {
			try {
				String resp = new JSONParser().parseAccessTokenLoginData(activity, response, accessToken);
				return resp;
			} catch (Exception e) {
				e.printStackTrace();
				return HttpRequester.SERVER_TIMEOUT;
			}
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if(result.contains(HttpRequester.SERVER_TIMEOUT)){
				loginDataFetched = false;
				new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
			}
			else{

				
				loginDataFetched = true;
				
			}
			

			DialogPopup.dismissLoadingDialog();
		}
		
	}

}
