package com.scooupetestdriver.driver.facebook;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.scooupetestdriver.driver.Data;
import com.scooupetestdriver.driver.R;
import com.scooupetestdriver.driver.customlayouts.DialogPopup;
import com.scooupetestdriver.driver.utils.AppStatus;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.Builder;
import com.facebook.widget.WebDialog.OnCompleteListener;

import java.util.Arrays;

public class FacebookLoginHelper {

	private static Session session;

	
	public void openFacebookSession(final Activity activity, final FacebookLoginCallback facebookLoginCallback,
			final boolean fetchFBData){
		if (!AppStatus.getInstance(activity).isOnline(activity)) {
			new DialogPopup().alertPopup(activity, "", activity.getString(R.string.check_internet_message));
		} else {
			session = Session.getActiveSession();
			Log.i("session", "="+session);
			if(session == null){
				callOpenActiveSession(activity, facebookLoginCallback, fetchFBData);
			}
			else{
				if(session.getState() == SessionState.OPENED || session.getState() == SessionState.OPENED_TOKEN_UPDATED){
					callRequestMeAsync(session, activity, facebookLoginCallback, fetchFBData);
				}
				else{
					Session.setActiveSession(session);	
					session.closeAndClearTokenInformation();	
					callOpenActiveSession(activity, facebookLoginCallback, fetchFBData);
				}
			}
			
			
		}
	
	}
	
	
	public void callOpenActiveSession(final Activity activity, final FacebookLoginCallback facebookLoginCallback, 
			final boolean fetchFBData){
		Session.openActiveSession(activity, true, new Session.StatusCallback() {
			@Override
			public void call(final Session session, SessionState state, Exception exception) {
				if(session.isOpened()){
					FacebookLoginHelper.session = session;
					Session.setActiveSession(session);
					callRequestMeAsync(session, activity, facebookLoginCallback, fetchFBData);
				}
			}
		});
	}
	
	public void callRequestMeAsync(Session session, final Activity activity, final FacebookLoginCallback facebookLoginCallback, 
			final boolean fetchFBData){
		if(fetchFBData){
			Data.fbAccessToken = session.getAccessToken();
			Log.e("fbAccessToken===", "="+Data.fbAccessToken);
			DialogPopup.showLoadingDialog(activity, activity.getString(R.string.loading));
			Request.newMeRequest(session, new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) { // fetching user data from FaceBook
						DialogPopup.dismissLoadingDialog();
						if (user != null) {
							Log.i("res", "="+response);
							Log.i("user", "=" + user);
							
							Data.fbId = user.getId();
							Data.fbFirstName = user.getFirstName();
							Data.fbLastName = user.getLastName();
							Data.fbUserName = user.getUsername();
							
							try {
								Data.fbUserEmail = ((String)user.asMap().get("email"));
								Log.e("Data.userEmail before","="+Data.fbUserEmail);
							} catch (Exception e2) {
								e2.printStackTrace();
							}
							finally{
								if(Data.fbUserEmail == null || "".equalsIgnoreCase(Data.fbUserEmail)){
									if(Data.fbUserName != null && !"".equalsIgnoreCase(Data.fbUserName)){
										Data.fbUserEmail = Data.fbUserName + "@facebook.com";
									}
									else{
										Data.fbUserEmail = Data.fbId + "@facebook.com";
									}
								}
							}
							
							if(Data.fbUserName == null){
								Data.fbUserName = "";
							}
							Log.e("Data.userEmail after","="+Data.fbUserEmail);
						}
						else{
							new DialogPopup().alertPopup(activity, "Facebook Error", "Error in fetching information from Facebook.");
						}
						facebookLoginCallback.facebookLoginDone();
					}
				}).executeAsync();
		}
		else{
			facebookLoginCallback.facebookLoginDone();
		}
	}
}
