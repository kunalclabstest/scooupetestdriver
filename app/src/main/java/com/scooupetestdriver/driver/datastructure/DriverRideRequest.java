package com.scooupetestdriver.driver.datastructure;

import com.google.android.gms.maps.model.LatLng;

public class DriverRideRequest {
	
	public String engagementId, customerId, address, startTime, customerName, customerImage;
	public LatLng latLng;
	public int customerRating;
	public double distance;

	public DriverRideRequest(String engagementId, String customerId, LatLng latLng, String startTime, String address, String customerName,
                             String customerImage, int customerRating, double distance){
		this.engagementId = engagementId;
		this.customerId = customerId;
		this.latLng = latLng;
		this.startTime = startTime;
		this.address = address;
        this.customerName = customerName;
        this.customerImage = customerImage;
        this.customerRating = customerRating;
		this.distance = distance;
	}
	
	public DriverRideRequest(String engagementId,String customerId){
		this.engagementId = engagementId;
		this.customerId =  customerId;
		this.latLng = new LatLng(0, 0);
		this.startTime = "";
		this.address = "";
	}


	@Override
	public String toString() {
		return engagementId + " " + customerId + " " + latLng + " " + startTime;
	}
	
	@Override
	public boolean equals(Object o) {
		try{
			if(((DriverRideRequest)o).engagementId.equalsIgnoreCase(engagementId) || ((DriverRideRequest)o).customerId.equalsIgnoreCase(customerId)){
				return true;
			}
			else{
				return false;
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
}
