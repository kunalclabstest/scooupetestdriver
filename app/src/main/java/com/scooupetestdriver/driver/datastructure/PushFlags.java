package com.scooupetestdriver.driver.datastructure;

public enum PushFlags {
	REQUEST(0), 
	REQUEST_TIMEOUT(1),
	REQUEST_CANCELLED(2),
	RIDE_ACCEPTED_BY_OTHER_DRIVER(6),
	WAITING_STARTED(9),
	WAITING_ENDED(10),
	CHANGE_STATE(20),
	DISPLAY_MESSAGE(21),
    ACTIVATIONPENDING(104),
	FORCE_END_RIDE(400),
	MESSAGE_FROM_ADMIN(500);

	private int ordinal;

	private PushFlags(int ordinal) {
		this.ordinal = ordinal;
	}

	public int getOrdinal() {
		return ordinal;
	}
}
