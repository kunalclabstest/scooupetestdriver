package com.scooupetestdriver.driver.datastructure;

import com.scooupetestdriver.driver.utils.Log;

public class FareStructure {
	public double fixedFare;
	public double thresholdDistance;
	public double farePerKm;
	public double farePerMin;
	public double freeMinutes;
	public double freeWaitMinutes;
	public double waitTimeFarePerMin;
	
	public FareStructure(double fixedFare, double thresholdDistance, double farePerKm, double farePerMin, double waitTimeFarePerMin, double freeMinutes, double freeWaitMinutes){
		this.fixedFare = fixedFare;
		this.thresholdDistance = thresholdDistance;
		this.farePerKm = farePerKm;
		this.farePerMin = farePerMin;
		this.freeMinutes = freeMinutes;
		this.freeWaitMinutes = freeWaitMinutes;
		this.waitTimeFarePerMin = waitTimeFarePerMin;
	}
	
	public double calculateFare(double totalDistanceInKm, double totalTimeInMin, double waitTimeInMin){
		Log.i("freeMinutes","=="+freeMinutes);
		Log.i("farePerMin","=="+farePerMin);
		Log.i("farePerMin","=="+farePerMin);
		Log.i("farePerMin","=="+farePerMin);
		totalTimeInMin = totalTimeInMin - freeMinutes;
		if(totalTimeInMin < 0){
			totalTimeInMin = 0;
		}
		waitTimeInMin = waitTimeInMin - freeWaitMinutes;
		if(waitTimeInMin < 0){
			waitTimeInMin = 0;
		}
		totalDistanceInKm=Math.floor(totalDistanceInKm);
		double fareOfRideTime = totalTimeInMin * farePerMin;
		double fare =  fareOfRideTime +
				fixedFare + ((totalDistanceInKm <= thresholdDistance) ? (0) : ((totalDistanceInKm - thresholdDistance)* farePerKm)) +
				waitTimeInMin* waitTimeFarePerMin;
//		fare = Math.floor(fare);
		return fare;
	}
}
