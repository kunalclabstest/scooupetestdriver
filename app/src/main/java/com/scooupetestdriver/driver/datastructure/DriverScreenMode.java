package com.scooupetestdriver.driver.datastructure;

public enum DriverScreenMode {
	D_INITIAL, D_REQUEST_ACCEPT, D_START_RIDE, D_IN_RIDE , D_RIDE_END, D_SEARCH
}
