package com.scooupetestdriver.driver.datastructure;

public enum ApiResponseFlags {

	INVALID_ACCESS_TOKEN(101),
	SHOW_ERROR_MESSAGE(103),
	SHOW_MESSAGE(104),

    SENT_OTP_ERROR(3),
	REQUEST_TIMEOUT(111),
	ENGAGEMENT_DATA(132),
	ACTIVE_REQUESTS(133),
	DRIVER_END_RIDE_SCREEN(175),

	ACK_RECEIVED(142),
	
	ACTION_COMPLETE(143),
	ACTION_FAILED(144),
	
	LOGIN_SUCCESSFUL(150),
	INCORRECT_PASSWORD(151),
	CUSTOMER_LOGGING_IN(152),
	LOGOUT_SUCCESSFUL(153),
	LOGOUT_FAILURE(154);

	private int ordinal;

	private ApiResponseFlags(int ordinal) {
		this.ordinal = ordinal;
	}

	public int getOrdinal() {
		return ordinal;
	}
}
