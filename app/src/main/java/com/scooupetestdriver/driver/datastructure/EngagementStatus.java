package com.scooupetestdriver.driver.datastructure;

public enum EngagementStatus {

	ACCEPTED(1),
	// request has been accepted by the driver
	STARTED(2),
	// ride has started
	ENDED(3);
	// ride has ended

	private int ordinal;

	private EngagementStatus(int ordinal) {
		this.ordinal = ordinal;
	}

	public int getOrdinal() {
		return ordinal;
	}
}
