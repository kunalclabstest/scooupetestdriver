package com.scooupetestdriver.driver;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scooupetestdriver.driver.datastructure.HelpSection;
import com.scooupetestdriver.driver.retrofit.RestClient;
import com.scooupetestdriver.driver.utils.AppStatus;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class FareDetails extends FragmentActivity{


	LinearLayout relative;

	Button backBtn;
	TextView title;
	ProgressBar progressBar;
	TextView textViewInfo;
	WebView webview;

	public static HelpSection helpSection = HelpSection.FARE_DETAILS;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fare_details);

		relative = (LinearLayout) findViewById(R.id.relative);
		new ASSL(FareDetails.this, relative, 1134, 720, false);


		backBtn = (Button) findViewById(R.id.backBtn);
		title = (TextView) findViewById(R.id.title); title.setTypeface(Data.getFont(getApplicationContext()));

		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		textViewInfo = (TextView) findViewById(R.id.textViewInfo); textViewInfo.setTypeface(Data.getFont(getApplicationContext()));
		webview = (WebView) findViewById(R.id.webview);
		WebSettings webSettings = webview.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webview.getSettings().setDomStorageEnabled(true);
		webview.getSettings().setDatabaseEnabled(true);

		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webview.setWebViewClient(new MyWebViewClient1());
		webview.setWebChromeClient(new MyWebChromeClient());


		if(helpSection != null){
			title.setText(helpSection.getName(getResources()));
		}


		backBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				performBackPressed();
			}
		});


		textViewInfo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				getFareDetailsAsync(FareDetails.this);
			}
		});

		getFareDetailsAsync(FareDetails.this);

	}


	boolean loadingFinished = true;
	boolean redirect = false;
	boolean apiCalling = true;

	class MyWebViewClient1 extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
			if (!loadingFinished) {
				redirect = true;
			}
			loadingFinished = false;
			view.loadUrl(urlNewString);
			Log.e("shouldOverrideUrlLoading", "urlNewString=" + urlNewString);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			loadingFinished = false;
			//SHOW LOADING IF IT ISNT ALREADY VISIBLE
			progressBar.setVisibility(View.VISIBLE);
			Log.e("onPageStarted", "url="+url);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			if(!redirect){
				loadingFinished = true;
			}

			if(loadingFinished && !redirect && !apiCalling){
				//HIDE LOADING IT HAS FINISHED
				progressBar.setVisibility(View.GONE);
			} else{
				redirect = false;
			}
			Log.e("onPageFinished", "url="+url);

		}

//        @Override
//        public void onReceivedSslError(WebView view, SslErrorHandler handler,
//                                       SslError error) {
//            Log.e("onReceivedSslError", "error=" + error);
//            handler.proceed();
//        }

		@Override
		public void onLoadResource(WebView view, String url) {
			Log.i("onLoadResource", "url=" + url);
			super.onLoadResource(view, url);
			//progressBar.setVisibility(View.GONE);
		}


	}


	private class MyWebChromeClient extends WebChromeClient {

		//display alert message in Web View
		@Override
		public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
			Log.e("message", message);
			return false;
		}

		@Override
		public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
			Log.e("consoleMessage", "="+consoleMessage);
			return super.onConsoleMessage(consoleMessage);
		}

		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			Log.i("newProgress", "="+newProgress);
			if(newProgress == 100){
				progressBar.setVisibility(View.GONE);
			}
			super.onProgressChanged(view, newProgress);
		}
	}

	public void openHelpData(String data, boolean errorOccured) {
		if (errorOccured) {
			textViewInfo.setVisibility(View.VISIBLE);
			textViewInfo.setText(data);
			webview.setVisibility(View.GONE);
		} else {
			textViewInfo.setVisibility(View.GONE);
			webview.setVisibility(View.VISIBLE);
			loadHTMLContent(data);
		}
	}

	public void loadHTMLContent(String data){
		final String mimeType = "text/html";
		final String encoding = "UTF-8";
		webview.loadUrl(data);
	}

	/**
	 * ASync for get rides from server
	 */
	public void getFareDetailsAsync(final Activity activity) {

		if (AppStatus.getInstance(activity).isOnline(activity)) {
			if(helpSection != null){
				progressBar.setVisibility(View.VISIBLE);
				textViewInfo.setVisibility(View.GONE);
				webview.setVisibility(View.GONE);
				loadHTMLContent("");

				Log.e("helpSection", "=" + helpSection.getOrdinal() + " " + helpSection.getName(getResources()));
				RestClient.getApiService().getHelpAsync(""+helpSection.getOrdinal(), new Callback<String>() {
					@Override
					public void success(String responseString, Response response) {
						Log.i("Server response faq ", "response = " + responseString);
						try {
							JSONObject jObj = new JSONObject(responseString);
							if(!jObj.isNull("error")){
								String errorMessage = jObj.getString("error");
								if(Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())){
									HomeActivity.logoutUser(activity);
								}
								else{
									openHelpData("Some error occured. Tap to retry.", true);
								}
							}
							else{
								String data = jObj.getString("link");

								openHelpData(data, false);
							}
						}  catch (Exception exception) {
							exception.printStackTrace();
							openHelpData("Some error occured. Tap to retry.", true);
						}
						// progressBar.setVisibility(View.GONE);
					}

					@Override
					public void failure(RetrofitError error) {
						Log.e("request fail", error.toString());
						progressBar.setVisibility(View.GONE);
						openHelpData("Some error occured. Tap to retry.", true);
					}
				});



			}
		}
		else {
			progressBar.setVisibility(View.GONE);
			openHelpData("No internet connection. Tap to retry.", true);
		}

	}


	public void performBackPressed(){

		finish();
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
	}

	@Override
	public void onBackPressed() {
		performBackPressed();
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		ASSL.closeActivity(relative);
		System.gc();
	}

}
