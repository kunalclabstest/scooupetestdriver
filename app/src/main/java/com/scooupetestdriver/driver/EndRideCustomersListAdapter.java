package com.scooupetestdriver.driver;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Locale;

import rmn.androidscreenlibrary.ASSL;


/**
 * BaseAdapter implementation which will inflate the layout "element_example".
 * 
 * 
 * @author Nadeem Khan
 * 
 */
public class EndRideCustomersListAdapter extends BaseAdapter{

        /** The inflator used to inflate the XML layout */
        private LayoutInflater inflator;

        /** A list containing some sample data to show. */
         
         ViewHolder holder = null;
//         static ArrayList<EndRideCustomerInfo> endRideCustomersList = new ArrayList<EndRideCustomerInfo>();
         Activity activity;
        public EndRideCustomersListAdapter(Activity activity) {
                super();
//                this.endRideCustomersList.clear();
//                this.endRideCustomersList.addAll(list);
                inflator = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);;
                this.activity=activity;
   
        }
   
        @Override
        public int getCount() {
                return Data.endRidesCustomerInfo.size();
        }

        @Override
        public Object getItem(int position) {
                return Data.endRidesCustomerInfo.get(position);
        }

        @Override
        public long getItemId(int position) {
                return position; 
        }

        public static class ViewHolder
    	{     
    	       public TextView tvName,tvFare;
    	       LinearLayout parent;
    	      
    	}
        
        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) 
        {
           
                if (view == null) 
                {
                        view = inflator.inflate(R.layout.end_ride_customers_item, null);
                        holder = new ViewHolder();
                        holder.parent = (LinearLayout)view.findViewById(R.id.relative);
                        holder.tvName =(TextView) view.findViewById(R.id.name);
                        holder.tvName.setTypeface(Data.getFont(activity));
                        holder.tvFare =(TextView) view.findViewById(R.id.fare);
                        holder.tvFare.setTypeface(Data.getFont(activity));
                       

                        holder.parent.setLayoutParams(new ListView.LayoutParams(720,
                                LayoutParams.WRAP_CONTENT));

                		ASSL.DoMagic(holder.parent);

                        view.setTag(holder);
                }
                else 
                {
                	holder=(ViewHolder)view.getTag();
                }
            	
                holder.tvName.setText(Data.endRidesCustomerInfo.get(position).name);
                if(Data.endRidesCustomerInfo.get(position).isPaymentSucessfull==0)
                {

//                		holder.tvFare.setText("Amount Paid: "+activity.getResources().getString(R.string.currency_symbol)+String.format("%.2f", Data.endRidesCustomerInfo.get(position).toPay));
                        holder.tvFare.setText(activity.getString(R.string.waiting_for_payment));
                        if(HomeActivity.makeOkButtonEnable){
                                holder.tvFare.setText("Amount Paid: "+activity.getResources().getString(R.string.currency_symbol)+String.format(Locale.US,"%.2f", Data.endRidesCustomerInfo.get(position).toPay));
                        }

                }
                else
                {
                	if(Data.endRidesCustomerInfo.get(position).paymentMethod==0)
                	{
                		holder.tvFare.setText("Paid via cash: "+activity.getResources().getString(R.string.currency_symbol)+String.format(Locale.US,"%.2f", Data.endRidesCustomerInfo.get(position).toPay));
                	}
                	else
                	{
                		holder.tvFare.setText("Amount Paid: "+activity.getResources().getString(R.string.currency_symbol)+String.format(Locale.US,"%.2f", Data.endRidesCustomerInfo.get(position).toPay));
                	}
                }
//                holder.tvFare.setText("Fare: "+activity.getResources().getString(R.string.currency_symbol)+String.format("%.2f", (HomeActivity.totalFare/endRideCustomersList.size())));
                return view;
        }

}