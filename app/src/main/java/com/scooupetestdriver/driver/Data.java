package com.scooupetestdriver.driver;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;

import com.scooupetestdriver.driver.datastructure.CustomerInfo;
import com.scooupetestdriver.driver.datastructure.DriverRideRequest;
import com.scooupetestdriver.driver.datastructure.EndRideCustomerInfo;
import com.scooupetestdriver.driver.datastructure.FareStructure;
import com.scooupetestdriver.driver.datastructure.UserData;
import com.scooupetestdriver.driver.locationfiles.LocationFetcher;
import com.scooupetestdriver.driver.utils.Log;
import com.scooupetestdriver.driver.utils.MySSLSocketFactory;
import com.scooupetestdriver.driver.utils.Prefs;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpClient;

import java.security.KeyStore;
import java.util.ArrayList;

/**
 * Stores common static data for access for all activities across the application
 * @author shankar
 *
 */
public class Data {

	public static final String INVALID_ACCESS_TOKEN = "invalid access token";
	
	public static final String DEBUG_PASSWORD = "3131", REGISTER_PASSWORD = "1485";
	
	public static final String SHARED_PREF_NAME = "myPref", SETTINGS_SHARED_PREF_NAME = "settingsPref";
	public static final String SP_ACCESS_TOKEN_KEY = "access_token",
			
			SP_TOTAL_DISTANCE = "total_distance", 
			SP_WAIT_TIME = "wait_time",
			SP_RIDE_TIME = "ride_time", 
			SP_RIDE_START_TIME = "ride_start_time", 
			SP_LAST_LATITUDE = "last_latitude",
			SP_LAST_LONGITUDE = "last_longitude",
			
			SP_DRIVER_SCREEN_MODE = "driver_screen_mode", 
			
			SP_D_ENGAGEMENT_ID = "d_engagement_id", 
			SP_D_LATITUDE = "d_latitude",
			SP_D_LONGITUDE = "d_longitude",
			SP_D_CUSTOMER_ID = "d_customer_id",
			SP_D_CUSTOMER_NAME = "d_customer_name", 
			SP_D_CUSTOMER_IMAGE = "d_customer_image", 
			SP_D_CUSTOMER_PHONE = "d_customer_phone", 
			SP_D_CUSTOMER_RATING = "d_customer_rating";

	public static final String SP_SELECTED_LANGUAGE = "LANG";
	public static final String SP_SERVER_LINK = "sp_server_link";
	public static Context context;
	
	
	public static String D_START_RIDE = "D_START_RIDE", D_IN_RIDE = "D_IN_RIDE",D_RIDE_END = "D_RIDE_END";

	
	
	public static LatLng startRidePreviousLatLng;

	
	//TODO
	public static final String DEV_SERVER_URL = "http://taximustv1.clicklabs.in:18000"; //http://taxidev3.clicklabs.in:18000";
	public static final String LIVE_SERVER_URL = "http://taximust.clicklabs.in:9001";
	public static final String TRIAL_SERVER_URL = "http://scooupe-taxi.clicklabs.in:3003";
	
	public static final String DEFAULT_SERVER_URL = TRIAL_SERVER_URL;
	
	public static String SERVER_URL = DEFAULT_SERVER_URL;

    public static float DIALOG_DIM_AMOUNT = 0.0f;
	
	
	
	public static final String SERVER_ERROR_MSG = "Connection lost. Please try again later.";
	public static final String SERVER_NOT_RESOPNDING_MSG = "Connection lost. Please try again later.";
	public static final String CHECK_INTERNET_MSG = "Check your internet connection.";


	public static double latitude, longitude;
	
	public static int isWaitStart=0,isArrived=1;


	public static UserData userData;
	
	public static LocationFetcher locationFetcher;
	

	public static final String DEVICE_TYPE = "0";
	public static String deviceToken = "", country = "", deviceName = "", osVersion = "", uniqueDeviceId = "",packageName="";
	public static String phoneNo = "",countryCode="";
	public static String password = "";
	public static String emailId = "",licenceNumber="",paypalId="";
	public static String userName = "", lastName = "";
	public static int appVersion;

	public static String dEngagementId = "", dCustomerId = "";
	public static LatLng dCustLatLng,dDestinationLatLng=null;
	
	public static ArrayList<DriverRideRequest> driverRideRequests = new ArrayList<DriverRideRequest>();
	public static ArrayList<EndRideCustomerInfo > endRidesCustomerInfo=new ArrayList<EndRideCustomerInfo >();
	
	public static CustomerInfo assignedCustomerInfo;
	
	public static boolean isOpenGooglePlay=false;

	
	public static LatLng pickupLatLng;

	public static String fbAccessToken = "", fbId = "", fbFirstName = "", fbLastName = "", fbUserName = "", fbUserEmail = "";

	public static FareStructure fareStructure;
	
	
	
	public static void clearDataOnLogout(Context context){
		try{
			userData = null;
			locationFetcher = null;
			deviceToken = ""; country = ""; deviceName = ""; appVersion = 0; osVersion = "";
			pickupLatLng = null;
			fbAccessToken = ""; fbId = ""; fbFirstName = ""; fbLastName = ""; fbUserName = ""; fbUserEmail = "";
			Prefs.with(context).save("USER_DATA","");
			SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
			Editor editor = pref.edit();
			editor.putString(Data.SP_ACCESS_TOKEN_KEY, "");
			editor.clear();
			editor.commit();
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	
	public static Typeface regular;																// fonts declaration
	

	public static Typeface getFont(Context appContext) {											// accessing fonts functions
		if (regular == null) {
			regular = Typeface.createFromAsset(appContext.getAssets(), "fonts/TitilliumWeb-Regular.ttf");
		}
		return regular;
	}

	
	public static AsyncHttpClient mainClient;
	
	public static final int SOCKET_TIMEOUT = 30000;
	public static final int CONNECTION_TIMEOUT = 30000;
	public static final int MAX_RETRIES = 0;
	public static final int RETRY_TIMEOUT = 1000;
	
	public static AsyncHttpClient getClient() {
		if (mainClient == null) {
			mainClient = new AsyncHttpClient();
			try {
				KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
				trustStore.load(null, null);
				MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
				sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
				mainClient.setSSLSocketFactory(sf);
			} catch (Exception e) {
				Log.e("exception in https hostname", "="+e.toString());
			}
			mainClient.setConnectTimeout(CONNECTION_TIMEOUT);
			mainClient.setResponseTimeout(SOCKET_TIMEOUT);
			mainClient.setMaxRetriesAndTimeout(MAX_RETRIES, RETRY_TIMEOUT);
		}
		return mainClient;
	}

	
}
