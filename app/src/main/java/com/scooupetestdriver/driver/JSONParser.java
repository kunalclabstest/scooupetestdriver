package com.scooupetestdriver.driver;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.scooupetestdriver.driver.datastructure.ApiResponseFlags;
import com.scooupetestdriver.driver.datastructure.CustomerInfo;
import com.scooupetestdriver.driver.datastructure.DriverRideRequest;
import com.scooupetestdriver.driver.datastructure.DriverScreenMode;
import com.scooupetestdriver.driver.datastructure.EndRideCustomerInfo;
import com.scooupetestdriver.driver.datastructure.EngagementStatus;
import com.scooupetestdriver.driver.datastructure.FareStructure;
import com.scooupetestdriver.driver.datastructure.UserData;
import com.scooupetestdriver.driver.locationfiles.MapUtils;
import com.scooupetestdriver.driver.utils.DateOperations;
import com.scooupetestdriver.driver.utils.FeaturesConfigFile;
import com.scooupetestdriver.driver.utils.HttpRequester;
import com.scooupetestdriver.driver.utils.Log;
import com.scooupetestdriver.driver.utils.Prefs;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class JSONParser {

	public JSONParser(){
		
	}
	
	public void parseLoginData(Context context, String response) throws Exception{
		JSONObject jObj = new JSONObject(response);
		JSONObject userData = jObj.getJSONObject("user_data");
		
		Data.userData = parseUserData(context, userData);
		Prefs.with(context).save("USER_DATA",Data.userData);
		

			SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
			Editor editor = pref.edit();
			editor.putString(Data.SP_ACCESS_TOKEN_KEY, Data.userData.accessToken);
			editor.commit();

		
		try{
			int currentUserStatus = userData.getInt("current_user_status");
			
			if(currentUserStatus == 1){
				Database2.getInstance(context).updateUserMode(Database2.UM_DRIVER);
				new DriverServiceOperations().startDriverService(context);

				HomeActivity.driverScreenMode = DriverScreenMode.D_INITIAL;
			}
//
		} catch(Exception e){
			e.printStackTrace();


		}
		
		parseFareDetails(userData);
		
	}
	
	
	public static void parseFareDetails(JSONObject userData){
		try{
			JSONArray fareDetailsArr = userData.getJSONArray("fare_details");
			JSONObject fareDetails0 = fareDetailsArr.getJSONObject(0);
			double farePerMin = 0;
			double waitTimeFarePerMin = 0;
			double freeMinutes = 0;
			double freeWaitMinutes = 0;

			if(fareDetails0.has("fare_per_min")){
				farePerMin = fareDetails0.getDouble("fare_per_min");
			}if(fareDetails0.has("wait_time_fare_per_min")){
				waitTimeFarePerMin = fareDetails0.getDouble("wait_time_fare_per_min");
			}
			if(fareDetails0.has("fare_threshold_time")){
				freeMinutes = fareDetails0.getDouble("fare_threshold_time");
			}if(fareDetails0.has("wait_threshold_time")){
				freeWaitMinutes = fareDetails0.getDouble("wait_threshold_time");
			}
			Data.fareStructure = new FareStructure(fareDetails0.getDouble("fare_fixed"), 
					fareDetails0.getDouble("fare_threshold_distance"), 
					fareDetails0.getDouble("fare_per_km"), 
					farePerMin, waitTimeFarePerMin, freeMinutes, freeWaitMinutes);
		} catch(Exception e){
			e.printStackTrace();
			Data.fareStructure = new FareStructure(25, 2, 6, 1, 0, 6,0);
		}
	}
	

	public UserData parseUserData(Context context, JSONObject userData) throws Exception{
		
		int canSchedule = 0, canChangeLocation = 0, schedulingLimitMinutes = 0, isAvailable = 1, exceptionalDriver = 0, gcmIntent = 1, christmasIconEnable = 0, nukkadEnable = 0;
		String phoneNo = "";
		
		if(userData.has("can_schedule")){
			canSchedule = userData.getInt("can_schedule");
		}
		
		if(userData.has("can_change_location")){
			canChangeLocation = userData.getInt("can_change_location");
		}
		
		if(userData.has("scheduling_limit")){
			schedulingLimitMinutes = userData.getInt("scheduling_limit");
		}
		
		if(userData.has("is_available")){
			isAvailable = userData.getInt("is_available");
		}
		
		if(userData.has("exceptional_driver")){
			exceptionalDriver = userData.getInt("exceptional_driver");
		}
		
		try{
			if(userData.has("gcm_intent")){
				gcmIntent = userData.getInt("gcm_intent");
				Database2.getInstance(context).updateDriverGcmIntent(gcmIntent);
				Database2.getInstance(context).close();
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			if(userData.has("christmas_icon_enable")){
				christmasIconEnable = userData.getInt("christmas_icon_enable");
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		
		if(userData.has("phone_no")){
			phoneNo = userData.getString("phone_no");
		}
		
		try{
			if(userData.has("nukkad_enable")){
				nukkadEnable = userData.getInt("nukkad_enable");
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		
		//"gcm_intent": 0, christmas_icon_enable
		
		return new UserData(userData.getString("access_token"), userData.getString("user_name"), 
				userData.getString("user_image"), userData.getString("referral_code"), phoneNo, userData.getString("user_email"),
				canSchedule, canChangeLocation, schedulingLimitMinutes, isAvailable, exceptionalDriver, gcmIntent, christmasIconEnable, nukkadEnable);
	}
	
	public String parseAccessTokenLoginData(Context context, String response, String accessToken) throws Exception{
		
		JSONObject jObj = new JSONObject(response);
		
		//Fetching login data
		JSONObject jLoginObject = jObj.getJSONObject("login");
		JSONObject userData = jLoginObject.getJSONObject("user_data");
		
		Data.userData = parseUserData(context, userData);
		Prefs.with(context).save("USER_DATA",Data.userData);
		parseFareDetails(userData);
		

			Database2.getInstance(context).updateUserMode(Database2.UM_DRIVER);

		//Fetching user current status
		JSONObject jUserStatusObject = jObj.getJSONObject("status");
		String resp = parseCurrentUserStatus(context, jUserStatusObject);
				
		return resp;
	}
	

	public String getUserStatus(Context context, String accessToken){
		String returnResponse = "";
		try{
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("access_token", accessToken));
			HttpRequester simpleJSONParser = new HttpRequester();
			String result = simpleJSONParser.getJSONFromUrlParams(Data.SERVER_URL + "/get_current_user_status", nameValuePairs);
			Log.e("result of = user_status", "="+result);
			if(result.contains(HttpRequester.SERVER_TIMEOUT)){
				returnResponse = HttpRequester.SERVER_TIMEOUT;
				return returnResponse;
			}
			else{
				JSONObject jObject1 = new JSONObject(result);
				returnResponse = parseCurrentUserStatus(context, jObject1);
				return returnResponse;
			}
		} catch(Exception e){
			e.printStackTrace();
			returnResponse = HttpRequester.SERVER_TIMEOUT;
			return returnResponse;
		}
	}
	

	public String parseCurrentUserStatus(Context context, JSONObject jObject1){
		
		String returnResponse = "";
		

			String screenMode = "";
			
			int engagementStatus = -1;
			String engagementId = "", userId = "", latitude = "", longitude = "", customerName = "", customerImage = "", customerPhone = "", customerRating = "", schedulePickupTime = "";
			int freeRide = 0;
			long rideTimeDiff=0;
			
			try{
							
							if(jObject1.has("error")){
								returnResponse = HttpRequester.SERVER_TIMEOUT;
								return returnResponse;
							}
							else{
								
//								{
//								    "flag": 150,
//								    "login": {
//								        "user_data": {
//								            "access_token": "f7dab9204dd4d08b2aaa1cf7cd28e4155fd6f41c80510a0aae6cfc557dfc3bbf0d92e5cde2f8bd49057df0df1f8f8ee7",
//								            "user_name": "Driver123",
//								            "user_image": "http://taximust.s3.amazonaws.com/user_profile/9022-userImage.jpg",
//								            "phone_no": "+917837546534",
//								            "referral_code": "DRIVER27",
//								            "is_available": 1,
//								            "gcm_intent": 1,
//								            "current_user_status": 1,
//								            "fare_details": [
//								                {
//								                    "id": 1,
//								                    "fare_fixed": 25,
//								                    "fare_per_km": 6,
//								                    "fare_threshold_distance": 2,
//								                    "fare_per_min": 1,
//								                    "fare_threshold_time": 0,
//								                    "car_type": 0
//								                }
//								            ],
//								            "exceptional_driver": 1,
//								            "user_email": "renuka.aggarwal+1@clicklabs.in"
//								        },
//								        "popup": 0
//								    },
//								    "status": {
//								        "last_engagement_info": [
//								            {
//								                "user_id": 415,
//								                "pickup_latitude": 30.718373,
//								                "pickup_longitude": 76.810287,
//								                "engagement_id": 7331,
//								                "status": 1,
//								                "session_id": 3955,
//								                "manual_destination_latitude": 0,
//								                "manual_destination_longitude": 0,
//								                "user_name": "Nadeem Alig",
//								                "phone_no": "+919258495485",
//								                "user_image": "http://graph.facebook.com/778124815574377/picture?width=160&height=160",
//								                "rating": 5,
//								                "is_scheduled": 0,
//								                "pickup_time": "",
//								                "free_ride": 0
//								            }
//								        ],
//								        "flag": 132
//								    }
//								}
								
								int flag = jObject1.getInt("flag");
								
								if(ApiResponseFlags.ACTIVE_REQUESTS.getOrdinal() == flag){
									
									JSONArray jActiveRequests = jObject1.getJSONArray("active_requests");
									
									Data.driverRideRequests.clear();
									for(int i=0; i<jActiveRequests.length(); i++){
										JSONObject jActiveRequest = jActiveRequests.getJSONObject(i);
										 String requestEngagementId = jActiveRequest.getString("engagement_id");
		    	    					 String requestUserId = jActiveRequest.getString("user_id");
		    	    					 double requestLatitude = jActiveRequest.getDouble("pickup_latitude");
		    	    					 double requestLongitude = jActiveRequest.getDouble("pickup_longitude");
		    	    					 String requestAddress = jActiveRequest.getString("pickup_location_address");

                                        String user_name = jActiveRequest.getString("user_name");
                                        String user_image = jActiveRequest.getString("user_image");


										if(user_image.contains("http://graph.facebook")){

											URL url = new URL(user_image);
											HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
											ucon.setInstanceFollowRedirects(false);
											URL secondURL = new URL(ucon.getHeaderField("Location"));
											URLConnection conn = secondURL.openConnection();
											conn.connect();

											user_image = secondURL.toString();

											Log.e("parsed url mein b mach gyi", "" + secondURL);

											Log.e("user_image_url mein mach gyi",""+user_image);
										}
										int user_rating = jActiveRequest.getInt("user_rating");


//		    	    					 String requestStartTime =  new DateOperations().getSixtySecAfterStartTime(""+jActiveRequest.getString("current_time"));
//		    	    					 String requestStartTime =  new DateOperations().getNintySecAfterStartTime(""+jActiveRequest.getString("current_time"));

										int timeLeft = Integer.parseInt(jActiveRequest.getString("time_left"));

										Calendar now = Calendar.getInstance();

										//add seconds to current date using Calendar.add method
										now.add(Calendar.SECOND, (timeLeft - 60));

										DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
										formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

										String requestStartTime = new DateOperations().getSixtySecAfterStartTime("" + formatter.format(now.getTime()).toString());


		    	    					 Log.i("current_time", "=="+jActiveRequest.getString("current_time"));
		    	    					 Log.i("requestStartTime", "requestStartTime = "+requestStartTime);
//
										LatLng source =new LatLng(Data.latitude, Data.longitude);
										LatLng destination =new LatLng(requestLatitude, requestLongitude);

		    	    					 Data.driverRideRequests.add(new DriverRideRequest(requestEngagementId, requestUserId, new LatLng(requestLatitude, requestLongitude),
		    	    								requestStartTime, requestAddress,user_name, user_image, user_rating,MapUtils.distance(source,destination,context)));
		    	    					 
		    	    					 Log.i("inserter in db", "insertDriverRequest = "+requestEngagementId);
									}
									
									Database2.getInstance(context).close();
									
									if(jActiveRequests.length() == 0){
										GCMIntentService.stopRing();
									}



								}
								else if(ApiResponseFlags.ENGAGEMENT_DATA.getOrdinal() == flag){
									JSONArray lastEngInfoArr = jObject1.getJSONArray("last_engagement_info");
									JSONObject jObject = lastEngInfoArr.getJSONObject(0);
									
									engagementStatus = jObject.getInt("status");
									
									if((EngagementStatus.ACCEPTED.getOrdinal() == engagementStatus) || 
											(EngagementStatus.STARTED.getOrdinal() == engagementStatus)){
										engagementId = jObject.getString("engagement_id");
										userId = jObject.getString("user_id");
										latitude = jObject.getString("pickup_latitude");
										longitude = jObject.getString("pickup_longitude");
										customerName = jObject.getString("user_name");
										customerImage = jObject.getString("user_image");
										customerPhone = jObject.getString("phone_no");
										customerRating = jObject.getString("rating");
										rideTimeDiff = jObject.getInt("ride_time")*1000;
										HomeActivity.previousWaitTime = jObject.getInt("wait_time")*1000;
										Data.isWaitStart = jObject.getInt("wait_time_started");
										
										int isScheduled = 0;
										if(jObject.has("is_scheduled")){
											isScheduled = jObject.getInt("is_scheduled");
											if(isScheduled == 1 && jObject.has("pickup_time")){
												schedulePickupTime = jObject.getString("pickup_time");
											}
										}
										
										if(jObject.has("free_ride")){
											freeRide = jObject.getInt("free_ride");
										}

										if(FeaturesConfigFile.isArrivedStateEnable){
											Data.isArrived = jObject.getInt("is_arrived");
											HomeActivity.timeTillArrived=jObject.getInt("arrival_time")*1000;
//											HomeActivity.timeTillArrived=1000;
										}

									}
								}
								else if(ApiResponseFlags.DRIVER_END_RIDE_SCREEN.getOrdinal() == flag){
									Data.endRidesCustomerInfo.clear();
									HomeActivity.totalFare=0;
									for(int i=0;i< jObject1.getJSONArray("users").length(); i++)
									{
										
										JSONObject customerObject=jObject1.getJSONArray("users").getJSONObject(i);
										HomeActivity.totalFare=HomeActivity.totalFare+customerObject.getDouble("fare");
										Data.endRidesCustomerInfo.add(new EndRideCustomerInfo(customerObject.getString("user_name"),customerObject.getString("user_image"),customerObject.getString("user_id"),customerObject.getString("engagement_id"),customerObject.getDouble("to_pay")
												,customerObject.getInt("is_payment_successful"),customerObject.getInt("payment_method"),customerObject.getInt("defaulter_flag")));
									}
									HomeActivity.totalDistance=jObject1.getJSONArray("users").getJSONObject(0).getDouble("distance_travelled");
									HomeActivity.waitTime=jObject1.getJSONArray("users").getJSONObject(0).getString("wait_time");
									HomeActivity.rideTime= "" + (Double.parseDouble(jObject1.getJSONArray("users").getJSONObject(0).getString("ride_time")) + Double.parseDouble(jObject1.getJSONArray("users").getJSONObject(0).getString("wait_time")));
//									HomeActivity.Co2_val=jObject1.getJSONArray("users").getJSONObject(0).getDouble("co2_saved");
									HomeActivity.discount=jObject1.getJSONArray("users").getJSONObject(0).getDouble("discount");
									
									engagementStatus = 3;
									
								}
							
							}
			} catch(Exception e){
				e.printStackTrace();
				engagementStatus = -1;
				returnResponse = HttpRequester.SERVER_TIMEOUT;
				return returnResponse;
			}
			
			Log.e("engagementStatus after parsing",  "="+engagementStatus);

			// 0 for request, 1 for accepted,2 for started,3 for ended, 4 for rejected by driver, 5 for rejected by user,6 for timeout, 7 for nullified by chrone
			if(EngagementStatus.ACCEPTED.getOrdinal() == engagementStatus){
				screenMode = Data.D_START_RIDE;
			}
			else if(EngagementStatus.STARTED.getOrdinal() == engagementStatus){
				screenMode = Data.D_IN_RIDE;
			}
			else if(EngagementStatus.ENDED.getOrdinal() == engagementStatus){
				screenMode = Data.D_RIDE_END;
			}
			else{
				screenMode = "";
			}
			
			
			if("".equalsIgnoreCase(screenMode)){
				HomeActivity.driverScreenMode = DriverScreenMode.D_INITIAL;
				clearSPData(context);
			}
			else{
				
				if(Data.D_START_RIDE.equalsIgnoreCase(screenMode)){
					HomeActivity.driverScreenMode = DriverScreenMode.D_START_RIDE;
					
					Data.dEngagementId = engagementId;
					Data.dCustomerId = userId;
					
					String lat = latitude;
					String lng = longitude;
					
					Data.dCustLatLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
					
					String name = customerName;
					String image = customerImage;
					String phone = customerPhone;
					String rating = customerRating;
					
					Data.assignedCustomerInfo = new CustomerInfo(Data.dCustomerId, name, image, phone, rating, freeRide);
					Data.assignedCustomerInfo.schedulePickupTime = schedulePickupTime;
					
				}
				else if(Data.D_IN_RIDE.equalsIgnoreCase(screenMode)){
					
					SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
					
					HomeActivity.driverScreenMode = DriverScreenMode.D_IN_RIDE;
					
					Data.dEngagementId = engagementId;
					Data.dCustomerId = userId;
					
					String lat = latitude;
					String lng = longitude;
					
					Data.dCustLatLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
					
					String name = customerName;
					String image = customerImage;
					String phone = customerPhone;
					String rating = customerRating;
					
					
					Data.assignedCustomerInfo = new CustomerInfo(Data.dCustomerId, name, image, phone, rating, freeRide);
					
					HomeActivity.totalDistance = Double.parseDouble(pref.getString(Data.SP_TOTAL_DISTANCE, "-1"));
//					HomeActivity.previousWaitTime = Long.parseLong(pref.getString(Data.SP_WAIT_TIME, "0"));
					
//					double previousRideTime = Double.parseDouble(pref.getString(Data.SP_RIDE_TIME, "0"));
					long rideStartTime = Long.parseLong(pref.getString(Data.SP_RIDE_START_TIME, ""+System.currentTimeMillis()));
//					long timeDiffToAdd = System.currentTimeMillis() - rideStartTime;
					long timeDiffToAdd = rideTimeDiff;
					Log.i("ride_time diff", "=="+rideTimeDiff);
					if(timeDiffToAdd > 0){
						HomeActivity.previousRideTime = timeDiffToAdd;
					}
					else{
						HomeActivity.previousRideTime = 0;
					}
					
					
					
					HomeActivity.waitStart = 2;
					
					String lat1 = pref.getString(Data.SP_LAST_LATITUDE, "0");
					String lng1 = pref.getString(Data.SP_LAST_LONGITUDE, "0");
					
					Data.startRidePreviousLatLng = new LatLng(Double.parseDouble(lat1), Double.parseDouble(lng1));
					
					Log.e("Data on app restart", "-----");
					Log.i("HomeActivity.totalDistance", "="+HomeActivity.totalDistance);
					Log.i("Data.startRidePreviousLatLng", "="+Data.startRidePreviousLatLng);
					Log.e("----------", "-----");
					
					Log.writePathLogToFile(Data.dEngagementId, "Got from SP totalDistance = "+HomeActivity.totalDistance);
					Log.writePathLogToFile(Data.dEngagementId, "Got from SP Data.startRidePreviousLatLng = "+Data.startRidePreviousLatLng);
					
				}
				else if(Data.D_RIDE_END.equalsIgnoreCase(screenMode))
				{
					HomeActivity.driverScreenMode = DriverScreenMode.D_RIDE_END;
				}
				else{
					HomeActivity.driverScreenMode = DriverScreenMode.D_INITIAL;
				}
				
			}
			
			Log.e("HomeActivity.driverScreenMode", "="+HomeActivity.driverScreenMode);
			
			
			


		
		return returnResponse;
	}
	

	
	public void clearSPData(final Context context) {
		SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
		Editor editor = pref.edit();

		editor.putString(Data.SP_DRIVER_SCREEN_MODE, "");

		editor.putString(Data.SP_D_ENGAGEMENT_ID, "");
		editor.putString(Data.SP_D_CUSTOMER_ID, "");
		editor.putString(Data.SP_D_LATITUDE, "0");
		editor.putString(Data.SP_D_LONGITUDE, "0");
		editor.putString(Data.SP_D_CUSTOMER_NAME, "");
		editor.putString(Data.SP_D_CUSTOMER_IMAGE, "");
		editor.putString(Data.SP_D_CUSTOMER_PHONE, "");
		editor.putString(Data.SP_D_CUSTOMER_RATING, "");

		editor.putString(Data.SP_TOTAL_DISTANCE, "-1");
		editor.putString(Data.SP_WAIT_TIME, "0");
		editor.putString(Data.SP_RIDE_TIME, "0");
		editor.putString(Data.SP_RIDE_START_TIME, ""+System.currentTimeMillis());
		editor.putString(Data.SP_LAST_LATITUDE, "0");
		editor.putString(Data.SP_LAST_LONGITUDE, "0");


		editor.commit();

		Database.getInstance(context).deleteSavedPath();
		Database.getInstance(context).close();

	}
	
	
	
	
}
