package com.scooupetestdriver.driver.locationfiles;

import android.content.Context;
import android.location.Location;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.scooupetestdriver.driver.Database2;
import com.scooupetestdriver.driver.DriverServiceOperations;
import com.scooupetestdriver.driver.utils.DateOperations;
import com.scooupetestdriver.driver.utils.HttpRequester;
import com.scooupetestdriver.driver.utils.Log;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

public class DriverLocationDispatcher {

	public void sendLocationToServer(Context context, String filePrefix){
		
		double LOCATION_TOLERANCE = 0.0001;
		
		try {
			String userMode = Database2.getInstance(context).getUserMode();
			
			if(Database2.UM_DRIVER.equalsIgnoreCase(userMode)){
				
				PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag2");
				wakeLock.acquire();
				
				String accessToken = Database2.getInstance(context).getDLDAccessToken();
				String deviceToken = Database2.getInstance(context).getDLDDeviceToken();
				String serverUrl = Database2.getInstance(context).getDLDServerUrl();
				
				if((!"".equalsIgnoreCase(accessToken)) && (!"".equalsIgnoreCase(deviceToken)) && (!"".equalsIgnoreCase(serverUrl))){
					LatLng latLng = Database2.getInstance(context).getDriverCurrentLocation();
					if((Math.abs(latLng.latitude) > LOCATION_TOLERANCE) && (Math.abs(latLng.longitude) > LOCATION_TOLERANCE)){
						ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("access_token", accessToken));
						nameValuePairs.add(new BasicNameValuePair("latitude", "" + latLng.latitude));
						nameValuePairs.add(new BasicNameValuePair("longitude", "" + latLng.longitude));
						nameValuePairs.add(new BasicNameValuePair("device_token", deviceToken));
						nameValuePairs.add(new BasicNameValuePair("driver_car_direction_angle", ""+Database2.getInstance(context).getDriverCurrentBearing()));
			
						HttpRequester simpleJSONParser = new HttpRequester();
						String result = simpleJSONParser.getJSONFromUrlParams(serverUrl + "/update_driver_location", nameValuePairs);
									
						Log.e("result in DLD", "=" + result);
						Log.writeLogToFile(filePrefix, "Server result "+DateOperations.getCurrentTime()+" = "+result);
						
						try{
							//{"log":"Updated"}
							JSONObject jObj = new JSONObject(result);
							if(jObj.has("log")){
								String log = jObj.getString("log");
								if("Updated".equalsIgnoreCase(log)){
									Database2.getInstance(context).updateDriverLastLocationTime();
								}
							}
						} catch(Exception e){
							e.printStackTrace();
						}
						
						simpleJSONParser = null;
						nameValuePairs = null;
					}
				}
				
				wakeLock.release();
			}
			else{
				new DriverServiceOperations().stopService(context);
			}

			Database2.getInstance(context).close();
			
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.writeLogToFile(filePrefix, "Exception in sending to server "+DateOperations.getCurrentTime()+" = "+e);
		}
		finally{
			Database2.getInstance(context).close();
    	}
	}

	
	
	
	public void saveLocationToDatabase(final Context context, final Location location){
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Database2.getInstance(context).updateDriverCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()),location.getBearing());
					Database2.getInstance(context).close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	
}
