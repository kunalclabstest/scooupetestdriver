package com.scooupetestdriver.driver.locationfiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.scooupetestdriver.driver.Database2;
import com.scooupetestdriver.driver.utils.AppStatus;
import com.scooupetestdriver.driver.utils.DateOperations;
import com.scooupetestdriver.driver.utils.Log;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.model.LatLng;

public class LocationReceiverDriver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
    	final Location location = (Location) intent.getExtras().get(LocationClient.KEY_LOCATION_CHANGED);
    	if(location != null){
	    	new Thread(new Runnable() {
				@Override
				public void run() {
					Database2.getInstance(context).updateDriverCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()),location.getBearing());
					Database2.getInstance(context).close();
			    	Log.e("DriverLocationUpdateService location in pi reciever ", "=="+location);
			    	Log.writeLogToFile("LocationReciever", "Receiver "+DateOperations.getCurrentTime()+" = "+location 
			    			+ " hasNet = "+AppStatus.getInstance(context).isOnline(context));
					new DriverLocationDispatcher().sendLocationToServer(context, "LocationReciever");
				}
			}).start();
    	}
    }
    
}