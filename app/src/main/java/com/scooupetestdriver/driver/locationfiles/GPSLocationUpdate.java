package com.scooupetestdriver.driver.locationfiles;

import android.location.Location;

public interface GPSLocationUpdate {
	public void onGPSLocationChanged(Location location);
}
