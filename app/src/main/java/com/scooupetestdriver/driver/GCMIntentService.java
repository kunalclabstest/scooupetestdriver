package com.scooupetestdriver.driver;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.scooupetestdriver.driver.datastructure.DriverRideRequest;
import com.scooupetestdriver.driver.datastructure.DriverScreenMode;
import com.scooupetestdriver.driver.datastructure.PushFlags;
import com.scooupetestdriver.driver.locationfiles.MapUtils;
import com.scooupetestdriver.driver.utils.DateOperations;
import com.scooupetestdriver.driver.utils.FlurryEventLogger;
import com.scooupetestdriver.driver.utils.Log;
import com.scooupetestdriver.driver.utils.Prefs;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;

public class GCMIntentService extends IntentService {
	
	public static final int NOTIFICATION_ID = 1;
    
    public GCMIntentService() {
        super("GcmIntentService");
    }


	  
		@SuppressWarnings("deprecation")
		public static void notificationManager(Context context, String message, boolean ring) {
	    	
			try {
				long when = System.currentTimeMillis();
				
				NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				
				Log.v("message",","+message);
				
				Intent notificationIntent = new Intent(context, SplashNewActivity.class);
				
				
				notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
				
				NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
				builder.setAutoCancel(true);
				builder.setContentTitle(context.getString(R.string.app_name));
				builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
				builder.setContentText(message);
				builder.setTicker(message);
				
				if(ring){
					builder.setLights(Color.GREEN, 500, 500);
				}
				else{
					builder.setDefaults(Notification.DEFAULT_ALL);
				}
				
				builder.setWhen(when);
				builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
				builder.setSmallIcon(R.drawable.notif_icon);
				builder.setContentIntent(intent);
				
				
				
				
				
				Notification notification = builder.build();
				notificationManager.notify(NOTIFICATION_ID, notification);
				
				PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
				wl.acquire(15000);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}

		@SuppressWarnings("deprecation")
		public static void notificationManagerResume(Context context, String message, boolean ring) {
			
			try {
				long when = System.currentTimeMillis();
				
				NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				
				Log.v("message",","+message);
				
				Intent notificationIntent = new Intent(context, HomeActivity.class);
				
				notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
				
				NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
				builder.setAutoCancel(true);
				builder.setContentTitle(context.getString(R.string.app_name));
				builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
				builder.setContentText(message);
				builder.setTicker(message);
				
				if(ring){
					builder.setLights(Color.GREEN, 500, 500);
				}
				else{
					builder.setDefaults(Notification.DEFAULT_ALL);
				}
				
				builder.setWhen(when);
				builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
				builder.setSmallIcon(R.drawable.notif_icon);
				builder.setContentIntent(intent);
				
				
				
				Notification notification = builder.build();
				notificationManager.notify(NOTIFICATION_ID, notification);
				
				PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
				wl.acquire(15000);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	    
	    
	    public static void clearNotifications(Context context){
			NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.cancel(NOTIFICATION_ID);
	    }

	    
	    
		 
	    @Override
	    public void onHandleIntent(Intent intent) {
	    	String currentTimeUTC = DateOperations.getCurrentTimeInUTC();
	    	String currentTime = DateOperations.getCurrentTime();
	    	
	        Bundle extras = intent.getExtras();
	        
	        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
	        // The getMessageType() intent parameter must be the intent you received
	        // in your BroadcastReceiver.
	        String messageType = gcm.getMessageType(intent);

	        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
	            /*
	             * Filter messages based on message type. Since it is likely that GCM
	             * will be extended in the future with new message types, just ignore
	             * any message types you're not interested in, or that you don't
	             * recognize.
	             */
	            if (GoogleCloudMessaging.
	                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//	                sendNotification("Send error: " + extras.toString());
	            } else if (GoogleCloudMessaging.
	                    MESSAGE_TYPE_DELETED.equals(messageType)) {
//	                sendNotification("Deleted messages on server: " +
//	                        extras.toString());
	            // If it's a regular GCM message, do some work.
	            } else if (GoogleCloudMessaging.
	                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
	                // This loop represents the service doing some work.

	            	String SHARED_PREF_NAME1 = "myPref", SP_ACCESS_TOKEN_KEY = "access_token";
	            	
	            	SharedPreferences pref1 = getSharedPreferences(SHARED_PREF_NAME1, 0);
	        		final String accessToken = pref1.getString(SP_ACCESS_TOKEN_KEY, "");
	        		if(!"".equalsIgnoreCase(accessToken)){
	            	
	    	    	try{
	    		    	 Log.e("Recieved a gcm message arg1...", ","+intent.getExtras());
	    		    	 
	    		    	 if(!"".equalsIgnoreCase(intent.getExtras().getString("message", ""))){
	    		    		 
	    		    		 String message = intent.getExtras().getString("message");
	    		    		 
	    		    		 try{
	    	    				 JSONObject jObj = new JSONObject(message);
	    	    				 
	    	    				 int flag = jObj.getInt("flag");
	    	    				 
	    	    				 if(PushFlags.REQUEST.getOrdinal() == flag){
	    	    						 
//	    	    						 {   "engagement_id": engagement_id, 
//	    	    							 "user_id": data.customer_id, 
//	    	    							 "flag": g_enum_notificationFlags.REQUEST,
//	    	    							 "latitude": data.pickup_latitude, 
//	    	    							 "longitude": data.pickup_longitude, 
//	    	    							 "address": pickup_address
//	    	    							 "start_time": date}
	    	    					 
	    	    					 
	    	    						 String engagementId = jObj.getString("engagement_id");
		    	    					 String userId = jObj.getString("user_id");
		    	    					 double latitude = jObj.getDouble("latitude");
		    	    					 double longitude = jObj.getDouble("longitude");
		    	    					 String startTime = jObj.getString("start_time");
		    	    					 String address = jObj.getString("address");
                                     String user_name = jObj.getString("user_name");


									 String user_image = jObj.getString("user_image");
									 if(user_image.contains("http://graph.facebook")){

										 URL url = new URL(user_image);
										 HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
										 ucon.setInstanceFollowRedirects(false);
										 URL secondURL = new URL(ucon.getHeaderField("Location"));
										 URLConnection conn = secondURL.openConnection();
										 conn.connect();

										 user_image = secondURL.toString();

										 Log.e("parsed url mein b mach gyi", "" + secondURL);

										 Log.e("user_image_url mein mach gyi",""+user_image);
									 }


									 int user_rating = jObj.getInt("user_rating");

		    	    					 //sendRequestAckToServer(this, engagementId, currentTimeUTC);
		    	    					 
		    	    					 FlurryEventLogger.requestPushReceived(this, engagementId, DateOperations.utcToLocal(startTime), currentTime);
		    	    					 
		    	    					 long startTimeMillis = new DateOperations().getMilliseconds(startTime);

		    	    					 startTime = new DateOperations().getSixtySecAfterCurrentTime();
		    	    					 
		    	    					 if(HomeActivity.appInterruptHandler != null){

		    	    							 if(DriverScreenMode.D_INITIAL == HomeActivity.driverScreenMode ||
		    	    								DriverScreenMode.D_RIDE_END == HomeActivity.driverScreenMode){
													 try {
														 if(Data.driverRideRequests.remove(new DriverRideRequest(engagementId,userId)))
														 {
															 HomeActivity.appInterruptHandler.onRideRequestTimeout(engagementId);
														 }
													 } catch (Exception e) {
														 e.printStackTrace();
													 }
													 LatLng source =new LatLng(0.0, 0.0);
													 LatLng destination =new LatLng(latitude, longitude);
													 if(HomeActivity.myLocation!=null)
													 {
														 source = new LatLng(HomeActivity.myLocation.getLatitude(), HomeActivity.myLocation.getLongitude());
													 }
													 Data.driverRideRequests.add(new DriverRideRequest(engagementId, userId,
															 new LatLng(latitude, longitude), startTime, address, user_name, user_image, user_rating,MapUtils.distance(source,destination,getApplicationContext())));

													 if(Prefs.with(getApplicationContext()).getString("IS_RING_ENABLE","1").equalsIgnoreCase("1"))
													 {
														 startRing(this);
													 }
					    	    					 RequestTimeoutTimerTask requestTimeoutTimerTask = new RequestTimeoutTimerTask(this, engagementId);
					    	    					 requestTimeoutTimerTask.startTimer(0, 20000, startTimeMillis, 60000);
					    	    					 
		    	    								 notificationManagerResume(this, getString(R.string.new_ride), true);
				    	    						 HomeActivity.appInterruptHandler.onNewRideRequest();
		    	    							 }

		    	    					 }
		    	    					 else{
		    	    						 notificationManager(this, getString(R.string.new_ride), true);

											 if(Prefs.with(getApplicationContext()).getString("IS_RING_ENABLE","1").equalsIgnoreCase("1"))
											 {
												 startRing(this);
											 }

			    	    					 RequestTimeoutTimerTask requestTimeoutTimerTask = new RequestTimeoutTimerTask(this, engagementId);
			    	    					 requestTimeoutTimerTask.startTimer(0, 20000, startTimeMillis, 60000);
		    	    					 }
	    	    					 
	    	    				 }
	    	    				 else if(PushFlags.REQUEST_CANCELLED.getOrdinal() == flag){
    	    						 
	    	    					 String engagementId = jObj.getString("engagement_id");
	    	    					 clearNotifications(this);
	    	    					 
	    	    					 stopRing();


	    	    					 if(HomeActivity.appInterruptHandler != null){
	    	    						 Data.driverRideRequests.remove(new DriverRideRequest(engagementId,""));
										 HomeActivity.appInterruptHandler.onCancelRideRequest(engagementId, false);
	    	    					 }

								 }

	    	    				 else if(PushFlags.RIDE_ACCEPTED_BY_OTHER_DRIVER.getOrdinal() == flag){
    	    						 
	    	    					 String engagementId = jObj.getString("engagement_id");
	    	    					 clearNotifications(this);
	    	    					 
	    	    					 stopRing();
	    	    					 
	    	    					 if(HomeActivity.appInterruptHandler != null){
	    	    						 Data.driverRideRequests.remove(new DriverRideRequest(engagementId,""));
	    	    						 HomeActivity.appInterruptHandler.onCancelRideRequest(engagementId, true);
	    	    					 }
	    	    					 
	    	    				 }
								 else if(PushFlags.FORCE_END_RIDE.getOrdinal()==flag){
									 if(HomeActivity.appInterruptHandler != null){
										 HomeActivity.appInterruptHandler.forceStateChange();
										 notificationManagerResume(this, getString(R.string.ride_end_by_admin), false);
									 }
									 else {
										 notificationManager(this, getString(R.string.ride_end_by_admin), false);
									 }

								 }
								 else if (PushFlags.MESSAGE_FROM_ADMIN.getOrdinal()==flag){
									 String messageFromAdmin  = jObj.getString("message_from_admin");
									 notificationManager(this,messageFromAdmin,false);
								 }
	    	    				 
	    	    				else if(PushFlags.REQUEST_TIMEOUT.getOrdinal() == flag){
    	    						 
	    	    					 String engagementId = jObj.getString("engagement_id");
	    	    					 clearNotifications(this);
	    	    					 
	    	    					 stopRing();
	    	    					 
	    	    					 if(HomeActivity.appInterruptHandler != null){
	    	    						 Data.driverRideRequests.remove(new DriverRideRequest(engagementId,""));
	    	    						 HomeActivity.appInterruptHandler.onRideRequestTimeout(engagementId);
	    	    					 }
	    	    					 
	    	    				 }

	    	    				 else if(PushFlags.WAITING_STARTED.getOrdinal() == flag || PushFlags.WAITING_ENDED.getOrdinal() == flag){
	    	    					 String message1 = jObj.getString("message");
	    	    					 if (HomeActivity.activity == null) {
		    	    					 notificationManager(this, ""+message1, false);
	    	    					 }
	    	    					 else{
		    	    					 notificationManagerResume(this, "" + message1, false);
	    	    					 }
	    	    				 }
	    	    				 else if (PushFlags.CHANGE_STATE.getOrdinal() == flag) {
	    	    					 String logMessage = jObj.getString("message");
									if (HomeActivity.appInterruptHandler != null) {
										HomeActivity.appInterruptHandler.onChangeStatePushReceived();
										notificationManagerResume(this, logMessage, false);
									} else {
										notificationManager(this, logMessage, false);
									}
								}
	    	    				else if(PushFlags.DISPLAY_MESSAGE.getOrdinal() == flag){
	    	    					 String message1 = jObj.getString("message");
	    	    					 if (HomeActivity.activity == null) {
		    	    					 notificationManager(this, ""+message1, false);
	    	    					 }
	    	    					 else{
		    	    					 notificationManagerResume(this, "" + message1, false);
	    	    					 }
	    	    				 }

	    		    		 } catch(Exception e){
	    		    			 e.printStackTrace();
	    		    		 }
	    		    		 
	    		    	 }
	    	    		
	    	    	 }
	    	    	 catch(Exception e){
	    	    		 Log.e("Recieved exception message arg1...", ","+intent);
	    	    		 Log.e("exception", ","+e);
						 e.printStackTrace();
	    	    	 }
	        		}
	    	         
	    	    
	            }
	        }
	        // Release the wake lock provided by the WakefulBroadcastReceiver.
	        GcmBroadcastReceiver.completeWakefulIntent(intent);
	    }

	    
	    
	    
	    
	    
	    
	    public static MediaPlayer mediaPlayer;
	    public static Vibrator vibrator;
	    
		public static void startRing(Context context){
			try {
				stopRing();
				vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				if(vibrator.hasVibrator()){
					long[] pattern = {0, 1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900 };
					vibrator.vibrate(pattern, -1);
				}
				AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//				am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
				am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
				mediaPlayer = MediaPlayer.create(context, R.raw.telephone_ring);
				mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
				    @Override
				    public void onCompletion(MediaPlayer mp) {
						mediaPlayer.stop();
				    	mediaPlayer.reset();
				    	mediaPlayer.release();
				    	vibrator.cancel();
				    }
				});
				mediaPlayer.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public static CountDownTimer ringStopTimer;
		public static void startRingWithStopHandler(Context context){
			try {
				stopRing();
				vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				if(vibrator.hasVibrator()){
					long[] pattern = {0, 1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900, 
											1350, 3900 };
					vibrator.vibrate(pattern, -1);
				}
				AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//				am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
				am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
				mediaPlayer = MediaPlayer.create(context, R.raw.telephone_ring);
				mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
				    @Override
				    public void onCompletion(MediaPlayer mp) {
						mediaPlayer.stop();
				    	mediaPlayer.reset();
				    	mediaPlayer.release();
				    	vibrator.cancel();
				    }
				});
				mediaPlayer.start();
				
				
				ringStopTimer = new CountDownTimer(20000, 1000) {

				    @Override
				    public void onTick(long millisUntilFinished) {
				    	Log.e("millisUntilFinished", "="+millisUntilFinished);
				    }

				    @Override
				    public void onFinish() {
				    	stopRing();
				    }
				};
				ringStopTimer.start();
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		public static void stopRing(){
			try {
				if(vibrator != null){
					vibrator.cancel();
				}
				if(mediaPlayer != null && mediaPlayer.isPlaying()){
					mediaPlayer.stop();
					mediaPlayer.reset();
					mediaPlayer.release();
				}
				if(ringStopTimer != null){
					ringStopTimer.cancel();
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				vibrator = null;
				mediaPlayer = null;
				ringStopTimer = null;
			}
		}

	    


	    class RequestTimeoutTimerTask{
	    	
	    	public Timer timer;
	    	public TimerTask timerTask;
	    	public Context context;
	    	
	    	public String engagementId;
	    	
	    	public long startTime, lifeTime, endTime, period, executionTime;
	    	
	    	public RequestTimeoutTimerTask(Context context, String engagementId){
	    		this.context = context;
	    		this.engagementId = engagementId;
	    	}
	    	
	    	public void startTimer(long delay, long period, long startTime, long lifeTime){
	    		stopTimer();
	    		
	    		this.startTime = startTime;
	    		this.lifeTime = lifeTime;
	    		this.endTime = startTime + lifeTime;
	    		this.period = period;
	    		this.executionTime = -1;
	    		
	    		timer = new Timer();
	    		timerTask = new TimerTask() {
	    			@Override
	    			public void run() {
	    				long start = System.currentTimeMillis();
	    				
	    				if(executionTime == -1){
							executionTime = RequestTimeoutTimerTask.this.startTime;
						}
	    				
		    			if(executionTime >= RequestTimeoutTimerTask.this.endTime){
		    				if(Data.driverRideRequests != null){
			    				boolean removed = Data.driverRideRequests.remove(new DriverRideRequest(engagementId,""));
			    				if(removed){
			    					if(HomeActivity.appInterruptHandler != null){
				    					HomeActivity.appInterruptHandler.onRideRequestTimeout(engagementId);
				    				}
			    					clearNotifications(context);
			    					stopRing();
			    				}
		    				}
		    				stopTimer();
		    			}
		    			long stop = System.currentTimeMillis();
					    long elapsedTime = stop - start;
					    if(executionTime != -1){
					    	if(elapsedTime >= RequestTimeoutTimerTask.this.period){
					    		executionTime = executionTime + elapsedTime;
					    	}
					    	else{
					    		executionTime = executionTime + RequestTimeoutTimerTask.this.period;
					    	}
					    }
					    Log.i("RequestTimeoutTimerTask execution", "="+(RequestTimeoutTimerTask.this.endTime - executionTime));
	    			}
	    		};
	    		timer.scheduleAtFixedRate(timerTask, delay, period);
	    	}
	    	
	    	public void stopTimer(){
	    		try{
	    			Log.e("RequestTimeoutTimerTask","stopTimer");
	    			startTime = 0;
	    			lifeTime = 0;
	    			if(timerTask != null){
	    				timerTask.cancel();
	    				timerTask = null;
	    			}
	    			if(timer != null){
	    				timer.cancel();
	    				timer.purge();
	    				timer = null;
	    			}
	    		} catch(Exception e){
	    			e.printStackTrace();
	    		}
	    	}
	    	
	    	
	    }
}





