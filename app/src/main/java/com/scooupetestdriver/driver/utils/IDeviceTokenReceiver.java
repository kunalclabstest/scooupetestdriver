package com.scooupetestdriver.driver.utils;

public interface IDeviceTokenReceiver {
	public void deviceTokenReceived(String regId);
}
