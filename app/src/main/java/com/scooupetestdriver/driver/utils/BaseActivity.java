package com.scooupetestdriver.driver.utils;

import android.support.v4.app.FragmentActivity;

import com.scooupetestdriver.driver.Data;

/**
 * Created by Sarthak on 04/11/15.
 */
public class BaseActivity extends FragmentActivity {
    @Override
    protected void onResume() {
        Data.context = this;
        super.onResume();
    }

    @Override
    protected void onPause() {
        Data.context = null;
        super.onPause();
    }
}
