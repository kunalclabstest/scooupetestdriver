package com.scooupetestdriver.driver.retrofit;


import java.io.File;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;


public interface ApiService {

    /**
     * @param accessToken
     * @param deviceToken
     * @param latitude
     * @param longitude
     * @param appVersion
     * @param deviceType
     * @param uniqueDeviceId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/login_driver_via_access_token")
    public void accessTokenLogin(@Field("access_token") String accessToken, @Field("device_token") String deviceToken, @Field("latitude") Double latitude, @Field("longitude") Double longitude, @Field("app_version") int appVersion, @Field("device_type") String deviceType, @Field("unique_device_id") String uniqueDeviceId, Callback<String> callback);




    /**
     *
     * @param userFbId
     * @param userFbName
     * @param fbAccessToken
     * @param userName
     * @param fbMail
     * @param latitude
     * @param longitude
     * @param deviceToken
     * @param country
     * @param appVersion
     * @param osVersion
     * @param deviceName
     * @param deviceType
     * @param uniqueDeviceId
     * @param otp
     * @param phoneNumber
     * @param password
     * @param heraAboutText
     * @param callback
     */
    @FormUrlEncoded
    @POST("/customer_fb_registeration_form")
    public void sendFacebookLoginValues(@Field("user_fb_id") String userFbId, @Field("user_fb_name") String userFbName, @Field("fb_access_token") String fbAccessToken,
                                        @Field("username") String userName, @Field("fb_mail") String fbMail, @Field("latitude") Double latitude, @Field("longitude") Double longitude, @Field("device_token") String deviceToken,
                                        @Field("country") String country, @Field("app_version") int appVersion, @Field("os_version") String osVersion, @Field("device_name") String deviceName, @Field("device_type") String deviceType,
                                        @Field("unique_device_id") String uniqueDeviceId, @Field("otp") String otp, @Field("ph_no") String phoneNumber, @Field("password") String password, @Field("how_hear_us") String heraAboutText, @Field("nounce") String nounce,
                                        @Field("payment_type_flag") int paymentTypeFlag, @Field("corporate_email") String corporateEmail, @Field("corporate_otp") String corporateCode, Callback<String> callback);

    /**
     * @param accessToken
     * @param oldPassword
     * @param newPassword
     * @param callback
     */

    @FormUrlEncoded
    @POST("/reset_password")
    public void resetPassword(@Field("access_token") String accessToken, @Field("old_password") String oldPassword, @Field("new_password") String newPassword, Callback<String> callback);

    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/get_missed_rides")
    public void getMissedRidesAsync(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param currentMode
     * @param callback
     */
    @FormUrlEncoded
    @POST("/booking_history")
    public void getRidesAsync(@Field("access_token") String accessToken, @Field("current_mode") String currentMode, Callback<String> callback);

    /**
     * @param userName
     * @param phoneNo
     * @param eMail
     * @param password
     * @param otp
     * @param deviceType
     * @param uniqueDeviceId
     * @param deviceToken
     * @param latitude
     * @param longitude
     * @param country
     * @param deviceName
     * @param appVersion
     * @param osVersion
     * @param hearAboutText
     * @param makeMeDriverFlag
     * @param imageFlag
     * @param nounce
     * @param image
     * @param callback
     */
    @FormUrlEncoded
    @POST("/customer_registeration")
    public void sendSignupValues(@Field("user_name") String userName, @Field("ph_no") String phoneNo, @Field("email") String eMail, @Field("password") String password,
                                 @Field("otp") String otp, @Field("device_type") String deviceType, @Field("unique_device_id") String uniqueDeviceId, @Field("device_token") String deviceToken,
                                 @Field("latitude") Double latitude, @Field("longitude") Double longitude, @Field("country") String country, @Field("device_name") String deviceName,
                                 @Field("app_version") int appVersion, @Field("os_version") String osVersion, @Field("how_hear_us") String hearAboutText, @Field("make_me_driver_flag") String makeMeDriverFlag,
                                 @Field("image_flag") String imageFlag, @Field("nounce") String nounce, @Field("image") String image,
                                 @Field("payment_type_flag") int paymentTypeFlag, @Field("corporate_email") String corporateEmail, @Field("corporate_otp") String corporateCode, Callback<String> callback);


    /**
     * @param email
     * @param callback
     */
    @FormUrlEncoded
    @POST("/forgot_password")
    public void forgotPasswordAsync(@Field("email") String email, Callback<String> callback);

    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/show_pickup_schedules")
    public void getFutureSchedulesAsync(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param section
     * @param callback
     */
    @FormUrlEncoded
    @POST("/get_information")
    public void getHelpAsync(@Field("section") String section, Callback<String> callback);

    /**
     * @param accessToken
     * @param sessionId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/cancel_the_request")
    public void cancelCustomerRequestAsync(@Field("access_token") String accessToken, @Field("session_id") String sessionId, Callback<String> callback);

    /**
     * @param accessToken
     * @param customerId
     * @param engagementId
     * @param latitude
     * @param longitude
     * @param callback
     */
    @FormUrlEncoded
    @POST("/accept_a_request")
    public void driverAcceptRideAsync(@Field("access_token") String accessToken, @Field("customer_id") String customerId, @Field("engagement_id") String engagementId,
                                      @Field("latitude") Double latitude, @Field("longitude") Double longitude, Callback<String> callback);

    /**
     * @param accessToken
     * @param customerId
     * @param engagementId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/cancel_the_ride")
    public void driverCancelRideAsync(@Field("access_token") String accessToken, @Field("customer_id") String customerId, @Field("engagement_id") String engagementId,
                                      Callback<String> callback);

    /**
     * @param accessToken
     * @param engagementId
     * @param customerId
     * @param latitude
     * @param longitude
     * @param distanceTraveled
     * @param waitTime
     * @param rideTime
     * @param callback
     */
    @FormUrlEncoded
    @POST("/end_ride")
    public void driverEndRideAsync(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, @Field("customer_id") String customerId,
                                   @Field("latitude") Double latitude, @Field("longitude") Double longitude, @Field("distance_travelled") String distanceTraveled,
                                   @Field("wait_time") String waitTime, @Field("ride_time") String rideTime, Callback<String> callback);

    /**
     * @param accessToken
     * @param customerId
     * @param engagementId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/reject_a_request")
    public void driverRejectRequestAsync(@Field("access_token") String accessToken, @Field("customer_id") String customerId, @Field("engagement_id") String engagementId,
                                         Callback<String> callback);

    /**
     * @param accessToken
     * @param engagementId
     * @param customerId
     * @param latitude
     * @param longitude
     * @param callback
     */
    @FormUrlEncoded
    @POST("/start_ride")
    public void driverStartRideAsync(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, @Field("customer_id") String customerId,
                                     @Field("latitude") String latitude, @Field("longitude") String longitude, Callback<String> callback);

    /**
     * @param accessToken
     * @param engagementId
     * @param ridePath
     * @param fileType
     * @param callback
     */
    @FormUrlEncoded
    @POST("/upload_file")
    public void driverUploadPathDataFileAsync(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, @Field("ride_path") File ridePath,
                                              @Field("file_type") String fileType, Callback<String> callback);

    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/logout")
    public void logoutAsync(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/make_me_driver_request")
    public void makeMeDriverAsync(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param engagementId
     * @param customerId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/acknowledge_manual_engagement")
    public void manualPatchPushAckAPI(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, @Field("customer_id") String customerId,
                                      Callback<String> callback);

    /**
     * @param accessToken
     * @param engagementId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/skip_rating_by_customer")
    public void skipFeedbackForCustomerAsync(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, Callback<String> callback);

    /**
     * @param accessToken
     * @param customerId
     * @param flag
     * @param callback
     */
    @FormUrlEncoded
    @POST("/start_end_wait")
    public void startEndWaitAsync(@Field("access_token") String accessToken, @Field("customer_id") String customerId, @Field("flag") String flag, Callback<String> callback);

    /**
     * @param accessToken
     * @param givenRating
     * @param engagementId
     * @param driverId
     * @param feedback
     * @param callback
     */
    @FormUrlEncoded
    @POST("/rate_the_driver")
    public void submitFeedbackToDriverAsync(@Field("access_token") String accessToken, @Field("given_rating") String givenRating, @Field("engagement_id") String engagementId,
                                            @Field("driver_id") String driverId, @Field("feedback") String feedback, Callback<String> callback);

    /**
     * @param phoneNo
     * @param callback
     */
    @FormUrlEncoded
    @POST("/send_otp_via_call")
    public void initiateOTPCallAsync(@Field("phone_no") String phoneNo, Callback<String> callback);

    /**
     * @param accessToken
     * @param userName
     * @param backAccountToken
     * @param imageFlag
     * @param image
     * @param callback
     */
    @Multipart
    @POST("/edit_driver_profile")
    public void updateProfile(@Part("access_token") TypedString accessToken, @Part("user_name") TypedString userName, @Part("bank_account_stripe_token") TypedString backAccountToken,
                              @Part("image_flag") TypedString imageFlag, @Part("image") TypedFile image, Callback<String> callback);

    /**
     * @param email
     * @param password
     * @param deviceType
     * @param uniqueDeviceId
     * @param latitude
     * @param longitude
     * @param country
     * @param deviceName
     * @param appVersion
     * @param osVersion
     * @param callback
     */
    @FormUrlEncoded
    @POST("/login_driver_via_email")
    public void sendLoginValues(@Field("email") String email, @Field("password") String password, @Field("device_type") String deviceType, @Field("unique_device_id") String uniqueDeviceId,
                                @Field("device_token") String deviceToken, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("country") String country, @Field("device_name") String deviceName,
                                @Field("app_version") int appVersion, @Field("os_version") String osVersion, Callback<String> callback);

   /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/agree_terms")
    public void termsAgreedAsync(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param tip
     * @param engagementId
     * @param paymentMethod
     * @param toPay
     * @param callback
     */
    @FormUrlEncoded
    @POST("/payment_on_ride_completion")
    public void payFare(@Field("access_token") String accessToken, @Field("tip") String tip, @Field("engagement_id") String engagementId, @Field("payment_method") String paymentMethod,
                        @Field("to_pay") String toPay, Callback<String> callback);

    @GET("/json")
    public String getDistanceMatrixData(@Query("origins")String origin,@Query("destinations")String destination);

    @GET("/json")
    public String getAutoCompleteSearchResults(@Query("input") String input, @Query("type") String type, @Query("location") String location, @Query("radius") String radius, @Query("key") String key);


    @GET("/json")
    public String getSearchResultsFromPlaceId(@Query("placeid") String placeId, @Query("key") String key);

    @FormUrlEncoded
    @POST("/collect_cash_payment")
    public void collectCashPayment(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, Callback<String> callback);
}