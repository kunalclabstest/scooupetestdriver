package com.scooupetestdriver.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.StyleSpan;
import android.util.Pair;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.scooupetestdriver.driver.customlayouts.CustomInfoWindow;
import com.scooupetestdriver.driver.customlayouts.CustomMapMarkerCreator;
import com.scooupetestdriver.driver.customlayouts.DialogPopup;
import com.scooupetestdriver.driver.customlayouts.PausableChronometer;
import com.scooupetestdriver.driver.datastructure.ApiResponseFlags;
import com.scooupetestdriver.driver.datastructure.AppMode;
import com.scooupetestdriver.driver.datastructure.CustomerInfo;
import com.scooupetestdriver.driver.datastructure.DriverRideRequest;
import com.scooupetestdriver.driver.datastructure.DriverScreenMode;
import com.scooupetestdriver.driver.datastructure.EndRideCustomerInfo;
import com.scooupetestdriver.driver.datastructure.HelpSection;
import com.scooupetestdriver.driver.datastructure.LatLngPair;
import com.scooupetestdriver.driver.datastructure.SearchResult;
import com.scooupetestdriver.driver.locationfiles.AutoCompleteSearchResult;
import com.scooupetestdriver.driver.locationfiles.DriverLocationUpdateService;
import com.scooupetestdriver.driver.locationfiles.GPSForegroundLocationFetcher;
import com.scooupetestdriver.driver.locationfiles.GPSLocationUpdate;
import com.scooupetestdriver.driver.locationfiles.LocationFetcher;
import com.scooupetestdriver.driver.locationfiles.LocationUpdate;
import com.scooupetestdriver.driver.locationfiles.MapStateListener;
import com.scooupetestdriver.driver.locationfiles.MapUtils;
import com.scooupetestdriver.driver.locationfiles.TouchableMapFragment;
import com.scooupetestdriver.driver.retrofit.RestClient;
import com.scooupetestdriver.driver.utils.AppStatus;
import com.scooupetestdriver.driver.utils.BaseActivity;
import com.scooupetestdriver.driver.utils.CustomAsyncHttpResponseHandler;
import com.scooupetestdriver.driver.utils.DateOperations;
import com.scooupetestdriver.driver.utils.FeaturesConfigFile;
import com.scooupetestdriver.driver.utils.FlurryEventLogger;
import com.scooupetestdriver.driver.utils.HttpRequester;
import com.scooupetestdriver.driver.utils.Log;
import com.scooupetestdriver.driver.utils.Prefs;
import com.scooupetestdriver.driver.utils.Utils;
import com.facebook.Session;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.BlurTransform;
import com.squareup.picasso.CircleTransform;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

@SuppressLint("DefaultLocale")
public class HomeActivity extends BaseActivity implements AppInterruptHandler, LocationUpdate, GPSLocationUpdate {


    DrawerLayout drawerLayout;// views declaration


    //menu bar
    LinearLayout menuLayout;


    ImageView profileImg;
    TextView userName;

    RelativeLayout driverONRl;
    TextView driverONText;
    ImageView driverONToggle;


    RelativeLayout bookingsRl;
    TextView bookingsText;

    RelativeLayout fareDetailsRl;
    TextView fareDetailsText;

    RelativeLayout helpRl;
    TextView helpText;

    RelativeLayout logoutRl;
    TextView logoutText;

    Dialog forceDialog, cancelDialog;
    RelativeLayout belowLayoutRating;


    //Top RL
    RelativeLayout topRl;
    Button menuBtn; //, favBtn;
    TextView title;
    ImageView navBarLogo;
    Button checkServerBtn, toggleDebugModeBtn;


    //Map layout
    RelativeLayout mapLayout;
    GoogleMap map;
    TouchableMapFragment mapFragment;

    Button driverEndRideNavBtn;

    // Driver main layout
    RelativeLayout driverMainLayout;


    //Driver initial layout
    RelativeLayout driverInitialLayout;
    TextView textViewDriverInfo;
    RelativeLayout driverNewRideRequestRl;
    ListView driverRideRequestsList;
    TextView driverNewRideRequestText;
    TextView driverNewRideRequestClickText;
    Button driverInitialMyLocationBtn;
    RelativeLayout driverOffLayout;
    TextView driverOffText;

    DriverRequestListAdapter driverRequestListAdapter;


    // Driver Engaged layout
    RelativeLayout driverEngagedLayout;

    RelativeLayout searchBarLayoutDestination;
    TextView searchEtDestination;

    TextView driverPassengerName, driverBelowPassengerName;
    TextView driverPassengerRatingValue;
    RelativeLayout driverPassengerCallRl, driverPassengerBelowCallRl, callCustomerRl, callCustomerBelowRl, driverPassengerMsgRl;
    TextView  driverPassengerBelowCallText;
    TextView driverScheduledRideText;
    ImageView driverFreeRideIcon;

    //Start ride layout
    LinearLayout driverStartRideMainRl,linearArrivedTimeChronometer;
    Button driverStartRideMyLocationBtn, driverStartRideBtn,driverCancelRideBtn,driverArrivedBtn;
    PausableChronometer driverArrivedChronometer;


    //SearchLayout
    LinearLayout linearSearchParent;
    EditText etLocation;
    RelativeLayout relClearSearchLocation;
    TextView tvLocation;
    ProgressBar searchLocationProgress;
    ImageView imgLocationIcon;
    ListView listViewSearch;
    SearchListAdapter searchListAdapter;
    ArrayList<AutoCompleteSearchResult> autoCompleteSearchResults = new ArrayList<AutoCompleteSearchResult>();
    boolean isSearchStartLocation = true, canChangeLocationText = true;

    //End ride layout
    RelativeLayout driverInRideMainRl;
    Button driverEndRideMyLocationBtn, driverStartRideNavigationBtn;
    TextView driverIRDistanceText, driverIRDistanceValue;
    TextView driverIRFareText, driverIRFareValue;
    TextView driverRideTimeText;
    PausableChronometer rideTimeChronometer;
    RelativeLayout driverWaitRl;
    TextView driverWaitText;
    PausableChronometer waitChronometer;
    TextView inrideMinFareText, inrideMinFareValue, inrideFareAfterText, inrideFareAfterValue;
    Button inrideFareInfoBtn;
    Button driverEndRideBtn;

    public static int waitStart = 2;
    double distanceAfterWaitStarted = 0;
    public static boolean searchStart = false;


    //Review layout
    RelativeLayout endRideReviewRl;

    ImageView reviewUserImgBlured, reviewUserImage;
    TextView reviewUserName, reviewReachedDestinationText,
            reviewDistanceText, reviewDistanceValue,
            reviewWaitText, reviewWaitValue, reviewRideTimeText, reviewRideTimeValue,
            reviewFareText, reviewFareValue, reviewCO2Text, reviewCO2Value,tvActualFare,tvActualFareValue,
            tvPaybleAmount,tvPAybleAmountVAlue,tvDiscount,tvDiscountValue,
            reviewRatingText;
    LinearLayout reviewRatingBarRl, endRideInfoRl;
    TextView rideOverText, takeFareText;
    RelativeLayout  previousImageRL, nextImageRL;

    RatingBar reviewRatingBar;
    Button btnCollectCash;
    TextView reviewMinFareText, reviewMinFareValue, reviewFareAfterText, reviewFareAfterValue;
    Button reviewFareInfoBtn;
    ListView endRideCustomerListView;
    EndRideCustomersListAdapter endRideCustomersListAdapter;

    // data variables declaration

    Location lastLocation;

    int setUserIndex = 0;

    DecimalFormat decimalFormat = new DecimalFormat("#.#");

    DecimalFormat decimalFormatNoDecimal = new DecimalFormat("#");

    static double totalDistance = -1, totalFare = 0, Co2_val = 0,discount=0;
    public static ArrayList<LatLngPair> deltaLatLngPairs = new ArrayList<LatLngPair>();


    static long previousWaitTime = 0, previousRideTime = 0,timeTillArrived=0;

    static String waitTime = "", rideTime = "";


    static Location myLocation;

    static DriverScreenMode driverScreenMode;

    Marker currentLocationMarker;
    MarkerOptions markerOptionsCustomerPickupLocation;

    static AppInterruptHandler appInterruptHandler;

    static Activity activity;

    boolean loggedOut = false,
            zoomedToMyLocation = false,
            mapTouchedOnce = false;
    boolean dontCallRefreshDriver = false;
    static boolean makeOkButtonEnable = false;
    double totalCashToCollect = 0;

    AlertDialog gpsDialogAlert;

    LocationFetcher lowPowerLF, highAccuracyLF;

    TextView feedbackSkipBtn;
    Button feedbackSubmitBtn;
    RatingBar feedbackRatingBar;
    EditText rateValue;


    //TODO check final variables
    public static AppMode appMode;

    public static final int MAP_PATH_COLOR = Color.TRANSPARENT;
    public static final int D_TO_C_MAP_PATH_COLOR = Color.RED;
    public static final int DRIVER_TO_STATION_MAP_PATH_COLOR = Color.BLUE;

    public static final long DRIVER_START_RIDE_CHECK_METERS = 600; //in meters

    public static final long LOCATION_UPDATE_TIME_PERIOD = 10000; //in milliseconds
    public static final double MAX_DISPLACEMENT_THRESHOLD = 200; //in meters


    public static final float LOW_POWER_ACCURACY_CHECK = 2000, HIGH_ACCURACY_ACCURACY_CHECK = 200;  //in meters
    public static final float WAIT_FOR_ACCURACY_UPPER_BOUND = 2000, WAIT_FOR_ACCURACY_LOWER_BOUND = 200;  //in meters

    public static final long AUTO_RATING_DELAY = 5 * 60 * 1000; //in milliseconds


    public static final double MAX_WAIT_TIME_ALLOWED_DISTANCE = 200; //in meters


    public ASSL assl;

    public GPSForegroundLocationFetcher gpsForegroundLocationFetcher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initializeGPSForegroundLocationFetcher();

        HomeActivity.appInterruptHandler = HomeActivity.this;

        activity = this;

        loggedOut = false;
        zoomedToMyLocation = false;
        dontCallRefreshDriver = false;
        mapTouchedOnce = false;

        appMode = AppMode.NORMAL;
        DecimalFormatSymbols custom=new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        decimalFormat.setDecimalFormatSymbols(custom);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);


        assl = new ASSL(HomeActivity.this, drawerLayout, 1134, 720, false);

        drawerLayout.setDrawerListener(new DrawerListener() {

            @Override
            public void onDrawerStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onDrawerSlide(View arg0, float arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onDrawerOpened(View arg0) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchEtDestination.getWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View arg0) {
                // TODO Auto-generated method stub


            }
        });


        //Swipe menu
        menuLayout = (LinearLayout) findViewById(R.id.menuLayout);

        RelativeLayout relEnableDisableRinger=(RelativeLayout)findViewById(R.id.relEnableDisableRing);
        relEnableDisableRinger.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                enableDisableRingerDialog(HomeActivity.this);
                return false;
            }
        });
        feedbackRatingBar = (RatingBar) findViewById(R.id.feedbackRatingBar);
        TextView rateRide = (TextView) findViewById(R.id.rateRide);
        rateRide.setTypeface(Data.getFont(activity));
        // TextView rateText = (TextView)findViewById(R.id.rateText); rateText.setTypeface(Data.getFont(activity));
        rateValue = (EditText) findViewById(R.id.rateTextValue);
        rateValue.setTypeface(Data.getFont(activity));


        feedbackRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating <= 0) {
                    ratingBar.setRating(1);
                }
            }
        });


        feedbackSubmitBtn = (Button) findViewById(R.id.driverSubmitBtn);
        feedbackSubmitBtn.setTypeface(Data.getFont(activity), Typeface.BOLD);

        feedbackSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                submitFeedbackToDriverAsync(HomeActivity.this, "" + feedbackRatingBar.getRating(), rateValue.getText().toString());

            }

        });

        feedbackSkipBtn = (TextView) findViewById(R.id.driverSkipBtn);
        feedbackSkipBtn.setTypeface(Data.getFont(activity),Typeface.BOLD);
        feedbackSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                skipFeedbackForCustomerAsync(HomeActivity.this);
            }

        });


        profileImg = (ImageView) findViewById(R.id.profileImg);
        userName = (TextView) findViewById(R.id.userName);
        userName.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvEditProfile = (TextView) findViewById(R.id.tvEditProfile);
        tvEditProfile.setTypeface(Data.getFont(getApplicationContext()));



        driverONRl = (RelativeLayout) findViewById(R.id.driverONRl);
        driverONText = (TextView) findViewById(R.id.driverONText);
        driverONText.setTypeface(Data.getFont(getApplicationContext()));
        driverONToggle = (ImageView) findViewById(R.id.driverONToggle);

        bookingsRl = (RelativeLayout) findViewById(R.id.bookingsRl);
        bookingsText = (TextView) findViewById(R.id.bookingsText);
        bookingsText.setTypeface(Data.getFont(getApplicationContext()));

        fareDetailsRl = (RelativeLayout) findViewById(R.id.fareDetailsRl);
        fareDetailsText = (TextView) findViewById(R.id.fareDetailsText);
        fareDetailsText.setTypeface(Data.getFont(getApplicationContext()));

        helpRl = (RelativeLayout) findViewById(R.id.helpRl);
        helpText = (TextView) findViewById(R.id.helpText);
        helpText.setTypeface(Data.getFont(getApplicationContext()));

        logoutRl = (RelativeLayout) findViewById(R.id.logoutRl);
        logoutText = (TextView) findViewById(R.id.logoutText);
        logoutText.setTypeface(Data.getFont(getApplicationContext()));


        belowLayoutRating = (RelativeLayout) findViewById(R.id.belowLayoutRating);
        belowLayoutRating.setVisibility(View.GONE);


        //Top RL
        topRl = (RelativeLayout) findViewById(R.id.topRl);
        menuBtn = (Button) findViewById(R.id.menuBtn);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        navBarLogo = (ImageView) findViewById(R.id.navBarLogo);
        checkServerBtn = (Button) findViewById(R.id.checkServerBtn);
        toggleDebugModeBtn = (Button) findViewById(R.id.toggleDebugModeBtn);


        menuBtn.setVisibility(View.VISIBLE);
        navBarLogo.setVisibility(View.VISIBLE);

        title.setVisibility(View.GONE);


        //Map Layout
        mapLayout = (RelativeLayout) findViewById(R.id.mapLayout);
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        mapFragment = ((TouchableMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));

        // Driver main layout
        driverMainLayout = (RelativeLayout) findViewById(R.id.driverMainLayout);


        //Driver initial layout
        driverInitialLayout = (RelativeLayout) findViewById(R.id.driverInitialLayout);
        textViewDriverInfo = (TextView) findViewById(R.id.textViewDriverInfo);
        textViewDriverInfo.setTypeface(Data.getFont(getApplicationContext()));
        driverNewRideRequestRl = (RelativeLayout) findViewById(R.id.driverNewRideRequestRl);
        driverRideRequestsList = (ListView) findViewById(R.id.driverRideRequestsList);
        driverNewRideRequestText = (TextView) findViewById(R.id.driverNewRideRequestText);
        driverNewRideRequestText.setTypeface(Data.getFont(getApplicationContext()));
        driverNewRideRequestClickText = (TextView) findViewById(R.id.driverNewRideRequestClickText);
        driverNewRideRequestClickText.setTypeface(Data.getFont(getApplicationContext()));
        driverInitialMyLocationBtn = (Button) findViewById(R.id.driverInitialMyLocationBtn);
        driverOffLayout = (RelativeLayout) findViewById(R.id.driverOffLayout);
        driverOffText = (TextView) findViewById(R.id.driverOffText);
        driverOffText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        driverNewRideRequestRl.setVisibility(View.GONE);

        driverRequestListAdapter = new DriverRequestListAdapter();
        driverRideRequestsList.setAdapter(driverRequestListAdapter);
        
        //search Layout
        linearSearchParent = (LinearLayout) findViewById(R.id.linearLayoutSearch);

        etLocation = (EditText) findViewById(R.id.etLocation);
        etLocation.setTypeface(Data.getFont(getApplicationContext()));
        relClearSearchLocation = (RelativeLayout) findViewById(R.id.relClearSearchLocation);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvLocation.setTypeface(Data.getFont(getApplicationContext()));
        searchLocationProgress = (ProgressBar) findViewById(R.id.progressBarSearch);
        searchLocationProgress.setVisibility(View.GONE);
        imgLocationIcon = (ImageView) findViewById(R.id.imgLocationIcon);
        imgLocationIcon.setVisibility(View.VISIBLE);
        listViewSearch = (ListView) findViewById(R.id.listViewSearch);
        searchListAdapter = new SearchListAdapter();
        listViewSearch.setAdapter(searchListAdapter);

        // Driver engaged layout
        driverEngagedLayout = (RelativeLayout) findViewById(R.id.driverEngagedLayout);
        searchBarLayoutDestination = (RelativeLayout) findViewById(R.id.searchBarLayoutDestination);

        searchEtDestination = (TextView) findViewById(R.id.searchEtDestination);
        searchEtDestination.setTypeface(Data.getFont(getApplicationContext()));


        searchBarLayoutDestination.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                etLocation.setHint(getResources().getString(R.string.search_destination_location));
                etLocation.setText(searchEtDestination.getText().toString());
                etLocation.setSelection(etLocation.getText().length());
                tvLocation.setText(getResources().getString(R.string.destination_location));
                Utils.showSoftKeyboard(HomeActivity.this, etLocation);
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                isSearchStartLocation = true;
                driverScreenMode = DriverScreenMode.D_SEARCH;
                switchDriverScreen(driverScreenMode);
            }
        });


        etLocation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                Log.i("search text", "==" + s.toString().trim());
                if (map != null) {
                    if (s.length() > 0) {
                        getSearchResults(s.toString().trim(), map.getCameraPosition().target);
                    }
                }
                markerOptionsCustomerPickupLocation = null;
                startDestinationPathUpdateTimer();
            }
        });

        etLocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                Utils.showSoftKeyboard(HomeActivity.this, etLocation);
            }
        });


        relClearSearchLocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                etLocation.setText("");

            }
        });

        RelativeLayout rlClearDestination = (RelativeLayout) findViewById(R.id.rlClearDestination);
        rlClearDestination.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                searchEtDestination.setText("");
                Prefs.with(getApplicationContext()).save("destinationAddress", "");
                Data.dDestinationLatLng = null;
                Prefs.with(getApplicationContext()).save("destinationLatitude", "");
                Prefs.with(getApplicationContext()).save("destinationLongitude", "");
                if (map != null) {
                    map.clear();
                    driverInitialMyLocationBtn.performClick();
                }
                cancelDestinationPathUpdateTimer();
            }
        });


        driverPassengerName = (TextView) findViewById(R.id.driverPassengerName);
        driverPassengerName.setTypeface(Data.getFont(getApplicationContext()));
        driverBelowPassengerName = (TextView) findViewById(R.id.driverBelowPassengerName);
        driverBelowPassengerName.setTypeface(Data.getFont(getApplicationContext()));
        driverPassengerRatingValue = (TextView) findViewById(R.id.driverPassengerRatingValue);
        driverPassengerRatingValue.setTypeface(Data.getFont(getApplicationContext()));
        driverPassengerCallRl = (RelativeLayout) findViewById(R.id.driverPassengerCallRl);
        driverPassengerMsgRl = (RelativeLayout) findViewById(R.id.driverPassengerMsgRl);
        driverPassengerBelowCallRl = (RelativeLayout) findViewById(R.id.driverBelowPassengerCallRl);
        driverPassengerBelowCallText = (TextView) findViewById(R.id.driverBelowPassengerCallText);
        driverPassengerBelowCallText.setTypeface(Data.getFont(getApplicationContext()));
        driverScheduledRideText = (TextView) findViewById(R.id.driverScheduledRideText);
        driverScheduledRideText.setTypeface(Data.getFont(getApplicationContext()));
        driverFreeRideIcon = (ImageView) findViewById(R.id.driverFreeRideIcon);

        driverPassengerRatingValue.setVisibility(View.GONE);

        //Start ride layout
        driverStartRideMainRl = (LinearLayout) findViewById(R.id.driverStartRideMainRl);
        driverStartRideMyLocationBtn = (Button) findViewById(R.id.driverStartRideMyLocationBtn);
        driverStartRideBtn = (Button) findViewById(R.id.driverStartRideBtn);
        driverStartRideBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        driverCancelRideBtn = (Button) findViewById(R.id.driverCancelRideBtn);
        driverCancelRideBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        driverArrivedBtn = (Button) findViewById(R.id.driverArrivedBtn);
        driverArrivedBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        linearArrivedTimeChronometer = (LinearLayout) findViewById(R.id.linearChronometreParent);
        driverArrivedChronometer=(PausableChronometer) findViewById(R.id.arrivedTimeChronometer);driverArrivedChronometer.setTypeface(Data.getFont(getApplicationContext()),Typeface.BOLD);
        TextView tvArrivedTime=(TextView)findViewById(R.id.tvArrivedTime); tvArrivedTime.setTypeface(Data.getFont(getApplicationContext()));

        //End ride layout
        driverInRideMainRl = (RelativeLayout) findViewById(R.id.driverInRideMainRl);

        driverEndRideMyLocationBtn = (Button) findViewById(R.id.driverEndRideMyLocationBtn);
        driverEndRideNavBtn = (Button) findViewById(R.id.driverEndRideNavBtn);

        driverStartRideNavigationBtn = (Button) findViewById(R.id.driverStartRideNavigationBtn);

        driverIRDistanceText = (TextView) findViewById(R.id.driverIRDistanceText);
        driverIRDistanceText.setTypeface(Data.getFont(getApplicationContext()));
        driverIRDistanceValue = (TextView) findViewById(R.id.driverIRDistanceValue);
        driverIRDistanceValue.setTypeface(Data.getFont(getApplicationContext()));


        driverIRFareText = (TextView) findViewById(R.id.driverIRFareText);
        driverIRFareText.setTypeface(Data.getFont(getApplicationContext()));

        driverIRFareValue = (TextView) findViewById(R.id.driverIRFareValue);
        driverIRFareValue.setTypeface(Data.getFont(getApplicationContext()));

        driverRideTimeText = (TextView) findViewById(R.id.driverRideTimeText);
        driverRideTimeText.setTypeface(Data.getFont(getApplicationContext()));
        rideTimeChronometer = (PausableChronometer) findViewById(R.id.rideTimeChronometer);
        rideTimeChronometer.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        driverWaitRl = (RelativeLayout) findViewById(R.id.driverWaitRl);
        TextView driverWaitTime = (TextView) findViewById(R.id.waitText);
        driverWaitTime.setTypeface(Data.getFont(getApplicationContext()));
        driverWaitText = (TextView) findViewById(R.id.driverWaitText);
        driverWaitText.setTypeface(Data.getFont(getApplicationContext()),Typeface.BOLD);
        waitChronometer = (PausableChronometer) findViewById(R.id.waitChronometer);
        waitChronometer.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        inrideMinFareText = (TextView) findViewById(R.id.inrideMinFareText);
        inrideMinFareText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        inrideMinFareValue = (TextView) findViewById(R.id.inrideMinFareValue);
        inrideMinFareValue.setTypeface(Data.getFont(getApplicationContext()));
        inrideFareAfterText = (TextView) findViewById(R.id.inrideFareAfterText);
        inrideFareAfterText.setTypeface(Data.getFont(getApplicationContext()));
        inrideFareAfterValue = (TextView) findViewById(R.id.inrideFareAfterValue);
        inrideFareAfterValue.setTypeface(Data.getFont(getApplicationContext()));
        inrideFareInfoBtn = (Button) findViewById(R.id.inrideFareInfoBtn);


        driverEndRideBtn = (Button) findViewById(R.id.driverEndRideBtn);
        driverEndRideBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        waitStart = 2;


        rideTimeChronometer.setText("00:00:00");
        waitChronometer.setText("00:00:00");
        driverArrivedChronometer.setText("00:00:00");


        //Review Layout
        endRideReviewRl = (RelativeLayout) findViewById(R.id.endRideReviewRl);
        callCustomerRl = (RelativeLayout) findViewById(R.id.callCustomerAboveRl);

        reviewUserImgBlured = (ImageView) findViewById(R.id.reviewUserImgBlured);
        reviewUserImage = (ImageView) findViewById(R.id.reviewUserImage);

        reviewUserName = (TextView) findViewById(R.id.reviewUserName);
        reviewUserName.setTypeface(Data.getFont(getApplicationContext()));
        reviewReachedDestinationText = (TextView) findViewById(R.id.reviewReachedDestinationText);
        reviewReachedDestinationText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        reviewDistanceText = (TextView) findViewById(R.id.reviewDistanceText);
        reviewDistanceText.setTypeface(Data.getFont(getApplicationContext()));
        reviewDistanceValue = (TextView) findViewById(R.id.reviewDistanceValue);
        reviewDistanceValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewWaitText = (TextView) findViewById(R.id.reviewWaitText);
        reviewWaitText.setTypeface(Data.getFont(getApplicationContext()));
        reviewWaitValue = (TextView) findViewById(R.id.reviewWaitValue);
        reviewWaitValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewRideTimeText = (TextView) findViewById(R.id.reviewRideTimeText);
        reviewRideTimeText.setTypeface(Data.getFont(getApplicationContext()));
        reviewRideTimeValue = (TextView) findViewById(R.id.reviewRideTimeValue);
        reviewRideTimeValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareText = (TextView) findViewById(R.id.reviewFareText);
        reviewFareText.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareValue = (TextView) findViewById(R.id.reviewFareValue);
        reviewFareValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewRatingText = (TextView) findViewById(R.id.reviewRatingText);
        reviewRatingText.setTypeface(Data.getFont(getApplicationContext()));

        reviewCO2Text = (TextView) findViewById(R.id.reviewCO2Text);
        reviewCO2Text.setTypeface(Data.getFont(getApplicationContext()));
        reviewCO2Value = (TextView) findViewById(R.id.reviewCO2Value);
        reviewCO2Value.setTypeface(Data.getFont(getApplicationContext()));

        tvActualFare = (TextView) findViewById(R.id.tvActualFare);
        tvActualFare.setTypeface(Data.getFont(getApplicationContext()));
        tvActualFareValue = (TextView) findViewById(R.id.tvActualFareValue);
        tvActualFareValue.setTypeface(Data.getFont(getApplicationContext()));
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);
        tvDiscount.setTypeface(Data.getFont(getApplicationContext()));
        tvDiscountValue = (TextView) findViewById(R.id.tvDiscountValue);
        tvDiscountValue.setTypeface(Data.getFont(getApplicationContext()));
        tvPaybleAmount = (TextView) findViewById(R.id.tvPayableAmount);
        tvPaybleAmount.setTypeface(Data.getFont(getApplicationContext()));
        tvPAybleAmountVAlue = (TextView) findViewById(R.id.tvPayableAmountValue);
        tvPAybleAmountVAlue.setTypeface(Data.getFont(getApplicationContext()));


        reviewRatingBarRl = (LinearLayout) findViewById(R.id.reviewRatingBarRl);
        endRideInfoRl = (LinearLayout) findViewById(R.id.endRideInfoRl);
        rideOverText = (TextView) findViewById(R.id.rideOverText);
        rideOverText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        takeFareText = (TextView) findViewById(R.id.takeFareText);
        takeFareText.setTypeface(Data.getFont(getApplicationContext()));
        reviewRatingBar = (RatingBar) findViewById(R.id.reviewRatingBar);
        btnCollectCash = (Button) findViewById(R.id.reviewSubmitBtn);
        btnCollectCash.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        previousImageRL = (RelativeLayout) findViewById(R.id.previousImageRL);
        nextImageRL = (RelativeLayout) findViewById(R.id.nextImageRL);
        endRideCustomerListView = (ListView) findViewById(R.id.listViewEndRideCustomer);

        reviewMinFareText = (TextView) findViewById(R.id.reviewMinFareText);
        reviewMinFareText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        reviewMinFareValue = (TextView) findViewById(R.id.reviewMinFareValue);
        reviewMinFareValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareAfterText = (TextView) findViewById(R.id.reviewFareAfterText);
        reviewFareAfterText.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareAfterValue = (TextView) findViewById(R.id.reviewFareAfterValue);
        reviewFareAfterValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareInfoBtn = (Button) findViewById(R.id.reviewFareInfoBtn);


        reviewRatingBarRl.setVisibility(View.GONE);
        endRideInfoRl.setVisibility(View.VISIBLE);


        reviewRatingBar.setRating(0);


        //Top bar events
        menuBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(menuLayout);
            }
        });


        checkServerBtn.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                String message = "";

                if (Data.SERVER_URL.equalsIgnoreCase(Data.TRIAL_SERVER_URL)) {
                    message = "Current server is TRIAL. " + Data.TRIAL_SERVER_URL;
                } else if (Data.SERVER_URL.equalsIgnoreCase(Data.LIVE_SERVER_URL)) {
                    message = "Current server is LIVE. " + Data.LIVE_SERVER_URL;
                } else if (Data.SERVER_URL.equalsIgnoreCase(Data.DEV_SERVER_URL)) {
                    message = "Current server is DEV. " + Data.DEV_SERVER_URL;
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                FlurryEventLogger.checkServerPressed(Data.userData.accessToken);


                return false;
            }
        });

        toggleDebugModeBtn.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                confirmDebugPasswordPopup(HomeActivity.this);
                FlurryEventLogger.debugPressed(Data.userData.accessToken);
                return false;
            }
        });





        tvEditProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, profile.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
//				FlurryEventLogger.shareScreenOpened(Data.userData.accessToken);
            }
        });

        driverONToggle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (driverScreenMode == DriverScreenMode.D_INITIAL && !(Data.driverRideRequests.size()>0)) {
                    Log.e("DriverMode onClick", "=" + Data.userData.isAvailable);

                    if (Data.userData.isAvailable == 1) {
                        changeDriverModeToON(0);
                    } else {
                        changeDriverModeToON(1);
                    }
                }
            }
        });





        fareDetailsRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendToFareDetails();
            }
        });


        helpRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, HelpActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
                FlurryEventLogger.helpScreenOpened(Data.userData.accessToken);
            }
        });


        bookingsRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    startActivity(new Intent(HomeActivity.this, DriverHistoryActivity.class));
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);

//
                FlurryEventLogger.rideScreenOpened(Data.userData.accessToken);
            }
        });


        if(FeaturesConfigFile.isChangeLanguagesEnable) {
            RelativeLayout  languagePrefrencesRl = (RelativeLayout) findViewById(R.id.languagePrefrencesRl);
            TextView languagePrefrencesText = (TextView) findViewById(R.id.languagePrefrencesText);
            languagePrefrencesText.setTypeface(Data.getFont(getApplicationContext()));
            languagePrefrencesRl.setVisibility(View.VISIBLE);
            languagePrefrencesRl.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, SelectLanguageActivity.class));
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);

                }
            });

        }
        logoutRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (driverScreenMode == DriverScreenMode.D_INITIAL) {
                    logoutPopup(HomeActivity.this);
                    FlurryEventLogger.logoutPressed(Data.userData.accessToken);
                } else {
                    new DialogPopup().alertPopup(activity, "", getString(R.string.cant_logout));
                    FlurryEventLogger.logoutPressedBetweenRide(Data.userData.accessToken);
                }
            }
        });


        menuLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        driverStartRideNavigationBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + Data.dCustLatLng.latitude + "," + Data.dCustLatLng.longitude));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        driverEndRideNavBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Data.dDestinationLatLng != null) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + Data.dDestinationLatLng.latitude + "," + Data.dDestinationLatLng.longitude));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(HomeActivity.this, "Destination not selected", Toast.LENGTH_SHORT).show();
                }
            }
        });


        // driver initial layout events
        driverNewRideRequestRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                driverNewRideRequestRl.setVisibility(View.GONE);
//                driverRideRequestsList.setVisibility(View.VISIBLE);
            }
        });

        driverOffLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(menuLayout);

            }
        });

        // driver start ride layout events
        driverPassengerCallRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + Data.assignedCustomerInfo.phoneNumber));
                startActivity(callIntent);
            }
        });

        driverPassengerMsgRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Uri sms_uri = Uri.parse("smsto:" + Data.assignedCustomerInfo.phoneNumber);
                Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
                startActivity(sms_intent);


            }
        });

        driverPassengerBelowCallRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + Data.assignedCustomerInfo.phoneNumber));
                startActivity(callIntent);
            }
        });


        driverStartRideBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (getBatteryPercentage() >= 10) {
                    startRidePopup(HomeActivity.this);
                } else {
                    new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.low_battery_warning));
                }
            }
        });


        driverCancelRideBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                cancelRidePopup(HomeActivity.this);
            }
        });
        driverArrivedBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                arrivedConfirmationPopup(HomeActivity.this);
            }
        });


        // driver in ride layout events
        driverWaitText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                    driverWaitText.setEnabled(false);
                    if (waitStart == 2) {
                        startWait(true);
                    } else if (waitStart == 1) {
                        stopWait(true);
                    } else if (waitStart == 0) {
                        startWait(true);
                    }
                } else {
                    new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.check_internet_message));
                }
            }
        });

        inrideFareInfoBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                fareInfoBtn.performClick();
            }
        });

        driverEndRideBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                endRidePopup(HomeActivity.this);
            }
        });


        // End ride review layout events
        endRideReviewRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        previousImageRL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Data.endRidesCustomerInfo != null && Data.endRidesCustomerInfo.size() > 0) {
                    if (setUserIndex > 0) {
                        setUserIndex = setUserIndex - 1;
                        reviewUserName.setText(Data.endRidesCustomerInfo.get(setUserIndex).name);

                        Data.endRidesCustomerInfo.get(setUserIndex).image = Data.endRidesCustomerInfo.get(setUserIndex).image.replace("http://graph.facebook", "https://graph.facebook");
                        try {
                            Picasso.with(HomeActivity.this).load(Data.endRidesCustomerInfo.get(setUserIndex).image).skipMemoryCache().transform(new BlurTransform()).into(reviewUserImgBlured);
                        } catch (Exception e) {
                        }
                        try {
                            Picasso.with(HomeActivity.this).load(Data.endRidesCustomerInfo.get(setUserIndex).image).skipMemoryCache().transform(new CircleTransform()).into(reviewUserImage);
                        } catch (Exception e) {
                        }
                    }
                }


            }
        });
        nextImageRL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Data.endRidesCustomerInfo != null && Data.endRidesCustomerInfo.size() > 0) {
                    if (setUserIndex < (Data.endRidesCustomerInfo.size() - 1)) {
                        setUserIndex = setUserIndex + 1;
                        reviewUserName.setText(Data.endRidesCustomerInfo.get(setUserIndex).name);

                        Data.endRidesCustomerInfo.get(setUserIndex).image = Data.endRidesCustomerInfo.get(setUserIndex).image.replace("http://graph.facebook", "https://graph.facebook");
                        try {
                            Picasso.with(HomeActivity.this).load(Data.endRidesCustomerInfo.get(setUserIndex).image).skipMemoryCache().transform(new BlurTransform()).into(reviewUserImgBlured);
                        } catch (Exception e) {
                        }
                        try {
                            Picasso.with(HomeActivity.this).load(Data.endRidesCustomerInfo.get(setUserIndex).image).skipMemoryCache().transform(new CircleTransform()).into(reviewUserImage);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        });

        btnCollectCash.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GCMIntentService.clearNotifications(HomeActivity.this);

                if(btnCollectCash.getText().toString().equals(getString(R.string.collect_cash)))
                {
                        collectCashServerCall();
                }
                else
                {
                    startAutomaticReviewHandler();
                    belowLayoutRating.setVisibility(View.VISIBLE);
                }

            }
        });

        reviewFareInfoBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                fareInfoBtn.performClick();
            }
        });


        lastLocation = null;

        // map object initialized
        if (map != null) {
            map.getUiSettings().setZoomGesturesEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setMyLocationEnabled(true);
            map.getUiSettings().setTiltGesturesEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            //30.7500, 76.7800

            if (0 == Data.latitude && 0 == Data.longitude) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.7500, 76.7800), 14));
            } else {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Data.latitude, Data.longitude), 14));
            }


            // Find ZoomControl view
            View zoomControls = mapFragment.getView().findViewById(0x1);

            if (zoomControls != null && zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                // ZoomControl is inside of RelativeLayout
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls.getLayoutParams();

                // Align it to - parent top|left
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);

                // Update margins, set to 10dp
                final int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 250 * ASSL.Yscale(),
                        getResources().getDisplayMetrics());
                final int marginRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 20 * ASSL.Yscale(),
                        getResources().getDisplayMetrics());

                params.setMargins(0, marginTop, marginRight, 0);
            }


            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng arg0) {
                    Log.e("arg0", "=" + arg0);
                }
            });

            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                @Override
                public boolean onMarkerClick(Marker arg0) {
                    Log.e("arg0", "=" + arg0);
                    if (arg0.getTitle() != null) {
                        if (arg0.getTitle().equalsIgnoreCase("pickup location")) {

                            CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, "Your Start Location", "");
                            map.setInfoWindowAdapter(customIW);

                            return false;
                        } else if (arg0.getTitle().equalsIgnoreCase("customer_current_location")) {

                            CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, arg0.getSnippet(), "");
                            map.setInfoWindowAdapter(customIW);

                            return true;
                        } else if (arg0.getTitle().equalsIgnoreCase("start service location")) {

                            CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, "Start Location", "");
                            map.setInfoWindowAdapter(customIW);

                            return false;
                        } else if (arg0.getTitle().equalsIgnoreCase("driver position")) {

                            CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, "Driver Location", "");
                            map.setInfoWindowAdapter(customIW);

                            return false;
                        }else if (arg0.getTitle().equalsIgnoreCase("station_marker")) {
                            CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, arg0.getSnippet(), "");
                            map.setInfoWindowAdapter(customIW);
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            });

            new MapStateListener(map, mapFragment, this) {
                @Override
                public void onMapTouched() {
                    // Map touched

                }

                @Override
                public void onMapReleased() {
                    // Map released

                    if (myLocation != null) {
                        double difference  = MapUtils.displacement(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                                new LatLng(map.getCameraPosition().target.latitude , map.getCameraPosition().target.longitude));
                        Log.e("difference", ""+difference);
                        if (difference > 1.2 &&!(myLocation.getLongitude() == map.getCameraPosition().target.longitude &&
                                myLocation.getLatitude() == map.getCameraPosition().target.latitude)){
                            driverInitialMyLocationBtn.setVisibility(View.VISIBLE);
                            driverStartRideMyLocationBtn.setVisibility(View.VISIBLE);
                            driverEndRideMyLocationBtn.setVisibility(View.VISIBLE);
                        }else {
                            driverInitialMyLocationBtn.setVisibility(View.GONE);
                            driverStartRideMyLocationBtn.setVisibility(View.GONE);
                            driverEndRideMyLocationBtn.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onMapUnsettled() {

                }

                @Override
                public void onMapSettled() {
                    // Map settled

                }
            };



            driverInitialMyLocationBtn.setOnClickListener(mapMyLocationClick);
            driverStartRideMyLocationBtn.setOnClickListener(mapMyLocationClick);
            driverEndRideMyLocationBtn.setOnClickListener(mapMyLocationClick);


        }


        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


Log.i("driverScreenMode in on creste","=="+driverScreenMode);

            if (driverScreenMode == null) {
                driverScreenMode = DriverScreenMode.D_INITIAL;
            }
            switchUserScreen();

            startUIAfterGettingUserStatus();


            changeDriverMode(Data.userData.isAvailable);

            changeExceptionalDriverUI();

            Database2.getInstance(HomeActivity.this).insertDriverLocData(Data.userData.accessToken, Data.deviceToken, Data.SERVER_URL);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Database2.getInstance(HomeActivity.this).close();

        showManualPatchPushReceivedDialog();
    }


//





    public void startUIAfterGettingUserStatus() {

        switchDriverScreen(driverScreenMode);

    }


    public void sendToFareDetails() {
        FareDetails.helpSection = HelpSection.FARE_DETAILS;
        startActivity(new Intent(HomeActivity.this, FareDetails.class));
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        FlurryEventLogger.fareDetailsOpened(Data.userData.accessToken);
    }


    public void startWait(final boolean isHitRequired) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                waitChronometer.start();
                rideTimeChronometer.stop();
                driverWaitText.setBackgroundResource(R.drawable.secondary_btn_selector);
                driverWaitText.setText(getResources().getString(R.string.stop_wait));
                waitStart = 1;
                distanceAfterWaitStarted = 0;
                if (isHitRequired) {
                    startEndWaitAsync(HomeActivity.this, Data.dCustomerId, 1);
                }
            }
        });
    }


    public void stopWait(final boolean isHitRequired) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                waitChronometer.stop();
                rideTimeChronometer.start();
                driverWaitText.setBackgroundResource(R.drawable.main_btn_selector);
                driverWaitText.setText(getResources().getString(R.string.start_wait));
                waitStart = 0;
                if (isHitRequired) {
                    startEndWaitAsync(HomeActivity.this, Data.dCustomerId, 0);
                }
            }
        });
    }




    public void changeDriverModeToON(int mode) {
        if (mode == 1) {
            if (myLocation != null) {
                switchDriverOnThroughServer(1, new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
            } else {
                Toast.makeText(HomeActivity.this, getString(R.string.waiting_for_location), Toast.LENGTH_SHORT).show();
            }
        } else {
            switchDriverOnThroughServer(0, new LatLng(0, 0));
        }
    }


    public void switchDriverOnThroughServer(final int driverOnFlag, final LatLng latLng) {


        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogPopup.showLoadingDialog(HomeActivity.this, getString(R.string.loading));
                }
            });
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("access_token", Data.userData.accessToken));
                        nameValuePairs.add(new BasicNameValuePair("latitude", "" + latLng.latitude));
                        nameValuePairs.add(new BasicNameValuePair("longitude", "" + latLng.longitude));
                        nameValuePairs.add(new BasicNameValuePair("flag", "" + driverOnFlag));

                        Log.e("nameValuePairs in sending loc on driver toggle", "=" + nameValuePairs);

                        HttpRequester simpleJSONParser = new HttpRequester();
                        String result = simpleJSONParser.getJSONFromUrlParams(Data.SERVER_URL + "/change_availability", nameValuePairs);

                        Log.e("result ", "=" + result);

                        JSONObject jObj = new JSONObject(result);

                        if (jObj.has("flag")) {
                            int flag = jObj.getInt("flag");

                            if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                DialogPopup.dismissLoadingDialog();
                                HomeActivity.logoutUser(activity);
                            }
                            else if (ApiResponseFlags.ACTION_COMPLETE.getOrdinal() == flag) {
                                if (driverOnFlag == 1) {
                                    Data.userData.isAvailable = 1;
                                    changeDriverMode(1);
                                } else {
                                    Data.userData.isAvailable = 0;
                                    changeDriverMode(0);
                                }
                            }
                        }
                        if (jObj.has("message")) {
                            String message = jObj.getString("message");
                            showDialogFromBackground(message);
                        }
                        else
                        {
                            DialogPopup.dismissLoadingDialog();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }

    public void showDialogFromBackground(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogPopup.dismissLoadingDialog();
                new DialogPopup().alertPopup(HomeActivity.this, "", message);
            }
        });
    }

    public void switchDriverOn() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    DialogPopup.dismissLoadingDialog();
                    new DriverServiceOperations().startDriverService(HomeActivity.this);
                    driverONToggle.setImageResource(R.drawable.off);
                    driverOffLayout.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void switchDriverOff() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogPopup.dismissLoadingDialog();
                try {
                    new DriverServiceOperations().stopAndScheduleDriverService(HomeActivity.this);
                    driverONToggle.setImageResource(R.drawable.on);
                    driverOffLayout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public void changeExceptionalDriverUI() {
        driverONRl.setVisibility(View.VISIBLE);
        logoutRl.setVisibility(View.VISIBLE);

    }


    public void changeDriverMode(int mode) {
        Log.e("homeac changeDriverMode ====", "=" + mode);
        if (mode == 1) {
            switchDriverOn();
        } else {
            switchDriverOff();
        }
    }


    OnClickListener mapMyLocationClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (myLocation != null) {

                driverInitialMyLocationBtn.setVisibility(View.GONE);
                driverStartRideMyLocationBtn.setVisibility(View.GONE);
                driverEndRideMyLocationBtn.setVisibility(View.GONE);

                if (map.getCameraPosition().zoom < 12) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 12));
                } else if (map.getCameraPosition().zoom < 17) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 17));
                } else {
                    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())));
                }
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.waiting_for_location), Toast.LENGTH_LONG).show();
                reconnectLocationFetchers();
            }
        }
    };


    Handler reconnectionHandler = null;
    ;

    public void reconnectLocationFetchers() {
        if (reconnectionHandler == null) {
            disconnectGPSListener();
            destroyFusedLocationFetchers();
            reconnectionHandler = new Handler();
            reconnectionHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    connectGPSListener();
                    initializeFusedLocationFetchers();
                    reconnectionHandler.removeCallbacks(this);
                    reconnectionHandler = null;
                }
            }, 2000);
        }
    }


    public void setUserData() {
        try {
            userName.setText(Data.userData.userName);
            Data.userData.userImage = Data.userData.userImage.replace("http://graph.facebook", "https://graph.facebook");
            Log.i("User Image", "" + Data.userData.userImage);
            try {
                Picasso.with(HomeActivity.this).load(Data.userData.userImage).skipMemoryCache().transform(new CircleTransform()).into(profileImg);
            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setEndRideData()
    {
        mapLayout.setVisibility(View.GONE);
        endRideReviewRl.setVisibility(View.VISIBLE);

        if (Data.endRidesCustomerInfo.size() == 1) {
            nextImageRL.setVisibility(View.GONE);
            previousImageRL.setVisibility(View.GONE);
        } else {
            nextImageRL.setVisibility(View.VISIBLE);
            previousImageRL.setVisibility(View.VISIBLE);
        }
        endRideCustomersListAdapter = new EndRideCustomersListAdapter(activity);
        endRideCustomerListView.setAdapter(endRideCustomersListAdapter);
        totalCashToCollect = 0;


        for (int i = 0; i < Data.endRidesCustomerInfo.size(); i++) {

            if (Data.endRidesCustomerInfo.get(i).paymentMethod == 0)

            {
                totalCashToCollect = totalCashToCollect + Data.endRidesCustomerInfo.get(i).toPay;
            }

                if (Data.endRidesCustomerInfo.get(i).isPaymentSucessfull == 0 && Data.endRidesCustomerInfo.get(i).defaulterFlag == 0) {
                    makeOkButtonEnable = false;
                    break;

                } else {

                    makeOkButtonEnable = true;
                    makeDriverFree(HomeActivity.this);
                }


        }

        if(FeaturesConfigFile.isCollectCashEnable)
        {
            btnCollectCash.setBackgroundResource(R.drawable.main_btn_selector);
            btnCollectCash.setTextColor(getResources().getColorStateList(R.drawable.white_color_selector));
            btnCollectCash.setEnabled(true);
            if(makeOkButtonEnable)
            {
                btnCollectCash.setText(getString(R.string.ok));
                cancelCheckPaymentStatusTimer();
            }
            else
            {
                btnCollectCash.setText(getString(R.string.collect_cash));
                startCheckPaymentStatusTimer();
            }
        }
        else
        {
            btnCollectCash.setText(getString(R.string.ok));
            if (makeOkButtonEnable) {
            btnCollectCash.setBackgroundResource(R.drawable.main_btn_selector);
            btnCollectCash.setTextColor(getResources().getColorStateList(R.drawable.white_color_selector));
            btnCollectCash.setEnabled(true);
            cancelCheckPaymentStatusTimer();
        } else {
            btnCollectCash.setBackgroundResource(R.drawable.disable_grey_btn);
            btnCollectCash.setTextColor(getResources().getColorStateList(R.color.white));
            btnCollectCash.setEnabled(false);
            startCheckPaymentStatusTimer();
        }

        }




        double totalDistanceInMiles = Math.abs(totalDistance)* Double.parseDouble(getString(R.string.distance_conversion_factor));



        if (decimalFormat.format(totalDistanceInMiles).equalsIgnoreCase("1")) {
            reviewDistanceValue.setText("" + decimalFormat.format(totalDistanceInMiles) + " " + getString(R.string.distance_unit));
        } else {
            reviewDistanceValue.setText("" + decimalFormat.format(totalDistanceInMiles) + " " + getString(R.string.distance_unit)+"s");
        }



        reviewWaitValue.setText(waitTime + " min");
        if(Double.parseDouble(rideTime)==60)
        {
            reviewRideTimeValue.setText(" 1 Hr");
        }
        else if(Double.parseDouble(rideTime)>60 && Double.parseDouble(rideTime)<120)
        {
            reviewRideTimeValue.setText(""+(int)(Double.parseDouble(rideTime)/60)+" Hr "+(int)(Double.parseDouble(rideTime)%60)+" Min");
        }
        else if( Double.parseDouble(rideTime)>=120)
        {
            reviewRideTimeValue.setText(""+(int)(Double.parseDouble(rideTime)/60)+" Hrs "+(int)(Double.parseDouble(rideTime)%60)+" Min");
        }
        else
        {
            reviewRideTimeValue.setText(""+(int)(Double.parseDouble(rideTime)/1)+" Min");
        }

        reviewFareValue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US,"%.2f", totalFare)));

        reviewCO2Value.setText(String.valueOf(String.format(Locale.US,"%.2f", Co2_val) + " gm"));

        tvActualFareValue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US,"%.2f", totalFare)));
        tvDiscountValue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US,"%.2f", discount)));
        tvPAybleAmountVAlue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US,"%.2f", (totalFare-discount))));
        reviewRatingText.setText(getString(R.string.customer_rating));
        reviewRatingBar.setRating(0);

        reviewUserName.setText(Data.endRidesCustomerInfo.get(0).name);

        Data.endRidesCustomerInfo.get(0).image = Data.endRidesCustomerInfo.get(0).image.replace("http://graph.facebook", "https://graph.facebook");
        try {
            Picasso.with(HomeActivity.this).load(Data.endRidesCustomerInfo.get(0).image).skipMemoryCache().transform(new BlurTransform()).into(reviewUserImgBlured);
        } catch (Exception e) {
        }
        try {
            Picasso.with(HomeActivity.this).load(Data.endRidesCustomerInfo.get(0).image).skipMemoryCache().transform(new CircleTransform()).into(reviewUserImage);
        } catch (Exception e) {
        }
        setUserIndex = 0;


        setTextToFareInfoTextViews(reviewMinFareValue, reviewFareAfterValue, reviewFareAfterText);


        takeFareText.setText(getString(R.string.take_fare));


    }



    public void switchUserScreen() {



                Database2.getInstance(HomeActivity.this).updateUserMode(Database2.UM_DRIVER);


                driverMainLayout.setVisibility(View.VISIBLE);

        Database2.getInstance(HomeActivity.this).close();

    }


    public void updateDriverServiceFast(String choice) {
        Database2.getInstance(HomeActivity.this).updateDriverServiceFast(choice);
        Database2.getInstance(HomeActivity.this).close();
    }


    public void switchDriverScreen(final DriverScreenMode mode) {

            initializeFusedLocationFetchers();

            if (currentLocationMarker != null) {
                currentLocationMarker.remove();
            }

            saveDataOnPause(false);

            if (mode == DriverScreenMode.D_RIDE_END) {
                setEndRideData();
            } else {
                stopAutomaticReviewhandler();
                mapLayout.setVisibility(View.VISIBLE);
                endRideReviewRl.setVisibility(View.GONE);
            //    topRl.setBackgroundColor(getResources().getColor(R.color.nav_bar_color));
            }

Log.i("mode in switchDriver screen", "==" + mode);
            switch (mode) {

                case D_INITIAL:

                    totalDistance = -1;
                    totalFare = 0;
                    Co2_val = 0;
                    updateDriverServiceFast("no");

                    textViewDriverInfo.setVisibility(View.GONE);

                    driverInitialLayout.setVisibility(View.VISIBLE);
                    driverEngagedLayout.setVisibility(View.GONE);
                    driverStartRideNavigationBtn.setVisibility(View.GONE);

                    new DriverServiceOperations().checkStartService(HomeActivity.this);

                    cancelCustomerPathUpdateTimer();
                    cancelDestinationPathUpdateTimer();

                    if(myLocation != null){
                        if(map.getCameraPosition().zoom < 12){
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 12));
                        }
                        else if(map.getCameraPosition().zoom < 17){
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 17));
                        }
                        else{
                            map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())));
                        }
                    }if (map != null) {
                    map.clear();
                }

                    showAllRideRequestsOnMap();
                    cancelMapAnimateAndUpdateRideDataTimer();
                    cancelFareUpdateTimer();
                    searchBarLayoutDestination.setVisibility(View.GONE);
                    initializeStationDataProcedure();

                    linearSearchParent.setVisibility(View.GONE);

                    break;


                case D_START_RIDE:

                    updateDriverServiceFast("yes");

                    stopService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));
                    startService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));

                    if (map != null) {
                        map.clear();

                        markerOptionsCustomerPickupLocation = null;

                        markerOptionsCustomerPickupLocation = new MarkerOptions();
                        markerOptionsCustomerPickupLocation.title(Data.dEngagementId);
                        markerOptionsCustomerPickupLocation.snippet("");
                        markerOptionsCustomerPickupLocation.position(Data.dCustLatLng);
                        markerOptionsCustomerPickupLocation.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPassengerMarkerBitmap(HomeActivity.this, assl)));

                        map.addMarker(markerOptionsCustomerPickupLocation);
                    }


                    setAssignedCustomerInfoToViews();


                    driverInitialLayout.setVisibility(View.GONE);
                    driverEngagedLayout.setVisibility(View.VISIBLE);

                    driverStartRideMainRl.setVisibility(View.VISIBLE);
                    driverInRideMainRl.setVisibility(View.GONE);
                    driverWaitRl.setVisibility(View.VISIBLE);
                    driverStartRideNavigationBtn.setVisibility(View.VISIBLE);

                    startCustomerPathUpdateTimer();
                    cancelMapAnimateAndUpdateRideDataTimer();
                    cancelFareUpdateTimer();
                    cancelStationPathUpdateTimer();
                    cancelDestinationPathUpdateTimer();
                        if(FeaturesConfigFile.isArrivedStateEnable)
                        {
                                if(Data.isArrived==1)
                                {
                                    long timeR = (long) HomeActivity.timeTillArrived;
                                    int hR = (int) (timeR / 3600000);
                                    int mR = (int) (timeR - hR * 3600000) / 60000;
                                    int sR = (int) (timeR - hR * 3600000 - mR * 60000) / 1000;
                                    String hhR = hR < 10 ? "0" + hR : hR + "";
                                    String mmR = mR < 10 ? "0" + mR : mR + "";
                                    String ssR = sR < 10 ? "0" + sR : sR + "";
                                    driverArrivedChronometer.setText(hhR + ":" + mmR + ":" + ssR);

                                    driverArrivedChronometer.eclipsedTime = (long) HomeActivity.timeTillArrived;
                                    driverArrivedChronometer.start();
                                    linearArrivedTimeChronometer.setVisibility(View.VISIBLE);
                                    driverStartRideBtn.setVisibility(View.VISIBLE);
                                    driverArrivedBtn.setVisibility(View.GONE);
                                }
                            else
                                {
                                    linearArrivedTimeChronometer.setVisibility(View.GONE);
                                    driverStartRideBtn.setVisibility(View.GONE);
                                    driverArrivedBtn.setVisibility(View.VISIBLE);
                                }
                        }
                    else
                        {
                            linearArrivedTimeChronometer.setVisibility(View.GONE);
                            driverStartRideBtn.setVisibility(View.VISIBLE);
                            driverArrivedBtn.setVisibility(View.GONE);
                        }

                    linearSearchParent.setVisibility(View.GONE);

                    break;


                case D_IN_RIDE:

                    if(!searchStart) {
                        Log.i("mode in switchDriver screen", "==" + mode);
                        updateDriverServiceFast("no");

                        cancelCustomerPathUpdateTimer();
                        cancelStationPathUpdateTimer();


                        startMapAnimateAndUpdateRideDataTimer();
                        startFareUpdateTimer();


                        stopService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));

                        long timeR = (long) HomeActivity.previousRideTime;
                        int hR = (int) (timeR / 3600000);
                        int mR = (int) (timeR - hR * 3600000) / 60000;
                        int sR = (int) (timeR - hR * 3600000 - mR * 60000) / 1000;
                        String hhR = hR < 10 ? "0" + hR : hR + "";
                        String mmR = mR < 10 ? "0" + mR : mR + "";
                        String ssR = sR < 10 ? "0" + sR : sR + "";
                        rideTimeChronometer.setText(hhR + ":" + mmR + ":" + ssR);

                        rideTimeChronometer.eclipsedTime = (long) HomeActivity.previousRideTime;
                        rideTimeChronometer.start();


                        long time = (long) HomeActivity.previousWaitTime;
                        int h = (int) (time / 3600000);
                        int m = (int) (time - h * 3600000) / 60000;
                        int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                        String hh = h < 10 ? "0" + h : h + "";
                        String mm = m < 10 ? "0" + m : m + "";
                        String ss = s < 10 ? "0" + s : s + "";
                        waitChronometer.setText(hh + ":" + mm + ":" + ss);

                        waitChronometer.eclipsedTime = (long) HomeActivity.previousWaitTime;
                        if (Data.isWaitStart == 1) {
                            startWait(false);
                        } else {
                            stopWait(false);
                        }

                        if (map != null) {
                            map.clear();
                            markerOptionsCustomerPickupLocation = null;
                        }
                        if (!Prefs.with(getApplicationContext()).getString("destinationAddress", "").equalsIgnoreCase("")) {
                            searchEtDestination.setText(Prefs.with(getApplicationContext()).getString("destinationAddress", ""));

                            if (!Prefs.with(getApplicationContext()).getString("destinationLatitude", "").equals("") && !Prefs.with(getApplicationContext()).getString("destinationLongitude", "").equals("")) {
                                Data.dDestinationLatLng = new LatLng(Double.parseDouble(Prefs.with(getApplicationContext()).getString("destinationLatitude", "")), Double.parseDouble(Prefs.with(getApplicationContext()).getString("destinationLongitude", "")));
                            }
                            Log.i("Data.dDestinationLatLng", "=" + Data.dDestinationLatLng);
                            startDestinationPathUpdateTimer();
                        }

                        updateDistanceFareTexts(false);

                        setAssignedCustomerInfoToViews();

                        setTextToFareInfoTextViews(inrideMinFareValue, inrideFareAfterValue, inrideFareAfterText);

                    }

                    searchStart = false;
                    driverStartRideNavigationBtn.setVisibility(View.GONE);
//                    callCustomerRl.setVisibility(View.GONE);
                    driverPassengerName.setVisibility(View.GONE);
                    searchBarLayoutDestination.setVisibility(View.VISIBLE);
                    driverInitialLayout.setVisibility(View.GONE);
                    driverEngagedLayout.setVisibility(View.VISIBLE);
                    driverScheduledRideText.setVisibility(View.GONE);
                    driverWaitRl.setVisibility(View.VISIBLE);
                    driverStartRideMainRl.setVisibility(View.GONE);
                    driverInRideMainRl.setVisibility(View.VISIBLE);
                    linearSearchParent.setVisibility(View.GONE);

                    break;

                case D_SEARCH:

                    linearSearchParent.setVisibility(View.VISIBLE);
                    searchBarLayoutDestination.setVisibility(View.INVISIBLE);
                    driverEngagedLayout.setVisibility(View.INVISIBLE);
                    driverWaitRl.setVisibility(View.INVISIBLE);
                    driverInRideMainRl.setVisibility(View.INVISIBLE);

                    break;


                case D_RIDE_END:

                    linearSearchParent.setVisibility(View.GONE);
                    updateDriverServiceFast("no");
                    feedbackRatingBar.setRating(5);
                    cancelMapAnimateAndUpdateRideDataTimer();
                    cancelFareUpdateTimer();
                    cancelStationPathUpdateTimer();
                    cancelDestinationPathUpdateTimer();

                    stopService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));
                    startService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));

                    driverInitialLayout.setVisibility(View.GONE);
                    driverEngagedLayout.setVisibility(View.GONE);
                    driverStartRideNavigationBtn.setVisibility(View.GONE);
                    driverWaitRl.setVisibility(View.GONE);
                    Data.dDestinationLatLng = null;

                    break;


                default:
                    driverInitialLayout.setVisibility(View.VISIBLE);
                    driverEngagedLayout.setVisibility(View.GONE);
                    driverStartRideNavigationBtn.setVisibility(View.GONE);
            }

    }

    public void setAssignedCustomerInfoToViews() {
        double rateingD = 4;
        try {
            rateingD = Double.parseDouble(Data.assignedCustomerInfo.rating);
        } catch (Exception e) {
            e.printStackTrace();
        }

        driverPassengerName.setText(Data.assignedCustomerInfo.name);
        driverBelowPassengerName.setText(Data.assignedCustomerInfo.name);
        driverPassengerRatingValue.setText(decimalFormat.format(rateingD) + " " + getResources().getString(R.string.rating));

        if ("".equalsIgnoreCase(Data.assignedCustomerInfo.schedulePickupTime)) {
            driverScheduledRideText.setVisibility(View.GONE);
        } else {
            String time = DateOperations.getTimeAMPM(DateOperations.utcToLocal(Data.assignedCustomerInfo.schedulePickupTime));
            if ("".equalsIgnoreCase(time)) {
                driverScheduledRideText.setVisibility(View.GONE);
            } else {
                driverScheduledRideText.setVisibility(View.VISIBLE);
                driverScheduledRideText.setText(getString(R.string.scheduled_ride_pickup) + time);
            }
        }

        if (1 == Data.assignedCustomerInfo.freeRide) {
            driverFreeRideIcon.setVisibility(View.VISIBLE);
        } else {
            driverFreeRideIcon.setVisibility(View.GONE);
        }

    }


    public void setTextToFareInfoTextViews(TextView minFareValue, TextView fareAfterValue, TextView fareAfterText) {


        String mileStr="";

        if(decimalFormat.format(Data.fareStructure.thresholdDistance) .equalsIgnoreCase("1")) {
            mileStr=" "+getString(R.string.distance_unit);
        }
        else
        {
            mileStr=" "+getString(R.string.distance_unit)+"s";
        }
        minFareValue.setText(getResources().getString(R.string.currency_symbol) + decimalFormat.format(Data.fareStructure.fixedFare) + " for "
                + decimalFormat.format(Data.fareStructure.thresholdDistance) +mileStr);

        fareAfterValue.setText(getResources().getString(R.string.currency_symbol) + decimalFormat.format(Data.fareStructure.farePerKm) + " per " +getString(R.string.distance_unit) + " + "+getResources().getString(R.string.currency_symbol)
                + decimalFormat.format(Data.fareStructure.farePerMin) + " per min");

        SpannableString sstr = new SpannableString("Fare");
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        sstr.setSpan(bss, 0, sstr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        fareAfterText.setText("");
        fareAfterText.append(sstr);
        fareAfterText.append(" (after " + decimalFormat.format(Data.fareStructure.thresholdDistance) + mileStr + ")");
    }





    @Override
    public synchronized void onGPSLocationChanged(Location location) {
         drawLocationChanged(location);
    }


    void buildAlertMessageNoGps() {
        if (!(((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                ((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
            if (gpsDialogAlert != null && gpsDialogAlert.isShowing()) {
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.enable_gps_message))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                ;
                gpsDialogAlert = null;
                gpsDialogAlert = builder.create();
                gpsDialogAlert.show();
            }
        } else {
            if (gpsDialogAlert != null && gpsDialogAlert.isShowing()) {
                gpsDialogAlert.dismiss();
            }
        }
    }

    public Dialog timeDialogAlert;

    @SuppressWarnings("deprecation")
    public void buildTimeSettingsAlertDialog(final Activity activity) {
        try {
            int autoTime = android.provider.Settings.System.getInt(activity.getContentResolver(), android.provider.Settings.System.AUTO_TIME);
            if (autoTime == 0) {
                if (timeDialogAlert != null && timeDialogAlert.isShowing()) {
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage(getString(R.string.correct_time_message))
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {
                                    activity.startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
                                }
                            })
                    ;
                    timeDialogAlert = null;
                    timeDialogAlert = builder.create();
                    timeDialogAlert.show();
                }
            } else {
                if (timeDialogAlert != null && timeDialogAlert.isShowing()) {
                    timeDialogAlert.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean checkIfUserDataNull(Activity activity) {
        Log.e("checkIfUserDataNull", "Data.userData = " + Data.userData);
        if (Data.userData == null) {
            activity.startActivity(new Intent(activity, SplashNewActivity.class));
            activity.finish();
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        } else {
            return false;
        }
    }


    public void initializeGPSForegroundLocationFetcher() {
        if (gpsForegroundLocationFetcher == null) {
            gpsForegroundLocationFetcher = new GPSForegroundLocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD);
        }
    }

    public void connectGPSListener() {
        disconnectGPSListener();
        try {
            initializeGPSForegroundLocationFetcher();
            gpsForegroundLocationFetcher.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnectGPSListener() {
        try {
            if (gpsForegroundLocationFetcher != null) {
                gpsForegroundLocationFetcher.destroy();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (!checkIfUserDataNull(HomeActivity.this)) {
            setUserData();

            connectGPSListener();

            initializeFusedLocationFetchers();


                buildTimeSettingsAlertDialog(this);

        }
    }


    @Override
    protected void onPause() {

        GCMIntentService.clearNotifications(getApplicationContext());
        saveDataOnPause(false);

        try {

                if (driverScreenMode != DriverScreenMode.D_IN_RIDE) {
                    disconnectGPSListener();
                    destroyFusedLocationFetchers();
                }

        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }


    @Override
    public void onBackPressed() {
        try {

                if (driverScreenMode == DriverScreenMode.D_IN_RIDE || driverScreenMode == DriverScreenMode.D_START_RIDE) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    startActivity(intent);
                }else if (DriverScreenMode.D_SEARCH == driverScreenMode) {

                    searchStart = true;
                    driverScreenMode = DriverScreenMode.D_IN_RIDE;
                switchDriverScreen(driverScreenMode);
            }else {
                    ActivityCompat.finishAffinity(this);
//					super.onBackPressed();
                }

        } catch (Exception e) {
            e.printStackTrace();
            ActivityCompat.finishAffinity(this);
//			super.onBackPressed();
        }
    }


    @Override
    public void onDestroy() {
        try {
            if (createPathAsyncTasks != null) {
                createPathAsyncTasks.clear();
            }
            saveDataOnPause(true);

            GCMIntentService.clearNotifications(HomeActivity.this);
            GCMIntentService.stopRing();

            disconnectGPSListener();

            destroyFusedLocationFetchers();

            ASSL.closeActivity(drawerLayout);


            appInterruptHandler = null;

            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }


    public void saveDataOnPause(final boolean stopWait) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
                        Editor editor = pref.edit();

                        if (driverScreenMode == DriverScreenMode.D_START_RIDE) {

                            editor.putString(Data.SP_DRIVER_SCREEN_MODE, Data.D_START_RIDE);

                            editor.putString(Data.SP_D_ENGAGEMENT_ID, Data.dEngagementId);
                            editor.putString(Data.SP_D_CUSTOMER_ID, Data.dCustomerId);

                            editor.putString(Data.SP_D_LATITUDE, "" + Data.dCustLatLng.latitude);
                            editor.putString(Data.SP_D_LONGITUDE, "" + Data.dCustLatLng.longitude);

                            editor.putString(Data.SP_D_CUSTOMER_NAME, Data.assignedCustomerInfo.name);
                            editor.putString(Data.SP_D_CUSTOMER_IMAGE, Data.assignedCustomerInfo.image);
                            editor.putString(Data.SP_D_CUSTOMER_PHONE, Data.assignedCustomerInfo.phoneNumber);
                            editor.putString(Data.SP_D_CUSTOMER_RATING, Data.assignedCustomerInfo.rating);

                        } else if (driverScreenMode == DriverScreenMode.D_IN_RIDE) {

                            if (stopWait) {
                                if (waitChronometer.isRunning) {
                                    stopWait(true);
                                }
                                if (rideTimeChronometer.isRunning) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                rideTimeChronometer.stop();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }

                            editor.putString(Data.SP_DRIVER_SCREEN_MODE, Data.D_IN_RIDE);

                            editor.putString(Data.SP_D_ENGAGEMENT_ID, Data.dEngagementId);
                            editor.putString(Data.SP_D_CUSTOMER_ID, Data.dCustomerId);

                            editor.putString(Data.SP_D_CUSTOMER_NAME, Data.assignedCustomerInfo.name);
                            editor.putString(Data.SP_D_CUSTOMER_IMAGE, Data.assignedCustomerInfo.image);
                            editor.putString(Data.SP_D_CUSTOMER_PHONE, Data.assignedCustomerInfo.phoneNumber);
                            editor.putString(Data.SP_D_CUSTOMER_RATING, Data.assignedCustomerInfo.rating);

                            long elapsedMillis = waitChronometer.eclipsedTime;

                            editor.putString(Data.SP_TOTAL_DISTANCE, "" + totalDistance);
                            editor.putString(Data.SP_WAIT_TIME, "" + elapsedMillis);

                            long elapsedRideTime = rideTimeChronometer.eclipsedTime;
                            editor.putString(Data.SP_RIDE_TIME, "" + elapsedRideTime);

                            if (HomeActivity.this.lastLocation != null) {
                                editor.putString(Data.SP_LAST_LATITUDE, "" + HomeActivity.this.lastLocation.getLatitude());
                                editor.putString(Data.SP_LAST_LONGITUDE, "" + HomeActivity.this.lastLocation.getLongitude());
                            }

                            Log.e("Data on app paused", "-----");
                            Log.i("HomeActivity.totalDistance", "=" + HomeActivity.totalDistance);
                            Log.i("lastLocation", "=" + lastLocation);
                            Log.e("----------", "-----");

                        } else {
                            editor.putString(Data.SP_DRIVER_SCREEN_MODE, "");
                        }


                        editor.commit();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


    public synchronized void drawLocationChanged(Location location){
        try {
            if(map != null){
                HomeActivity.myLocation = location;
                zoomToCurrentLocationAtFirstLocationFix(location);


                if(driverScreenMode == DriverScreenMode.D_IN_RIDE ){

                    final LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

                    if(Utils.compareDouble(totalDistance, -1.0) == 0){
                        lastLocation = null;
                        Log.i("lastLocation made null", "="+lastLocation);
                    }

                    Log.i("lastLocation", "="+lastLocation);
                    Log.i("totalDistance", "="+totalDistance);

                    writePathLogToFile("lastLocation = "+lastLocation);
                    writePathLogToFile("totalDistance = "+totalDistance);

                    if(lastLocation != null){
                        final LatLng lastLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                        addLatLngPathToDistance(lastLatLng, currentLatLng);
                    }
                    else{
                        if(Utils.compareDouble(totalDistance, -1.0) == 0){
                            totalDistance = 0;
                        }
                        displayOldPath();
                        writePathLogToFile("Data.startRidePreviousLatLng = "+Data.startRidePreviousLatLng);
                        addLatLngPathToDistance(Data.startRidePreviousLatLng, currentLatLng);
                    }

                    lastLocation = location;

                    saveDataOnPause(false);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public synchronized void writePathLogToFile(String text) {
        try {
            if (DriverScreenMode.D_IN_RIDE == driverScreenMode) {
                Log.writePathLogToFile(Data.dEngagementId, text);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized void addLatLngPathToDistance(final LatLng lastLatLng, final LatLng currentLatLng) {
        try {
            double displacement = MapUtils.displacement(lastLatLng, currentLatLng);
            Log.i("displacement", "=" + displacement);

            writePathLogToFile("lastLatLng = " + lastLatLng + ", currentLatLng = " + currentLatLng);
            writePathLogToFile("displacement = " + displacement);

            if (Utils.compareDouble(displacement, MAX_DISPLACEMENT_THRESHOLD) == -1) {

                boolean validDistance = updateTotalDistance(lastLatLng, currentLatLng, displacement);
                if (validDistance) {
                    checkAndUpdateWaitTimeDistance(displacement);
                    map.addPolyline(new PolylineOptions()
                            .add(lastLatLng, currentLatLng)
                            .width(5)
                            .color(MAP_PATH_COLOR).geodesic(true));
                    logPathDataToFlurry(currentLatLng, totalDistance);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Database.getInstance(HomeActivity.this).insertPolyLine(lastLatLng, currentLatLng);
                            Database.getInstance(HomeActivity.this).close();
                        }
                    }).start();
                }

                updateDistanceFareTexts(false);
            } else {
                callGooglePathAPI(lastLatLng, currentLatLng, displacement);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    ArrayList<CreatePathAsyncTask> createPathAsyncTasks = new ArrayList<HomeActivity.CreatePathAsyncTask>();

    public synchronized void callGooglePathAPI(LatLng lastLatLng, LatLng currentLatLng, double displacement) {
        if (createPathAsyncTasks == null) {
            createPathAsyncTasks = new ArrayList<HomeActivity.CreatePathAsyncTask>();
        }
        CreatePathAsyncTask createPathAsyncTask = new CreatePathAsyncTask(lastLatLng, currentLatLng, displacement);
        if (!createPathAsyncTasks.contains(createPathAsyncTask)) {
            createPathAsyncTasks.add(createPathAsyncTask);
            createPathAsyncTask.execute();
        }
    }

    class CreatePathAsyncTask extends AsyncTask<Void, Void, String> {
        String url;
        double displacementToCompare;
        LatLng source, destination;

        CreatePathAsyncTask(LatLng source, LatLng destination, double displacementToCompare) {
            this.source = source;
            this.destination = destination;
            this.url = MapUtils.makeDirectionsURL(source, destination);
            this.displacementToCompare = displacementToCompare;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            return new HttpRequester().getJSONFromUrl(url);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            updateDistanceFareTexts(false);
            if (result != null) {
                drawPath(result, displacementToCompare, source, destination);

            }
            createPathAsyncTasks.remove(this);
        }


        @Override
        public boolean equals(Object o) {
            try {
                if ((((CreatePathAsyncTask) o).source == this.source) && (((CreatePathAsyncTask) o).destination == this.destination)) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }


    public void logPathDataToFlurry(LatLng latLng, double totalDistance) {
        try {
            if (DriverScreenMode.D_IN_RIDE == driverScreenMode) {
                FlurryEventLogger.logRideData(Data.userData.accessToken, Data.dEngagementId, latLng, totalDistance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void logPathDataToFlurryGAPI(LatLng latLng1, LatLng latLng2, double totalDistance) {
        try {
            if ( DriverScreenMode.D_IN_RIDE == driverScreenMode) {
                FlurryEventLogger.logRideDataGAPI(Data.userData.accessToken, Data.dEngagementId, latLng1, latLng2, totalDistance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void checkAndUpdateWaitTimeDistance(final double distance) {
        try {
            if (waitStart == 1) {
                distanceAfterWaitStarted = distanceAfterWaitStarted + distance;
                if (distanceAfterWaitStarted >= MAX_WAIT_TIME_ALLOWED_DISTANCE) {
                    stopWait(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized void updateDistanceFareTexts(boolean waitStarted) {
        double totalDistanceInMiles = Math.abs(totalDistance)*Double.parseDouble(getString(R.string.distance_conversion_factor));
        //int m = (int) Math.ceil((rideTimeChronometer.eclipsedTime ) / 60000);
        double totalTimeInMin = Math.ceil((rideTimeChronometer.eclipsedTime ) / 60000) + 1.0;
        Log.e("totalTimeInMin", "=" + totalTimeInMin);

        Log.e("rideTimeChronometer eclipsed time value", "" + rideTimeChronometer.eclipsedTime);
        Log.e("rideTimeChronometer eclipsed time value/6000", "" + rideTimeChronometer.eclipsedTime / 60000);
        //Log.e("m value", "=" + m);
        //int waitMin = (int) Math.ceil((waitChronometer.eclipsedTime ) / 60000);
        double waitTimeInMin = 0;
        Log.e("waitStart", "=" + waitStart);
        if(waitStarted || !waitChronometer.getText().toString().equals("00:00:00")){
            waitTimeInMin = Math.ceil((waitChronometer.eclipsedTime ) / 60000) + 1.0;
        }

        Log.e("waitTimeChronometer eclipsed time value", "" + waitChronometer.eclipsedTime);
        Log.e("waitTimeChronometer eclipsed time value/60000", "" + waitChronometer.eclipsedTime / 60000);
        Log.e("waitTimeInMin", "=" + waitTimeInMin);

        if(decimalFormat.format(totalDistanceInMiles).equalsIgnoreCase("1"))
        {
            driverIRDistanceValue.setText("" + decimalFormat.format(totalDistanceInMiles)+" "+getString(R.string.distance_unit));
        }
        else
        {
            driverIRDistanceValue.setText("" + decimalFormat.format(totalDistanceInMiles)+" "+getString(R.string.distance_unit)+"s");
        }

        driverIRFareValue.setText(getString(R.string.currency_symbol)+ decimalFormat.format(Data.fareStructure.calculateFare(totalDistanceInMiles, totalTimeInMin, waitTimeInMin)));
    }


    public synchronized void displayOldPath() {

        try {
            ArrayList<Pair<LatLng, LatLng>> path = Database.getInstance(HomeActivity.this).getSavedPath();
            Database.getInstance(HomeActivity.this).close();

            LatLng firstLatLng = null;

            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.width(5);
            polylineOptions.color(MAP_PATH_COLOR);
            polylineOptions.geodesic(true);

            for (Pair<LatLng, LatLng> pair : path) {
                LatLng src = pair.first;
                LatLng dest = pair.second;

                if (firstLatLng == null) {
                    firstLatLng = src;
                }

                polylineOptions.add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude));
            }

            map.addPolyline(polylineOptions);

            if (firstLatLng == null) {
                firstLatLng = Data.startRidePreviousLatLng;
            }

            if (firstLatLng != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.snippet("");
                markerOptions.title("start ride location");
                markerOptions.position(firstLatLng);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPinMarkerBitmap(HomeActivity.this, assl)));
                map.addMarker(markerOptions);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public synchronized boolean updateTotalDistance(LatLng lastLatLng, LatLng currentLatLng, double deltaDistance) {
        boolean validDistance = false;
        if (deltaDistance > 0.0) {
            LatLngPair latLngPair = new LatLngPair(lastLatLng, currentLatLng, deltaDistance);

            Log.e("latLngPair to add", "=" + latLngPair);

            if (HomeActivity.deltaLatLngPairs == null) {
                HomeActivity.deltaLatLngPairs = new ArrayList<LatLngPair>();
            }

            if (!HomeActivity.deltaLatLngPairs.contains(latLngPair)) {
                totalDistance = totalDistance + deltaDistance;
                HomeActivity.deltaLatLngPairs.add(latLngPair);
                validDistance = true;
            }
        }
        Log.e("HomeActivity.deltaLatLngPairs", "=" + HomeActivity.deltaLatLngPairs);
        return validDistance;
    }


    public synchronized void drawPath(String result, double displacementToCompare, LatLng source, LatLng destination) {
        try {
            writePathLogToFile("GAPI source = " + source + ", destination = " + destination);
            final JSONObject json = new JSONObject(result);

            JSONObject leg0 = json.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0);
            double distanceOfPath = leg0.getJSONObject("distance").getDouble("value");

            writePathLogToFile("GAPI distanceOfPath = " + distanceOfPath);

            if (Utils.compareDouble(distanceOfPath, (displacementToCompare * 1.8)) <= 0) {                                                        // distance would be approximately correct

                JSONArray routeArray = json.getJSONArray("routes");
                JSONObject routes = routeArray.getJSONObject(0);
                JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
                String encodedString = overviewPolylines.getString("points");
                List<LatLng> list = MapUtils.decodeDirectionsPolyline(encodedString);

//			    	totalDistance = totalDistance + distanceOfPath;
                boolean validDistance = updateTotalDistance(source, destination, distanceOfPath);
                if (validDistance) {
                    checkAndUpdateWaitTimeDistance(distanceOfPath);

                    for (int z = 0; z < list.size() - 1; z++) {
                        LatLng src = list.get(z);
                        LatLng dest = list.get(z + 1);
                        map.addPolyline(new PolylineOptions()
                                .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude))
                                .width(5)
                                .color(MAP_PATH_COLOR).geodesic(true));
                        Database.getInstance(this).insertPolyLine(src, dest);
                    }
                    Database.getInstance(this).close();
                }
            } else {                                                                                                    // displacement would be correct
//	    		totalDistance = totalDistance + displacementToCompare;
                boolean validDistance = updateTotalDistance(source, destination, displacementToCompare);
                if (validDistance) {
                    checkAndUpdateWaitTimeDistance(displacementToCompare);

                    map.addPolyline(new PolylineOptions()
                            .add(new LatLng(source.latitude, source.longitude), new LatLng(destination.latitude, destination.longitude))
                            .width(5)
                            .color(MAP_PATH_COLOR).geodesic(true));
                    Database.getInstance(this).insertPolyLine(source, destination);
                    Database.getInstance(this).close();
                }

            }
            writePathLogToFile("totalDistance after GAPI = " + totalDistance);
            logPathDataToFlurryGAPI(source, destination, totalDistance);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    class ViewHolderDriverRequest {
        TextView textViewRequestAddress, textViewRequestDistance, textViewRequestTime, textViewRequestNumber,text_accept, requestName;
        RelativeLayout relative,circularProgressbar_bg;
        ImageView requestImg, imgRatingStar1, imgRatingStar2, imgRatingStar3, imgRatingStar4, imgRatingStar5;
        int id;
        Button decline;
        ProgressBar circularProgressbar;
    }

    class DriverRequestListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderDriverRequest holder;

        DateOperations dateOperations;

        public DriverRequestListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            dateOperations = new DateOperations();
        }

        public DateOperations getDateOperations() {
            if (dateOperations == null) {
                dateOperations = new DateOperations();
            }
            return dateOperations;
        }

        @Override
        public int getCount() {
            return Data.driverRideRequests.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {

                holder = new ViewHolderDriverRequest();
                convertView = mInflater.inflate(R.layout.list_item_driver_request, null);

                holder.textViewRequestAddress = (TextView) convertView.findViewById(R.id.textViewRequestAddress);
                holder.textViewRequestAddress.setTypeface(Data.getFont(getApplicationContext()));
                holder.textViewRequestDistance = (TextView) convertView.findViewById(R.id.textViewRequestDistance);
                holder.textViewRequestDistance.setTypeface(Data.getFont(getApplicationContext()));
                holder.textViewRequestTime = (TextView) convertView.findViewById(R.id.textViewRequestTime);
                holder.textViewRequestTime.setTypeface(Data.getFont(getApplicationContext()));
                holder.textViewRequestNumber = (TextView) convertView.findViewById(R.id.textViewRequestNumber);
                holder.textViewRequestNumber.setTypeface(Data.getFont(getApplicationContext()));

                holder.text_accept= (TextView) convertView.findViewById(R.id.text_accept);
                holder.text_accept.setTypeface(Data.getFont(getApplicationContext()));
                holder.decline = (Button) convertView.findViewById(R.id.decline);
                holder.decline.setTypeface(Data.getFont(getApplicationContext()));

                holder.circularProgressbar = (ProgressBar) convertView.findViewById(R.id.circularProgressbar);

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                Drawable myIcon;
                if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP){
                    // Do something for froyo and above versions
                    myIcon = getResources().getDrawable(R.drawable.progress_bar_bg );
                } else{
                    // do something for phones running an SDK before LOLLIPOP
                    myIcon = getResources().getDrawable(R.drawable.progress_bar_pre_lollipop);

                }
                holder.circularProgressbar.setProgressDrawable(myIcon);


                holder.circularProgressbar_bg = (RelativeLayout) convertView.findViewById(R.id.circularProgressbar_bg);
                holder.circularProgressbar_bg.setTag(holder);



                holder.requestName = (TextView) convertView.findViewById(R.id.requestName);
                holder.requestName.setTypeface(Data.getFont(getApplicationContext()));
                holder.requestImg = (ImageView) convertView.findViewById(R.id.requestImg);


                holder.imgRatingStar1 = (ImageView) convertView.findViewById(R.id.imgRatingStar1);
                holder.imgRatingStar2 = (ImageView) convertView.findViewById(R.id.imgRatingStar2);
                holder.imgRatingStar3 = (ImageView) convertView.findViewById(R.id.imgRatingStar3);
                holder.imgRatingStar4 = (ImageView) convertView.findViewById(R.id.imgRatingStar4);
                holder.imgRatingStar5 = (ImageView) convertView.findViewById(R.id.imgRatingStar5);

                holder.relative = (RelativeLayout) convertView.findViewById(R.id.relative);
                holder.relative.setTag(holder);
                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderDriverRequest) convertView.getTag();
            }


            try {
                DriverRideRequest driverRideRequest = Data.driverRideRequests.get(position);

                holder.id = position;

                holder.textViewRequestNumber.setText("" + (position + 1));
                holder.textViewRequestAddress.setText(driverRideRequest.address);
                holder.requestName.setText(driverRideRequest.customerName);
                try {
                    Picasso.with(HomeActivity.this).load(driverRideRequest.customerImage).transform(new CircleTransform()).into(holder.requestImg);
                } catch (Exception e) {
                }


                holder.imgRatingStar1.setBackgroundResource(R.drawable.small_star_empty);
                holder.imgRatingStar2.setBackgroundResource(R.drawable.small_star_empty);
                holder.imgRatingStar3.setBackgroundResource(R.drawable.small_star_empty);
                holder.imgRatingStar4.setBackgroundResource(R.drawable.small_star_empty);
                holder.imgRatingStar5.setBackgroundResource(R.drawable.small_star_empty);
                for (int i = 1; i <= driverRideRequest.customerRating; i++) {
                    switch (i) {
                        case 1:
                            holder.imgRatingStar1.setBackgroundResource(R.drawable.small_star_filled);
                            break;
                        case 2:
                            holder.imgRatingStar2.setBackgroundResource(R.drawable.small_star_filled);
                            break;
                        case 3:
                            holder.imgRatingStar3.setBackgroundResource(R.drawable.small_star_filled);
                            break;
                        case 4:
                            holder.imgRatingStar4.setBackgroundResource(R.drawable.small_star_filled);
                            break;
                        case 5:
                            holder.imgRatingStar5.setBackgroundResource(R.drawable.small_star_filled);
                            break;


                    }
                }


                long timeDiff = getDateOperations().getTimeDifference(DateOperations.getCurrentTime(), driverRideRequest.startTime);
                int timeDiffInSec = (int) (timeDiff / 1000);
                Log.i("timeDiff", "==" + timeDiff);
                Log.i("timeDiffInSec", "==" + timeDiffInSec);
                if (timeDiffInSec <= 0) {
                    if (timer != null) {
                        timer.cancel();
                        timerTask.cancel();
                    }
                    Data.driverRideRequests.remove(new DriverRideRequest(driverRideRequest.engagementId,""));
                    driverRequestListAdapter.notifyDataSetChanged();
                    showAllRideRequestsOnMap();
                }
                holder.textViewRequestTime.setText("" + timeDiffInSec + " sec left");
                holder.circularProgressbar.setProgress(timeDiffInSec);

                if (myLocation != null) {
                    holder.textViewRequestDistance.setVisibility(View.VISIBLE);

                    if(decimalFormat.format(driverRideRequest.distance).equalsIgnoreCase("1")){
                        holder.textViewRequestDistance.setText("1 "+getString(R.string.distance_unit)+" away");
                    }
                    else{
                        holder.textViewRequestDistance.setText(""+decimalFormat.format(driverRideRequest.distance)+" "+getString(R.string.distance_unit)+"s away");
                    }
                } else {
                    holder.textViewRequestDistance.setVisibility(View.GONE);
                }
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                showAllRideRequestsOnMap();
            }

            holder.circularProgressbar_bg.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                        holder = (ViewHolderDriverRequest) v.getTag();
                        DriverRideRequest driverRideRequest = Data.driverRideRequests.get(position);
                        Data.dEngagementId = driverRideRequest.engagementId;
                        Data.dCustomerId = driverRideRequest.customerId;
                        Data.dCustLatLng = driverRideRequest.latLng;
                                map.animateCamera(CameraUpdateFactory.newLatLng(driverRideRequest.latLng), 1000, null);

                                if (getBatteryPercentage() >= 10) {
                                    GCMIntentService.clearNotifications(HomeActivity.this);
                                    GCMIntentService.stopRing();
                                    driverAcceptRideAsync(HomeActivity.this);
                                } else {
                                    new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.low_battery_warning));
                                }
                        map.animateCamera(CameraUpdateFactory.newLatLng(driverRideRequest.latLng), 1000, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                        }
                    });
                }
            });

            holder.decline.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    try {


                                        DriverRideRequest driverRideRequest = Data.driverRideRequests.get(position);

                                        Data.dEngagementId = driverRideRequest.engagementId;
                                        Data.dCustomerId = driverRideRequest.customerId;
                                        Data.dCustLatLng = driverRideRequest.latLng;
                                        GCMIntentService.clearNotifications(HomeActivity.this);
                                        GCMIntentService.stopRing();
                                        driverRejectRequestAsync(HomeActivity.this);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }
                            }
                    );

                }
            });

            return convertView;
        }


    }



    public float getBatteryPercentage() {
        try {
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = registerReceiver(null, ifilter);
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float batteryPct = (level / (float) scale) * 100;

            // Are we charging / charged?
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;
            if (isCharging) {
                return 70;
            } else {
                return batteryPct;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 70;
        }
    }




    /**
     * ASync for change driver mode from server
     */
    public void driverAcceptRideAsync(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();

            if (myLocation != null) {
                Data.latitude = myLocation.getLatitude();
                Data.longitude = myLocation.getLongitude();
            }


            params.put("access_token", Data.userData.accessToken);
            params.put("customer_id", Data.dCustomerId);
            params.put("engagement_id", Data.dEngagementId);
            params.put("latitude", "" + Data.latitude);
            params.put("longitude", "" + Data.longitude);

            Log.e("accept ride api", "=");
            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("user_id", "=" + Data.dCustomerId);
            Log.i("engage_id", "=" + Data.dEngagementId);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);


            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/accept_a_request", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail acccept request", arg3.toString());
//							new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                            DialogPopup.dismissLoadingDialog();
                            callAndHandleStateRestoreAPI();
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.i("accept ride api Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    int flag = jObj.getInt("flag");
                                    Log.e("accept_a_request flag", "=" + flag);
                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (112 == flag) {
                                        if (driverScreenMode == DriverScreenMode.D_START_RIDE ) {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }

//									response = {"error":"Request timed out","flag":10}
                                    DialogPopup.dismissLoadingDialog();

                                    reduceRideRequest(activity, Data.dEngagementId);

                                } else {

                                    if (jObj.has("flag")) {
                                        try {
                                            int flag = jObj.getInt("flag");
                                            Log.e("accept_a_request flag", "=" + flag);
                                            String logMessage = jObj.getString("log");
                                            new DialogPopup().alertPopup(activity, "", "" + logMessage);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        reduceRideRequest(activity, Data.dEngagementId);
                                    } else {
//


                                        JSONObject userData = jObj.getJSONObject("user_data");

                                        String userName = userData.getString("user_name");
                                        String userImage = userData.getString("user_image");
                                        String phoneNo = userData.getString("phone_no");
                                        String rating = "4";
                                        try {
                                            rating = userData.getString("user_rating");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (userName == null) {
                                            userName = "";
                                        }
                                        if (userImage == null) {
                                            userImage = "http://i68.tinypic.com/2hr2zwp.jpg";
                                        }
                                        if (phoneNo == null) {
                                            phoneNo = "";
                                        }


                                        int isScheduled = 0;
                                        String pickupTime = "";
                                        if (jObj.has("is_scheduled")) {
                                            isScheduled = jObj.getInt("is_scheduled");
                                            if (isScheduled == 1 && jObj.has("pickup_time")) {
                                                pickupTime = jObj.getString("pickup_time");
                                            }
                                        }

                                        int freeRide = 0;
                                        if (jObj.has("free_ride")) {
                                            freeRide = jObj.getInt("free_ride");
                                        }
                                        if(FeaturesConfigFile.isArrivedStateEnable)
                                        {
                                            Data.isArrived=0;
                                        }

                                        Data.assignedCustomerInfo = new CustomerInfo(Data.dCustomerId, userName,
                                                userImage, phoneNo, rating, freeRide);
                                        Data.assignedCustomerInfo.schedulePickupTime = pickupTime;

                                        Data.driverRideRequests.clear();

                                        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
                                        Editor editor = pref.edit();
                                        editor.putString(Data.SP_DRIVER_SCREEN_MODE, Data.D_START_RIDE);

                                        editor.putString(Data.SP_D_ENGAGEMENT_ID, Data.dEngagementId);
                                        editor.putString(Data.SP_D_CUSTOMER_ID, Data.dCustomerId);

                                        editor.putString(Data.SP_D_LATITUDE, "" + Data.dCustLatLng.latitude);
                                        editor.putString(Data.SP_D_LONGITUDE, "" + Data.dCustLatLng.longitude);

                                        editor.putString(Data.SP_D_CUSTOMER_NAME, Data.assignedCustomerInfo.name);
                                        editor.putString(Data.SP_D_CUSTOMER_IMAGE, Data.assignedCustomerInfo.image);
                                        editor.putString(Data.SP_D_CUSTOMER_PHONE, Data.assignedCustomerInfo.phoneNumber);
                                        editor.putString(Data.SP_D_CUSTOMER_RATING, Data.assignedCustomerInfo.rating);

                                        editor.commit();

                                        GCMIntentService.clearNotifications(getApplicationContext());

                                        driverScreenMode = DriverScreenMode.D_START_RIDE;
                                        switchDriverScreen(driverScreenMode);
                                        if (timer != null) {
                                            timer.cancel();
                                            timerTask.cancel();
                                        }

                                    }


                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                DialogPopup.dismissLoadingDialog();
                            }

                            DialogPopup.dismissLoadingDialog();

                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }


    public void reduceRideRequest(Activity activity, String engagementId) {
        Data.driverRideRequests.remove(new DriverRideRequest(engagementId,""));
        driverScreenMode = DriverScreenMode.D_INITIAL;
        switchDriverScreen(driverScreenMode);

    }


    /**
     * ASync for change driver mode from server
     */
    public void driverRejectRequestAsync(final Activity activity) {

        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();


            params.put("access_token", Data.userData.accessToken);
            params.put("customer_id", Data.dCustomerId);
            params.put("engagement_id", Data.dEngagementId);

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("customer_id", "=" + Data.dCustomerId);
            Log.i("engagement_id", "=" + Data.dEngagementId);


            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/reject_a_request", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail reject request", arg3.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.v("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                } else {
//										{"log":"rejected successfully"}
//										{"log":"Rejected successfully","flag":110}
                                    try {
                                        int flag = jObj.getInt("flag");
                                        if (ApiResponseFlags.REQUEST_TIMEOUT.getOrdinal() == flag) {
                                            String log = jObj.getString("log");
                                            new DialogPopup().alertPopup(activity, "", "" + log);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    if (map != null) {
                                        map.clear();
                                    }
                                    stopService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));

                                    reduceRideRequest(activity, Data.dEngagementId);

                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                            DialogPopup.dismissLoadingDialog();
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }


    }


    public void initializeStartRideVariables() {
        if (createPathAsyncTasks != null) {
            createPathAsyncTasks.clear();
        }

        lastLocation = null;

        HomeActivity.previousWaitTime = 0;
        HomeActivity.previousRideTime = 0;
        HomeActivity.totalDistance = -1;

        driverWaitText.setBackgroundResource(R.drawable.main_btn_selector);
        driverWaitText.setText(getResources().getString(R.string.start_wait));

        if (HomeActivity.deltaLatLngPairs == null) {
            HomeActivity.deltaLatLngPairs = new ArrayList<LatLngPair>();
        }
        HomeActivity.deltaLatLngPairs.clear();

        clearRideSPData();

        waitStart = 2;
    }


    /**
     * ASync for start ride in  driver mode from server
     */
    public void driverStartRideAsync(final Activity activity, final LatLng driverAtPickupLatLng) {
        initializeStartRideVariables();

        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();


            params.put("access_token", Data.userData.accessToken);
            params.put("engagement_id", Data.dEngagementId);
            params.put("customer_id", Data.dCustomerId);
            params.put("pickup_latitude", "" + driverAtPickupLatLng.latitude);
            params.put("pickup_longitude", "" + driverAtPickupLatLng.longitude);


            //need to change
            params.put("manual_destination_latitude", "0.0");
            params.put("manual_destination_longitude", "0.0");
            params.put("manual_destination_address", "fdsf");


            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engagement_id", "=" + Data.dEngagementId);
            Log.i("customer_id", "=" + Data.dCustomerId);
            Log.i("pickup_latitude", "=" + driverAtPickupLatLng.latitude);
            Log.i("pickup_longitude", "=" + driverAtPickupLatLng.longitude);


            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/start_ride", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail start ride", arg3.toString());
                            DialogPopup.dismissLoadingDialog();
//							new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                            callAndHandleStateRestoreAPI();
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.v("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                } else {


//									{"log":"ride_started"}

                                    if (map != null) {
                                        map.clear();
                                    }

                                    initializeStartRideVariables();
                                    JSONParser.parseFareDetails(jObj);
                                    Prefs.with(getApplicationContext()).save("destinationAddress", jObj.getString("manual_destination_address"));
                                    Prefs.with(getApplicationContext()).save("destinationLatitude", jObj.getString("manual_destination_latitude"));
                                    Prefs.with(getApplicationContext()).save("destinationLongitude", jObj.getString("manual_destination_longitude"));
                                    Data.startRidePreviousLatLng = driverAtPickupLatLng;
                                    SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
                                    Editor editor = pref.edit();
                                    editor.putString(Data.SP_LAST_LATITUDE, "" + driverAtPickupLatLng.latitude);
                                    editor.putString(Data.SP_LAST_LONGITUDE, "" + driverAtPickupLatLng.longitude);
                                    editor.commit();
                                    Log.e("driverAtPickupLatLng in start_ride", "=" + driverAtPickupLatLng);
                                    writePathLogToFile("on Start driverAtPickupLatLng" + driverAtPickupLatLng);

                                    driverScreenMode = DriverScreenMode.D_IN_RIDE;
                                    switchDriverScreen(driverScreenMode);

                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                            DialogPopup.dismissLoadingDialog();
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }


    @Override
    public void forceStateChange() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    try {
                        if (forceDialog != null && forceDialog.isShowing()) {
                            forceDialog.dismiss();
                        }
                    } catch (Exception e) {
                    }

                    forceDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
                    forceDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
                    forceDialog.setContentView(R.layout.custom_message_dialog);

                    FrameLayout frameLayout = (FrameLayout) forceDialog.findViewById(R.id.rv);
                    new ASSL(activity, frameLayout, 1134, 720, false);

                    WindowManager.LayoutParams layoutParams = forceDialog.getWindow().getAttributes();
                    layoutParams.dimAmount = 0.6f;
                    forceDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    forceDialog.setCancelable(false);
                    forceDialog.setCanceledOnTouchOutside(false);


                    TextView textHead = (TextView) forceDialog.findViewById(R.id.textHead);
                    textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
                    TextView textMessage = (TextView) forceDialog.findViewById(R.id.textMessage);
                    textMessage.setTypeface(Data.getFont(activity));

                    textMessage.setMovementMethod(new ScrollingMovementMethod());
                    textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

                    textMessage.setText(getString(R.string.ride_end_by_admin));

                    textHead.setVisibility(View.VISIBLE);

                    Button btnOk = (Button) forceDialog.findViewById(R.id.btnOk);
                    btnOk.setTypeface(Data.getFont(activity));

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                forceDialog.dismiss();
                                callAndHandleStateRestoreAPI();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });

                    forceDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    /**
     * ASync for change driver mode from server
     */
    public void driverCancelRideAsync(final Activity activity) {

        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();


            params.put("access_token", Data.userData.accessToken);
            params.put("customer_id", Data.dCustomerId);
            params.put("engagement_id", Data.dEngagementId);

            Log.e("access_token", "=" + Data.userData.accessToken);
            Log.e("customer_id", "=" + Data.dCustomerId);
            Log.e("engagement_id", "=" + Data.dEngagementId);


            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/cancel_the_ride", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail cancel ride", arg3.toString());
                            DialogPopup.dismissLoadingDialog();
//								new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                            callAndHandleStateRestoreAPI();
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.v("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                } else {
//										{"log":"rejected successfully"}
//										response = {"log":"Ride cancelled successfully","flag":109}

                                    try {
                                        int flag = jObj.getInt("flag");
                                        if (ApiResponseFlags.REQUEST_TIMEOUT.getOrdinal() == flag) {
                                            String log = jObj.getString("log");
                                            new DialogPopup().alertPopup(activity, "", "" + log);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (map != null) {
                                        map.clear();
                                    }
                                    stopService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));

                                    reduceRideRequest(activity, Data.dEngagementId);

                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                            DialogPopup.dismissLoadingDialog();
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }


    }




    /**
     * ASync for change driver mode from server
     */
    public void driverArrivedAsync(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();

            params.put("access_token", Data.userData.accessToken);
            params.put("engagement_id", Data.dEngagementId);

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engage_id", "=" + Data.dEngagementId);

            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/send_arrival_text", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail acccept request", arg3.toString());
							new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                            DialogPopup.dismissLoadingDialog();

                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.i("accept ride api Server response", "response = " + response);
                            try {
                                jObj = new JSONObject(response);
                                if (!jObj.isNull("error")) {
                                    int flag = jObj.getInt("flag");
                                    Log.e("accept_a_request flag", "=" + flag);
                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (112 == flag) {
                                        if (driverScreenMode == DriverScreenMode.D_START_RIDE ) {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                    DialogPopup.dismissLoadingDialog();
                                } else {
;
                                            Data.isArrived=1;
                                    timeTillArrived=0;
                                    driverArrivedChronometer.stop();
                                        driverScreenMode = DriverScreenMode.D_START_RIDE;
                                        switchDriverScreen(driverScreenMode);

                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                DialogPopup.dismissLoadingDialog();
                            }

                            DialogPopup.dismissLoadingDialog();

                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }


    /**
     * ASync for start ride in  driver mode from server
     */
    public void driverEndRideAsync(final Activity activity, double dropLatitude, double dropLongitude, double waitMinutes, double rideMinutes) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            if (createPathAsyncTasks != null) {
                createPathAsyncTasks.clear();
            }

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();
            Log.i("rideMinutes", "" + rideMinutes);
            SharedPreferences pref = activity.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
            long rideStartTime = Long.parseLong(pref.getString(Data.SP_RIDE_START_TIME, "" + System.currentTimeMillis()));
            long timeDiffToAdd = System.currentTimeMillis() - rideStartTime;
            long rideTimeSeconds = timeDiffToAdd / 1000;
//			double rideTimeMinutes = Math.floor(((double)rideTimeSeconds) / 60.0);
            double rideTimeMinutes = Math.ceil((double) rideTimeSeconds / 60.0);
            Log.e("System.currentTimeMillis() - rideStartTime", "=" + System.currentTimeMillis() + " - " + rideStartTime);
            Log.e("timeDiffToAdd", "=" + rideTimeMinutes);
            if (rideTimeMinutes > 0) {
                rideMinutes = rideTimeMinutes;
            }
            rideTime = "" + rideMinutes;
//			rideTime = decimalFormatNoDecimal.format(rideMinutes);
            waitTime = decimalFormatNoDecimal.format(waitMinutes);
            double totalDistanceInMiles = Math.abs(totalDistance)*Double.parseDouble(getString(R.string.distance_conversion_factor));

            params.put("access_token", Data.userData.accessToken);
            params.put("engagement_id", Data.dEngagementId);
            params.put("customer_id", Data.dCustomerId);
            params.put("latitude", "" + dropLatitude);
            params.put("longitude", "" + dropLongitude);
            params.put("distance_travelled", decimalFormat.format(totalDistanceInMiles));
            params.put("wait_time", waitTime);
            params.put("ride_time",rideMinutes );

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engagement_id", "=" + Data.dEngagementId);
            Log.i("customer_id", "=" + Data.dCustomerId);
            Log.i("latitude", "=" + dropLatitude);
            Log.i("longitude", "=" + dropLongitude);
            Log.i("distance_travelled", "=" + decimalFormat.format(totalDistanceInMiles));
            Log.i("wait_time", "=" + waitTime);
            Log.i("ride_time", "=" + rideTime);


            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/end_ride", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail end_ride", arg3.toString());
                            driverScreenMode = DriverScreenMode.D_IN_RIDE;
                            DialogPopup.dismissLoadingDialog();
                            rideTimeChronometer.start();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.e("Server response end_ride", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                    driverScreenMode = DriverScreenMode.D_IN_RIDE;
                                    rideTimeChronometer.start();
                                } else {

//

                                    try {
                                        totalFare = jObj.getDouble("fare");
                                        rideTime = "" + (Double.parseDouble(jObj.getString("ride_time")) + Double.parseDouble(jObj.getString("wait_time")));
                                        Co2_val = jObj.getDouble("co2_saved");
                                        discount = jObj.getDouble("discount");

                                        Data.endRidesCustomerInfo.clear();
                                        for (int i = 0; i < jObj.getJSONArray("users").length(); i++) {
                                            JSONObject customerObject = jObj.getJSONArray("users").getJSONObject(i);
                                            Data.endRidesCustomerInfo.add(new EndRideCustomerInfo(customerObject.getString("user_name"), customerObject.getString("user_image"), customerObject.getString("user_id"), customerObject.getString("engagement_id"), customerObject.getDouble("to_pay")
                                                    , customerObject.getInt("is_payment_successful"), customerObject.getInt("payment_method"), customerObject.getInt("defaulter_flag")));
                                        }
//
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        totalFare = 0;
                                        Co2_val = 0;
                                    }

//									displayCouponApplied(jObj);


                                    lastLocation = null;

                                    if (map != null) {
                                        map.clear();
                                    }

                                    waitStart = 2;
                                    waitChronometer.stop();
                                    rideTimeChronometer.stop();


                                    clearSPData();
                                    searchEtDestination.setText("");
                                    Prefs.with(getApplicationContext()).save("destinationAddress", "");
                                    Prefs.with(getApplicationContext()).save("destinationLatitude", "");
                                    Prefs.with(getApplicationContext()).save("destinationLongitude", "");
                                    makeOkButtonEnable=false;
                                    driverScreenMode = DriverScreenMode.D_RIDE_END;
                                    switchDriverScreen(driverScreenMode);

                                    driverUploadPathDataFileAsync(activity, Data.dEngagementId);

                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                driverScreenMode = DriverScreenMode.D_IN_RIDE;
                                rideTimeChronometer.start();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                            DialogPopup.dismissLoadingDialog();
                        }
                    });
        } else {
            driverScreenMode = DriverScreenMode.D_IN_RIDE;
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
            rideTimeChronometer.start();
        }

    }




    /**
     * ASync for uploading path data file to server
     */
    public void driverUploadPathDataFileAsync(final Activity activity, final String engagementId) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            File pathLogFile = null;
            try {
                pathLogFile = Log.getPathLogFile(engagementId);
                if (pathLogFile != null) {
                    RequestParams params = new RequestParams();
                    params.put("access_token", Data.userData.accessToken);
                    params.put("engagement_id", engagementId);
                    params.put("ride_path", pathLogFile);
                    params.put("file_type", "0");

                    Log.e("access_token", "=" + Data.userData.accessToken);
                    Log.e("engagement_id", "=" + engagementId);
                    Log.e("pathLogFile", "=" + pathLogFile);

                    AsyncHttpClient client = Data.getClient();
                    client.post(Data.SERVER_URL + "/upload_file", params,
                            new CustomAsyncHttpResponseHandler() {

                                @Override
                                public void onFailure(Throwable arg3) {
                                    Log.e("request fail upload file", arg3.toString());
                                }

                                @Override
                                public void onSuccess(String response) {
                                    Log.e("Server response on upload path file", "response = " + response);
                                }
                            });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * ASync for makeDriverFree from server
     */
    public void makeDriverFree(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();

            params.put("access_token", Data.userData.accessToken);

            Log.i("access_token", "=" + Data.userData.accessToken);


            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/make_driver_free", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail make_driver_free", arg3.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.v("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (0 == flag) { // {"error": 'some parameter missing',"flag":0}//error
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                } else {



                                    switchUserScreen();
                                    cancelCheckPaymentStatusTimer();
                                    driverScreenMode = DriverScreenMode.D_INITIAL;
                                    switchDriverScreen(driverScreenMode);


                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                            DialogPopup.dismissLoadingDialog();
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }



    /**
     * ASync for logout from server
     */
    public void logoutAsync(final Activity activity) {
        if (AppStatus.getInstance(activity).isOnline(activity)) {

            DialogPopup.showLoadingDialog(activity, "Please Wait ...");

            RequestParams params = new RequestParams();

            params.put("access_token", Data.userData.accessToken);

            Log.i("access_token", "=" + Data.userData.accessToken);

            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/logout_driver", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail logout_driver", arg3.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.v("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);
                                int flag = jObj.getInt("flag");
                                if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                    HomeActivity.logoutUser(activity);
                                } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                    String errorMessage = jObj.getString("error");
                                    new DialogPopup().alertPopup(activity, "", errorMessage);
                                } else if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                                    String message = jObj.getString("message");
                                    new DialogPopup().alertPopup(activity, "", message);
                                } else if (ApiResponseFlags.LOGOUT_FAILURE.getOrdinal() == flag) {
                                    String errorMessage = jObj.getString("error");
                                    new DialogPopup().alertPopup(activity, "", errorMessage);
                                } else if (ApiResponseFlags.LOGOUT_SUCCESSFUL.getOrdinal() == flag) {
                                    PicassoTools.clearCache(Picasso.with(activity));

                                    try {
                                        Session.getActiveSession().closeAndClearTokenInformation();
                                    } catch (Exception e) {
                                        Log.v("Logout", "Error" + e);
                                    }

                                    GCMIntentService.clearNotifications(activity);

                                    Data.clearDataOnLogout(activity);


                                    driverScreenMode = DriverScreenMode.D_INITIAL;

                                    new DriverServiceOperations().stopService(activity);

                                    loggedOut = true;
                                } else {
                                    new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                            DialogPopup.dismissLoadingDialog();
                        }

                        @Override
                        public void onRetry(int retryNo) {
                            Log.e("retryNo", "=" + retryNo);
                            super.onRetry(retryNo);
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }


    /**
     * ASync for start or end wait from server
     */
    public void startEndWaitAsync(final Activity activity, String customerId, final int flag) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            RequestParams params = new RequestParams();

            params.put("access_token", Data.userData.accessToken);
            params.put("user_id", customerId);
            params.put("flag", "" + flag);
            params.put("engagement_id", Data.dEngagementId);

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("user_id", "=" + customerId);
            Log.i("flag", "=" + flag);
            Log.i("engagement_id", "=" + Data.dEngagementId);

            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/start_end_wait", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail", arg3.toString());

                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                            if (flag == 1) {
                                stopWait(false);
                            } else {
                                startWait(false);
                            }

                            driverWaitText.setEnabled(true);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.v("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    }
                                } else  {

                                    driverWaitText.setEnabled(true);
                                   updateDistanceFareTexts(true);
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                driverWaitText.setEnabled(true);
                            }

                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
            driverWaitText.setEnabled(true);
        }

    }


    void logoutPopup(final Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            textHead.setVisibility(View.VISIBLE);
            ImageView imgHorizontalLine = (ImageView) dialog.findViewById(R.id.imgHorizontalLine);
            imgHorizontalLine.setVisibility(View.VISIBLE);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            textHead.setText(getString(R.string.logout));
            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity),Typeface.BOLD);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity),Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    logoutAsync(activity);
                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    //Driver's timer
    Timer timerCustomerPathUpdater;
    TimerTask timerTaskCustomerPathUpdater;


    public void startCustomerPathUpdateTimer() {

        try {
            if (timerTaskCustomerPathUpdater != null) {
                timerTaskCustomerPathUpdater.cancel();
                timerTaskCustomerPathUpdater = null;
            }

            if (timerCustomerPathUpdater != null) {
                timerCustomerPathUpdater.cancel();
                timerCustomerPathUpdater.purge();
                timerCustomerPathUpdater = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            timerCustomerPathUpdater = new Timer();

            timerTaskCustomerPathUpdater = new TimerTask() {

                @Override
                public void run() {
                    try {
                        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                            //customer path updater
                            if (myLocation != null) {
                                if (Data.dCustLatLng != null) {

                                    if (driverScreenMode == DriverScreenMode.D_START_RIDE) {
                                        LatLng source = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                                        String url = MapUtils.makeDirectionsURL(source, Data.dCustLatLng);
                                        String result = new HttpRequester().getJSONFromUrl(url);

                                        if (result != null) {
                                            final List<LatLng> list = MapUtils.getLatLngListFromPath(result);

                                            runOnUiThread(new Runnable() {

                                                @Override
                                                public void run() {
                                                    try {
                                                        if (driverScreenMode == DriverScreenMode.D_START_RIDE) {
                                                            map.clear();

                                                            if (markerOptionsCustomerPickupLocation == null) {
                                                                markerOptionsCustomerPickupLocation = new MarkerOptions();
                                                                markerOptionsCustomerPickupLocation.title(Data.dEngagementId);
                                                                markerOptionsCustomerPickupLocation.snippet("");
                                                                markerOptionsCustomerPickupLocation.position(Data.dCustLatLng);
                                                                markerOptionsCustomerPickupLocation.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPassengerMarkerBitmap(HomeActivity.this, assl)));
                                                            }

                                                            map.addMarker(markerOptionsCustomerPickupLocation);

                                                            for (int z = 0; z < list.size() - 1; z++) {
                                                                LatLng src = list.get(z);
                                                                LatLng dest = list.get(z + 1);
                                                                map.addPolyline(new PolylineOptions()
                                                                        .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude))
                                                                        .width(5)
                                                                        .color(D_TO_C_MAP_PATH_COLOR).geodesic(true));
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };


            timerCustomerPathUpdater.scheduleAtFixedRate(timerTaskCustomerPathUpdater, 10, 15000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelCustomerPathUpdateTimer() {
        try {
            if (timerTaskCustomerPathUpdater != null) {
                timerTaskCustomerPathUpdater.cancel();
                timerTaskCustomerPathUpdater = null;
            }

            if (timerCustomerPathUpdater != null) {
                timerCustomerPathUpdater.cancel();
                timerCustomerPathUpdater.purge();
                timerCustomerPathUpdater = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Driver's timer
    Timer timerDestinationPathUpdater;
    TimerTask timerTaskDestinationPathUpdater;


    public void startDestinationPathUpdateTimer() {

        try {
            if (timerTaskDestinationPathUpdater != null) {
                timerTaskDestinationPathUpdater.cancel();
                timerTaskDestinationPathUpdater = null;
            }

            if (timerDestinationPathUpdater != null) {
                timerDestinationPathUpdater.cancel();
                timerDestinationPathUpdater.purge();
                timerDestinationPathUpdater = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            timerDestinationPathUpdater = new Timer();

            timerTaskDestinationPathUpdater = new TimerTask() {

                @Override
                public void run() {
                    try {
                        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                            //customer path updater
                            if (myLocation != null) {
                                if (Data.dDestinationLatLng != null) {

                                    if (driverScreenMode == DriverScreenMode.D_IN_RIDE) {
                                        LatLng source = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                                        String url = MapUtils.makeDirectionsURL(source, Data.dDestinationLatLng);
                                        String result = new HttpRequester().getJSONFromUrl(url);
                                        Log.i("Create destination Path", "==" + result);
                                        if (result != null) {

                                            final List<LatLng> list = MapUtils.getLatLngListFromPath(result);
                                            Log.i("list", "==" + list.size());
                                            if (list.size() > 0) {
                                                runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        try {
                                                            if (driverScreenMode == DriverScreenMode.D_IN_RIDE) {
                                                                map.clear();

                                                                if (markerOptionsCustomerPickupLocation == null) {
                                                                    markerOptionsCustomerPickupLocation = new MarkerOptions();
//																	markerOptionsCustomerPickupLocation.title(Data.dEngagementId);
                                                                    markerOptionsCustomerPickupLocation.snippet("");

                                                                    markerOptionsCustomerPickupLocation.position(Data.dDestinationLatLng);
//																	markerOptionsCustomerPickupLocation.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPassengerMarkerBitmap(HomeActivity.this, assl)));
                                                                }
                                                                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                                                builder.include(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                                                                builder.include(new LatLng(Data.dDestinationLatLng.latitude, Data.dDestinationLatLng.longitude));
                                                                LatLngBounds bounds = builder.build();
                                                                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) (720 * ASSL.Xscale()), (int) (880 * ASSL.Yscale()), (int) (40 * ASSL.Xscale())));
                                                                map.addMarker(markerOptionsCustomerPickupLocation);

                                                                for (int z = 0; z < list.size() - 1; z++) {
                                                                    LatLng src = list.get(z);
                                                                    LatLng dest = list.get(z + 1);
                                                                    map.addPolyline(new PolylineOptions()
                                                                            .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude))
                                                                            .width(5)
                                                                            .color(D_TO_C_MAP_PATH_COLOR).geodesic(true));
                                                                }
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });
                                            } else {
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        new DialogPopup().alertPopup(activity, "", "No route found");
                                                        cancelDestinationPathUpdateTimer();

                                                    }
                                                });

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };


            timerDestinationPathUpdater.scheduleAtFixedRate(timerTaskDestinationPathUpdater, 10, 15000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelDestinationPathUpdateTimer() {
        try {
            if (timerTaskDestinationPathUpdater != null) {
                timerTaskDestinationPathUpdater.cancel();
                timerTaskDestinationPathUpdater = null;
            }

            if (timerDestinationPathUpdater != null) {
                timerDestinationPathUpdater.cancel();
                timerDestinationPathUpdater.purge();
                timerDestinationPathUpdater = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public void updateInRideData() {
        if ( DriverScreenMode.D_IN_RIDE == driverScreenMode) {
            if (myLocation != null) {
                double totalDistanceInMiles = Math.abs(totalDistance)*Double.parseDouble(getString(R.string.distance_conversion_factor));
                long rideTimeSeconds = rideTimeChronometer.eclipsedTime / 1000;
                double rideTimeMinutes = Math.ceil(((double) rideTimeSeconds) / 60.0);

                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("access_token", Data.userData.accessToken));
                nameValuePairs.add(new BasicNameValuePair("engagement_id", Data.dEngagementId));
                nameValuePairs.add(new BasicNameValuePair("current_latitude", "" + myLocation.getLatitude()));
                nameValuePairs.add(new BasicNameValuePair("current_longitude", "" + myLocation.getLongitude()));
                nameValuePairs.add(new BasicNameValuePair("distance_travelled", decimalFormat.format(totalDistanceInMiles)));
                nameValuePairs.add(new BasicNameValuePair("ride_time", decimalFormatNoDecimal.format(rideTimeMinutes)));
                nameValuePairs.add(new BasicNameValuePair("wait_time", "0"));

                Log.i("update_in_ride_data nameValuePairs", "=" + nameValuePairs);

                HttpRequester simpleJSONParser = new HttpRequester();
                String result = simpleJSONParser.getJSONFromUrlParams(Data.SERVER_URL + "/update_in_ride_data", nameValuePairs);
                Log.i("update_in_ride_data result", "=" + result);
            }
        }
    }


    //Update In Ride Data
    Timer timerMapAnimateAndUpdateRideData;
    TimerTask timerTaskMapAnimateAndUpdateRideData;


    public void startMapAnimateAndUpdateRideDataTimer() {
        cancelMapAnimateAndUpdateRideDataTimer();
        try {
            timerMapAnimateAndUpdateRideData = new Timer();

            timerTaskMapAnimateAndUpdateRideData = new TimerTask() {

                @Override
                public void run() {
                    try {
                        updateInRideData();

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                updateDistanceFareTexts(false);
                                if (Prefs.with(getApplicationContext()).getString("destinationAddress", "").equalsIgnoreCase("")) {
                                    if (myLocation != null && map != null) {
                                        map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())));
                                    }
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            timerMapAnimateAndUpdateRideData.scheduleAtFixedRate(timerTaskMapAnimateAndUpdateRideData, 100, 60000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelMapAnimateAndUpdateRideDataTimer() {
        try {
            if (timerTaskMapAnimateAndUpdateRideData != null) {
                timerTaskMapAnimateAndUpdateRideData.cancel();
                timerTaskMapAnimateAndUpdateRideData = null;
            }

            if (timerMapAnimateAndUpdateRideData != null) {
                timerMapAnimateAndUpdateRideData.cancel();
                timerMapAnimateAndUpdateRideData.purge();
                timerMapAnimateAndUpdateRideData = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Update In Ride Fare Values
    Timer timerUpdateFareData;
    TimerTask timerTaskUpdateFareData;


    public void startFareUpdateTimer() {
        cancelFareUpdateTimer();
        try {
            timerUpdateFareData = new Timer();

            timerTaskUpdateFareData = new TimerTask() {

                @Override
                public void run() {
                    try {

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                updateDistanceFareTexts(false);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            timerUpdateFareData.scheduleAtFixedRate(timerTaskUpdateFareData, 100, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelFareUpdateTimer() {
        try {
            if (timerTaskUpdateFareData != null) {
                timerTaskUpdateFareData.cancel();
                timerTaskUpdateFareData = null;
            }

            if (timerUpdateFareData != null) {
                timerUpdateFareData.cancel();
                timerUpdateFareData.purge();
                timerUpdateFareData = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    Dialog startRideDialog;
    void startRidePopup(final Activity activity) {
        try {
            startRideDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            startRideDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            startRideDialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) startRideDialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = startRideDialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            startRideDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            startRideDialog.setCancelable(false);
            startRideDialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) startRideDialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            textHead.setVisibility(View.VISIBLE);
            TextView textMessage = (TextView) startRideDialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textHead.setText(getString(R.string.start_ride));

            textMessage.setText(getString(R.string.are_you_sure_you_want_to_start_ride));


            Button btnOk = (Button) startRideDialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity),Typeface.BOLD);
            Button btnCancel = (Button) startRideDialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity),Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (myLocation != null) {
                        startRideDialog.dismiss();
                        LatLng driverAtPickupLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        double displacement = MapUtils.displacement(driverAtPickupLatLng, Data.dCustLatLng);

                        if (displacement <= DRIVER_START_RIDE_CHECK_METERS) {
                            buildAlertMessageNoGps();

                            GCMIntentService.clearNotifications(activity);

                            driverStartRideAsync(activity, driverAtPickupLatLng);
                        } else {
                            new DialogPopup().alertPopup(activity, "", getString(R.string.start_ride_error));
                        }
                    } else {
                        Toast.makeText(activity, getString(R.string.waiting_for_location), Toast.LENGTH_SHORT).show();
                    }

                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startRideDialog.dismiss();
                }

            });

            startRideDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void endRidePopup(final Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textHead.setVisibility(View.VISIBLE);
            textHead.setText(getString(R.string.end_ride));
            textMessage.setText(getString(R.string.are_you_sure_you_want_to_end_ride));


            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity),Typeface.BOLD);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity),Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (myLocation != null) {
                        dialog.dismiss();

                        GCMIntentService.clearNotifications(HomeActivity.this);
                        waitChronometer.stop();
                        rideTimeChronometer.stop();

                        driverWaitText.setBackgroundResource(R.drawable.main_btn_selector);
                        driverWaitText.setText(getResources().getString(R.string.start_wait));
                        waitStart = 0;

                        long rideTimeSeconds = rideTimeChronometer.eclipsedTime / 1000;
                        double rideTimeMinutes = Math.floor(((double) rideTimeSeconds) / 60.0);

                        driverScreenMode = DriverScreenMode.D_RIDE_END;

                        driverEndRideAsync(activity, myLocation.getLatitude(), myLocation.getLongitude(), 0, ((double) rideTimeSeconds) / 60.0);
                    } else {
                        Toast.makeText(activity, getString(R.string.waiting_for_location), Toast.LENGTH_SHORT).show();
                    }

                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void cancelRidePopup(final Activity activity) {
        try {
            cancelDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            cancelDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            cancelDialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) cancelDialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = cancelDialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            cancelDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            cancelDialog.setCancelable(false);
            cancelDialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) cancelDialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) cancelDialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textHead.setVisibility(View.VISIBLE);
            textMessage.setText(getString(R.string.are_you_sure_you_want_to_cancel_ride));

            textHead.setText(getString(R.string.cancel_ride));
            Button btnOk = (Button) cancelDialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity),Typeface.BOLD);
            Button btnCancel = (Button) cancelDialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity),Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelDialog.dismiss();

                    GCMIntentService.clearNotifications(HomeActivity.this);
                    driverCancelRideAsync(HomeActivity.this);
                }


            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelDialog.dismiss();
                }

            });

            cancelDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     Dialog arrivedConfirmationDialog;
    void arrivedConfirmationPopup(final Activity activity) {
        try {
            arrivedConfirmationDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            arrivedConfirmationDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            arrivedConfirmationDialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) arrivedConfirmationDialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = arrivedConfirmationDialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            arrivedConfirmationDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            arrivedConfirmationDialog.setCancelable(false);
            arrivedConfirmationDialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) arrivedConfirmationDialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) arrivedConfirmationDialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textHead.setVisibility(View.VISIBLE);
            textMessage.setText(getString(R.string.are_you_sure_you_have_arrived));

            textHead.setText(getString(R.string.arrived));
            Button btnOk = (Button) arrivedConfirmationDialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity),Typeface.BOLD);
            Button btnCancel = (Button) arrivedConfirmationDialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity),Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    arrivedConfirmationDialog.dismiss();
                    LatLng driverAtPickupLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                    double displacement = MapUtils.displacement(driverAtPickupLatLng, Data.dCustLatLng);

                    if (displacement <= DRIVER_START_RIDE_CHECK_METERS) {
                        buildAlertMessageNoGps();
                        GCMIntentService.clearNotifications(activity);
                        driverArrivedAsync(HomeActivity.this);
                    } else {
                        new DialogPopup().alertPopup(activity, "", getString(R.string.start_ride_error));
                    }

                }


            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    arrivedConfirmationDialog.dismiss();
                }

            });

            arrivedConfirmationDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void confirmDebugPasswordPopup(final Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.edittext_confirm_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            final EditText etCode = (EditText) dialog.findViewById(R.id.etCode);
            etCode.setTypeface(Data.getFont(activity));

            textHead.setText("Confirm Debug Password");
            textMessage.setText("Please enter password to continue.");

            textHead.setVisibility(View.GONE);
            textMessage.setVisibility(View.GONE);

            final Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
            btnConfirm.setTypeface(Data.getFont(activity));
            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setTypeface(Data.getFont(activity));

            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String code = etCode.getText().toString().trim();
                    if ("".equalsIgnoreCase(code)) {
                        etCode.requestFocus();
                        etCode.setError("Code can't be empty.");
                    } else {
                        if (Data.DEBUG_PASSWORD.equalsIgnoreCase(code)) {
                            dialog.dismiss();
                            changeDebugModePopup(activity);
                        } else {
                            etCode.requestFocus();
                            etCode.setError("Code not matched.");
                        }
                    }
                }

            });


            etCode.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    int result = actionId & EditorInfo.IME_MASK_ACTION;
                    switch (result) {
                        case EditorInfo.IME_ACTION_DONE:
                            btnConfirm.performClick();
                            break;

                        case EditorInfo.IME_ACTION_NEXT:
                            break;

                        default:
                    }
                    return true;
                }
            });

            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void changeDebugModePopup(final Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textHead.setVisibility(View.VISIBLE);

            textHead.setText("Change Mode");

            if (appMode == AppMode.DEBUG) {
                textMessage.setText("App is in DEBUG mode.\nChange to:");
            } else if (appMode == AppMode.NORMAL) {
                textMessage.setText("App is in NORMAL mode.\nChange to:");
            }


            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity));
            btnOk.setText("NORMAL");
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity));
            btnCancel.setText("DEBUG");

            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setTypeface(Data.getFont(activity));
            crossbtn.setVisibility(View.VISIBLE);


            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appMode = AppMode.NORMAL;
                    dialog.dismiss();
                }


            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appMode = AppMode.DEBUG;
                    dialog.dismiss();
                }

            });

            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (loggedOut) {
                loggedOut = false;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Database2.getInstance(HomeActivity.this).updateUserMode(Database2.UM_OFFLINE);
                        Database2.getInstance(HomeActivity.this).close();
                    }
                }).start();


                stopService(new Intent(HomeActivity.this, DriverLocationUpdateService.class));

                Intent intent = new Intent(HomeActivity.this, SplashNewActivity.class);
                intent.putExtra("no_anim", "yes");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            } else {
                buildAlertMessageNoGps();
            }
        }
    }


    Timer timer;
    TimerTask timerTask;

    public void showAllRideRequestsOnMap() {

        try {


                map.clear();
                if (timer != null) {
                    timer.cancel();
                    timerTask.cancel();
                }
                if (Data.driverRideRequests.size() > 0) {


                    timer = new Timer();
                    timer.schedule(timerTask = new TimerTask() {
                        @Override
                        public void run() {


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    //stuff that updates ui
                                    driverRequestListAdapter.notifyDataSetChanged();
//							        	driverRideRequestsList.postDelayed(this, 1000);
                                }
                            });

                        }
                    }, 0, 1000);

                    //if(driverNewRideRequestRl.getVisibility() == View.GONE && driverRideRequestsList.getVisibility() == View.GONE){
                    if (driverNewRideRequestRl.getVisibility() == View.GONE) {
                        driverNewRideRequestRl.setVisibility(View.VISIBLE);
//                        driverRideRequestsList.setVisibility(View.VISIBLE);
                        // driverRideRequestsList.setVisibility(View.GONE);
                    }

                    LatLng last = map.getCameraPosition().target;

                    for (int i = 0; i < Data.driverRideRequests.size(); i++) {
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.title(Data.driverRideRequests.get(i).engagementId);
                        markerOptions.snippet("");
                        markerOptions.position(Data.driverRideRequests.get(i).latLng);
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPassengerMarkerBitmap(HomeActivity.this, assl)));

                        map.addMarker(markerOptions);

                    }

                    driverNewRideRequestText.setText(getResources().getString(R.string.you_have) + " " + Data.driverRideRequests.size() + " " + getResources().getString(R.string.ride_request));

                    map.animateCamera(CameraUpdateFactory.newLatLng(last), 1000, null);

                } else {
                    driverNewRideRequestRl.setVisibility(View.GONE);
                   // driverRideRequestsList.setVisibility(View.GONE);
                }


        } catch (NotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNewRideRequest() {
        if (driverScreenMode == DriverScreenMode.D_INITIAL || driverScreenMode == DriverScreenMode.D_RIDE_END) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dismissStationDataPopup();
                    cancelStationPathUpdateTimer();
                    showAllRideRequestsOnMap();
                }
            });
        }
    }

    @Override
    public void onCancelRideRequest(final String engagementId, final boolean acceptedByOtherDriver) {
        try {

            Log.i("ON cancel ride", "==" + driverScreenMode);

            if(arrivedConfirmationDialog!=null && arrivedConfirmationDialog.isShowing())
            {
                arrivedConfirmationDialog.dismiss();
                arrivedConfirmationDialog = null;
            }
            else if(startRideDialog!=null && startRideDialog.isShowing())
            {
                startRideDialog.dismiss();
                startRideDialog = null;
            }else if(cancelDialog != null && cancelDialog.isShowing())
            {
                cancelDialog.dismiss();
                cancelDialog = null;
            }
            if (driverScreenMode == DriverScreenMode.D_INITIAL) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showAllRideRequestsOnMap();
                        initializeStationDataProcedure();
                    }
                });
            }
            else if ( driverScreenMode == DriverScreenMode.D_START_RIDE) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (engagementId.equalsIgnoreCase(Data.dEngagementId)) {
                            dismissStationDataPopup();
                            cancelStationPathUpdateTimer();
                            driverScreenMode = DriverScreenMode.D_INITIAL;
                            switchDriverScreen(driverScreenMode);
                            new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.cancel_by_customer));
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRideRequestTimeout(final String engagementId) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (driverScreenMode == DriverScreenMode.D_INITIAL) {
                        showAllRideRequestsOnMap();
                    }
                }
            });

    }


    public void clearRideSPData() {

        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        Editor editor = pref.edit();

        editor.putString(Data.SP_TOTAL_DISTANCE, "0");
        editor.putString(Data.SP_WAIT_TIME, "0");
        editor.putString(Data.SP_RIDE_TIME, "0");
        editor.putString(Data.SP_RIDE_START_TIME, "" + System.currentTimeMillis());
        editor.putString(Data.SP_LAST_LATITUDE, "0");
        editor.putString(Data.SP_LAST_LONGITUDE, "0");

        editor.commit();

        Database.getInstance(this).deleteSavedPath();
        Database.getInstance(this).close();

    }

    public void clearSPData() {

        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        Editor editor = pref.edit();

        editor.putString(Data.SP_DRIVER_SCREEN_MODE, "");

        editor.putString(Data.SP_D_ENGAGEMENT_ID, "");
        editor.putString(Data.SP_D_CUSTOMER_ID, "");
        editor.putString(Data.SP_D_LATITUDE, "0");
        editor.putString(Data.SP_D_LONGITUDE, "0");
        editor.putString(Data.SP_D_CUSTOMER_NAME, "");
        editor.putString(Data.SP_D_CUSTOMER_IMAGE, "");
        editor.putString(Data.SP_D_CUSTOMER_PHONE, "");
        editor.putString(Data.SP_D_CUSTOMER_RATING, "");

        editor.putString(Data.SP_TOTAL_DISTANCE, "0");
        editor.putString(Data.SP_WAIT_TIME, "0");
        editor.putString(Data.SP_RIDE_TIME, "0");
        editor.putString(Data.SP_RIDE_START_TIME, "" + System.currentTimeMillis());
        editor.putString(Data.SP_LAST_LATITUDE, "0");
        editor.putString(Data.SP_LAST_LONGITUDE, "0");


        editor.commit();

        Database.getInstance(this).deleteSavedPath();
        Database.getInstance(this).close();

    }



    @Override
    public void onChangeStatePushReceived() {
        try {
            callAndHandleStateRestoreAPI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static class FBLogoutNoIntent extends AsyncTask<Void, Void, String> {

        Activity act;

        public FBLogoutNoIntent(Activity act) {
            this.act = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(Void... urls) {
            try {
                try {
                    Session.getActiveSession().closeAndClearTokenInformation();
                } catch (Exception e) {
                    Log.v("Logout", "Error" + e);
                }

                SharedPreferences pref = act.getSharedPreferences("myPref", 0);
                Editor editor = pref.edit();
                editor.clear();
                editor.commit();
            } catch (Exception e) {
                Log.v("e", e.toString());

            }

            return "";
        }

        @Override
        protected void onPostExecute(String text) {

        }
    }

    public static void logoutUser(final Activity cont) {
        try {

            new FBLogoutNoIntent(cont).execute();

            PicassoTools.clearCache(Picasso.with(cont));

            cont.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    SharedPreferences pref = cont.getSharedPreferences("myPref", 0);
                    Editor editor = pref.edit();
                    editor.clear();
                    editor.commit();
                    Data.clearDataOnLogout(cont);
                    sessionExpireAlert(cont);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void sessionExpireAlert(final Activity activity){

        try {

            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_message_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, false);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = 0.0f;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

            textHead.setText(activity.getResources().getString(R.string.alert));
            textMessage.setText(activity.getResources().getString(R.string.your_login_session_expired) +" " +
                    activity.getString(R.string.customer_care_no));

            textHead.setTextColor(activity.getResources().getColorStateList(R.color.alert_dialog_heading_text_color));
            textHead.setVisibility(View.VISIBLE);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dialog.dismiss();
                        Intent intent = new Intent(activity, SplashNewActivity.class);
                        intent.putExtra("no_anim", "yes");
                        activity.startActivity(intent);
                        activity.finish();
                        activity.overridePendingTransition(
                                R.anim.left_in,
                                R.anim.left_out);
                    } catch (Exception e) {
                        Log.i("excption logout",
                                e.toString());
                    }
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Handler automaticReviewHandler;
    public Runnable automaticReviewRunnable;

    public void startAutomaticReviewHandler() {
        try {
            stopAutomaticReviewhandler();
            automaticReviewHandler = new Handler();
            automaticReviewRunnable = new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if ( driverScreenMode == DriverScreenMode.D_RIDE_END) {
                                GCMIntentService.clearNotifications(HomeActivity.this);
                                skipFeedbackForCustomerAsync(HomeActivity.this);
//                                submitReviewAsync(HomeActivity.this, Data.dEngagementId, "0", Data.dCustomerId, "5");
                            }
//
                        }
                    });
                }
            };

            automaticReviewHandler.postDelayed(automaticReviewRunnable, AUTO_RATING_DELAY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopAutomaticReviewhandler() {
        try {
            if (automaticReviewHandler != null && automaticReviewRunnable != null) {
                automaticReviewHandler.removeCallbacks(automaticReviewRunnable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        automaticReviewHandler = null;
        automaticReviewRunnable = null;
    }


    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
        FlurryAgent.onEvent("HomeActivity started");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


    public void initializeFusedLocationFetchers() {
        destroyFusedLocationFetchers();
//		if(((userMode == UserMode.DRIVER) && (driverScreenMode != DriverScreenMode.D_IN_RIDE)) 
//				|| ((userMode == UserMode.PASSENGER) && (passengerScreenMode != PassengerScreenMode.P_IN_RIDE))){
        if (myLocation == null) {
            if (lowPowerLF == null) {
                lowPowerLF = new LocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD, 0);
            }
            if (highAccuracyLF == null) {
                highAccuracyLF = new LocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD, 2);
            }
        } else {
            if (highAccuracyLF == null) {
                highAccuracyLF = new LocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD, 2);
            }
        }
//		}
    }


    public void destroyFusedLocationFetchers() {
        destroyLowPowerFusedLocationFetcher();
        destroyHighAccuracyFusedLocationFetcher();
    }

    public void destroyLowPowerFusedLocationFetcher() {
        try {
            if (lowPowerLF != null) {
                lowPowerLF.destroy();
                lowPowerLF = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void destroyHighAccuracyFusedLocationFetcher() {
        try {
            if (highAccuracyLF != null) {
                highAccuracyLF.destroy();
                highAccuracyLF = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public synchronized void onLocationChanged(Location location, int priority) {
        if (priority == 0) {
            if (location.getAccuracy() <= LOW_POWER_ACCURACY_CHECK) {
                HomeActivity.myLocation = location;
                zoomToCurrentLocationAtFirstLocationFix(location);
                //updatePickupLocation(location);
            }
        } else if (priority == 2) {
            destroyLowPowerFusedLocationFetcher();
            if (location.getAccuracy() <= HIGH_ACCURACY_ACCURACY_CHECK) {
                HomeActivity.myLocation = location;
                zoomToCurrentLocationAtFirstLocationFix(location);

                drawLocationChanged(location);

            }
        }
        if (driverScreenMode == DriverScreenMode.D_IN_RIDE) {
            updateDistanceFareTexts(false);
        }
    }


    public void zoomToCurrentLocationAtFirstLocationFix(Location location) {
        try {
            if (map != null) {
                if (!zoomedToMyLocation) {
                    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                    zoomedToMyLocation = true;

                    initializeStationDataProcedure();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void callAndHandleStateRestoreAPI() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Data.userData != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogPopup.showLoadingDialog(HomeActivity.this, getString(R.string.loading));
                        }
                    });

                        String resp = new JSONParser().getUserStatus(HomeActivity.this, Data.userData.accessToken);
                        if (resp.contains(HttpRequester.SERVER_TIMEOUT)) {
                            String resp1 = new JSONParser().getUserStatus(HomeActivity.this, Data.userData.accessToken);
                            if (resp1.contains(HttpRequester.SERVER_TIMEOUT)) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new DialogPopup().alertPopup(HomeActivity.this, "", Data.SERVER_NOT_RESOPNDING_MSG);
                                    }
                                });
                            }
                        }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogPopup.dismissLoadingDialog();
                            startUIAfterGettingUserStatus();
                        }
                    });
                }
            }
        }).start();
    }



    public void showManualPatchPushReceivedDialog() {
        try {
            if (DriverScreenMode.D_START_RIDE == driverScreenMode) {
                if (Data.assignedCustomerInfo != null) {
                    String manualPatchPushReceived = Database2.getInstance(HomeActivity.this).getDriverManualPatchPushReceived();
                    if (Database2.YES.equalsIgnoreCase(manualPatchPushReceived)) {
                        new DialogPopup().alertPopupWithListener(HomeActivity.this, "", getString(R.string.manual_request_recieve_message), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                GCMIntentService.stopRing();
                                Database2.getInstance(HomeActivity.this).updateDriverManualPatchPushReceived(Database2.NO);
                                Database2.getInstance(HomeActivity.this).close();
                                manualPatchPushAckAPI(HomeActivity.this);
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Database2.getInstance(HomeActivity.this).close();
    }


    /**
     * ASync for acknowledging the server about manual patch push received
     */
    public void manualPatchPushAckAPI(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            RequestParams params = new RequestParams();

            params.put("access_token", Data.userData.accessToken);
            params.put("engagement_id", Data.dEngagementId);
            params.put("customer_id", Data.assignedCustomerInfo.userId);

            Log.i("server call", "=" + Data.SERVER_URL + "/acknowledge_manual_engagement");
            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engagement_id", Data.dEngagementId);
            Log.i("customer_id", Data.assignedCustomerInfo.userId);

            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/acknowledge_manual_engagement", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail acknowledge_manual_engagement", arg3.toString());
                            manualPatchPushAckAPI(activity);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.v("Server response acknowledge_manual_engagement", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {
                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                } else {
                                    if (jObj.has("flag")) {
                                        int flag = jObj.getInt("flag");
                                        if (ApiResponseFlags.ACTION_COMPLETE.getOrdinal() == flag) {

                                        }
                                    }
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                    });
        }

    }




    //TODO Stationing logic
    public void initializeStationDataProcedure() {
        if (myLocation != null) {
            dismissStationDataPopup();
            cancelStationPathUpdateTimer();
            if (checkDriverFree()) {
                fetchStationDataAPI(HomeActivity.this);
            }
        }
    }

    public void fetchStationDataAPI(final Activity activity) {
        if (AppStatus.getInstance(activity).isOnline(activity)) {
            RequestParams params = new RequestParams();
            params.put("access_token", Data.userData.accessToken);
            params.put("latitude", "" + myLocation.getLatitude());
            params.put("longitude", "" + myLocation.getLongitude());

            Log.e("get_nearest_station", "=");
            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("latitude", "" + myLocation.getLatitude());
            Log.i("longitude", "" + myLocation.getLongitude());

            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/get_nearest_station", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail get_nearest_station", arg3.toString());
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.i("Server response", "response = " + response);
                            try {
                                jObj = new JSONObject(response);
                                int flag = jObj.getInt("flag");
                                if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                    HomeActivity.logoutUser(activity);
                                    GCMIntentService.stopRing();
                                    GCMIntentService.clearNotifications(activity);
                                } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                    String errorMessage = jObj.getString("error");
                                    new DialogPopup().alertPopup(activity, "", errorMessage);
                                    GCMIntentService.stopRing();
                                    GCMIntentService.clearNotifications(activity);
                                } else if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                                    String message = jObj.getString("message");
                                    new DialogPopup().alertPopup(activity, "", message);
                                    GCMIntentService.stopRing();
                                    GCMIntentService.clearNotifications(activity);
                                }    else {
                                    new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                    GCMIntentService.stopRing();
                                    GCMIntentService.clearNotifications(activity);
                                }

                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                    });
        }
    }

    public boolean checkDriverFree() {
        return ( driverScreenMode == DriverScreenMode.D_INITIAL && Data.userData.isAvailable == 1 && Data.driverRideRequests.size() == 0);
    }


    Dialog stationDataDialog;

    public void dismissStationDataPopup() {
        try {
            if (stationDataDialog != null) {
                stationDataDialog.dismiss();
                stationDataDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    int stationPathUpdaterRunCount;
    Timer timerStationPathUpdater;
    TimerTask timerTaskStationPathUpdater;



    public void cancelStationPathUpdateTimer() {
        try {
            if (timerTaskStationPathUpdater != null) {
                timerTaskStationPathUpdater.cancel();
            }
            if (timerStationPathUpdater != null) {
                timerStationPathUpdater.cancel();
                timerStationPathUpdater.purge();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            timerTaskStationPathUpdater = null;
            timerStationPathUpdater = null;
            stationPathUpdaterRunCount = 1;
        }
        textViewDriverInfo.setVisibility(View.GONE);
    }

    Timer timerCheckPaymentStatus;
    TimerTask timerTaskCheckPaymentStatus;


    public void startCheckPaymentStatusTimer() {
        cancelCheckPaymentStatusTimer();
        try {
            timerCheckPaymentStatus = new Timer();

            timerTaskCheckPaymentStatus = new TimerTask() {


                @Override
                public void run() {
                    try {


                        if (HomeActivity.driverScreenMode == DriverScreenMode.D_RIDE_END) {


                            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                            nameValuePairs.add(new BasicNameValuePair("access_token", Data.userData.accessToken));
                            nameValuePairs.add(new BasicNameValuePair("engagement_id", Data.endRidesCustomerInfo.get(0).engagementId));
                            Log.i("nameValuePairs of request_ride", "=" + nameValuePairs);
                            String response = new HttpRequester().getJSONFromUrlParams(Data.SERVER_URL + "/check_payment_status", nameValuePairs);

                            Log.e("response of request_ride", "=" + response);

                            if (response.contains(HttpRequester.SERVER_TIMEOUT)) {
                                Log.e("timeout", "=");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//										new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                                    }
                                });
                            } else {
                                try {
                                    JSONObject jObj = new JSONObject(response);
                                    Log.i("jObj", "" + jObj.toString());
                                    if (!jObj.isNull("error")) {
                                        final String errorMessage = jObj.getString("error");
                                        if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                            cancelCheckPaymentStatusTimer();
                                            HomeActivity.logoutUser(activity);
                                        }
                                    } else {
                                        Data.endRidesCustomerInfo.clear();
                                        totalCashToCollect = 0;
                                        for (int i = 0; i < jObj.getJSONArray("users").length(); i++) {

                                            JSONObject customerObject = jObj.getJSONArray("users").getJSONObject(i);
//											HomeActivity.totalFare=HomeActivity.totalFare+customerObject.getDouble("to_pay");
                                            Data.endRidesCustomerInfo.add(new EndRideCustomerInfo(customerObject.getString("user_name"), customerObject.getString("user_image"), customerObject.getString("user_id"), customerObject.getString("engagement_id"), customerObject.getDouble("to_pay")
                                                    , customerObject.getInt("is_payment_successful"), customerObject.getInt("payment_method"), customerObject.getInt("defaulter_flag")));
                                            if (customerObject.getInt("payment_method") == 0)

                                            {
                                                totalCashToCollect = totalCashToCollect + customerObject.getDouble("to_pay");
                                            }

                                        }
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                for (int i = 0; i < Data.endRidesCustomerInfo.size(); i++) {
                                                    if (Data.endRidesCustomerInfo.get(i).isPaymentSucessfull == 0 && Data.endRidesCustomerInfo.get(i).defaulterFlag == 0) {
                                                        makeOkButtonEnable = false;
                                                        break;

                                                    } else {

                                                        makeOkButtonEnable = true;
                                                    }
                                                }
                                                if(FeaturesConfigFile.isCollectCashEnable)
                                                {
                                                    btnCollectCash.setBackgroundResource(R.drawable.main_btn_selector);
                                                    btnCollectCash.setTextColor(getResources().getColorStateList(R.drawable.white_color_selector));
                                                    btnCollectCash.setEnabled(true);
                                                    if (makeOkButtonEnable) {
                                                        btnCollectCash.setText(getString(R.string.ok));
                                                        rideOverText.setText("Please collect cash: " + getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US,"%.2f", totalCashToCollect)));
                                                        cancelCheckPaymentStatusTimer();
                                                    } else {

                                                        btnCollectCash.setText(getString(R.string.collect_cash));
                                                    }
                                                }
                                                else {
                                                    btnCollectCash.setText(getString(R.string.ok));
                                                    if (makeOkButtonEnable) {
                                                        rideOverText.setText("Please collect cash: " + getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US,"%.2f", totalCashToCollect)));
                                                        btnCollectCash.setBackgroundResource(R.drawable.main_btn_selector);
                                                        btnCollectCash.setTextColor(getResources().getColorStateList(R.drawable.white_color_selector));
                                                        btnCollectCash.setEnabled(true);
                                                        cancelCheckPaymentStatusTimer();
                                                    } else {

                                                        btnCollectCash.setBackgroundResource(R.drawable.disable_grey_btn);
                                                        btnCollectCash.setTextColor(getResources().getColorStateList(R.color.white));
                                                        btnCollectCash.setEnabled(false);
                                                    }
                                                }
                                                endRideCustomersListAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            timerCheckPaymentStatus.scheduleAtFixedRate(timerTaskCheckPaymentStatus, 100, 5000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelCheckPaymentStatusTimer() {
        try {
            if (timerTaskCheckPaymentStatus != null) {
                timerTaskCheckPaymentStatus.cancel();
                timerTaskCheckPaymentStatus = null;
            }

            if (timerCheckPaymentStatus != null) {
                timerCheckPaymentStatus.cancel();
                timerCheckPaymentStatus.purge();
                timerCheckPaymentStatus = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public void submitFeedbackToDriverAsync(final Activity activity, String givenRating, String feedbackText) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();

            params.put("access_token", Data.userData.accessToken);
            params.put("given_rating", givenRating);
            params.put("feedback", feedbackText);

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("given_rating", givenRating);
            Log.i("feedback", feedbackText);


            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/rate_the_customer", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail", arg3.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.i("Server response", "response = " + response);
                            try {
                                jObj = new JSONObject(response);
                                int flag = jObj.getInt("flag");
                                if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                    HomeActivity.logoutUser(activity);
                                } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                    String errorMessage = jObj.getString("error");
                                    new DialogPopup().alertPopup(activity, "", errorMessage);
                                } else if (ApiResponseFlags.ACTION_FAILED.getOrdinal() == flag) {
                                    String message = jObj.getString("log");
                                    new DialogPopup().alertPopup(activity, "", message);
                                } else if (ApiResponseFlags.ACTION_COMPLETE.getOrdinal() == flag) {

                                    belowLayoutRating.setVisibility(View.GONE);
//

                                    switchUserScreen();
                                    rateValue.setText("");
                                    Utils.hideSoftKeyboard(HomeActivity.this,rateValue);
                                    driverScreenMode = DriverScreenMode.D_INITIAL;
                                    switchDriverScreen(driverScreenMode);
                                    Toast.makeText(activity,getString(R.string.feedback_submitted), Toast.LENGTH_SHORT).show();
                                } else {
                                    new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }
                            DialogPopup.dismissLoadingDialog();
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }


    public void skipFeedbackForCustomerAsync(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            RequestParams params = new RequestParams();

            params.put("access_token", Data.userData.accessToken);
            //         params.put("engagement_id", engagementId);

            Log.i("access_token", "=" + Data.userData.accessToken);
            //      Log.i("engagement_id", engagementId);

            AsyncHttpClient client = Data.getClient();
            client.post(Data.SERVER_URL + "/skip_rating_by_driver", params,
                    new CustomAsyncHttpResponseHandler() {
                        private JSONObject jObj;

                        @Override
                        public void onFailure(Throwable arg3) {
                            Log.e("request fail", arg3.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void onSuccess(String response) {
                            Log.i("Server response", "response = " + response);
                            try {
                                jObj = new JSONObject(response);
                                int flag = jObj.getInt("flag");
                                if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                    HomeActivity.logoutUser(activity);
                                } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                    String errorMessage = jObj.getString("error");
                                    new DialogPopup().alertPopup(activity, "", errorMessage);
                                } else if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                                    String message = jObj.getString("message");
                                    new DialogPopup().alertPopup(activity, "", message);
                                } else if (ApiResponseFlags.ACTION_COMPLETE.getOrdinal() == flag) {
                                    belowLayoutRating.setVisibility(View.GONE);
                                    switchUserScreen();
                                    rateValue.setText("");
                                    Utils.hideSoftKeyboard(HomeActivity.this, rateValue);
                                    driverScreenMode = DriverScreenMode.D_INITIAL;
                                    switchDriverScreen(driverScreenMode);
                                } else {
                                    new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }
                            DialogPopup.dismissLoadingDialog();
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }
    void enableDisableRingerDialog(final Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.DIALOG_DIM_AMOUNT;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity));
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity));

            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

            textHead.setText("Enable/Disable Ringer");
            if(Prefs.with(getApplicationContext()).getString("IS_RING_ENABLE","1").equalsIgnoreCase("1"))
            {
                textMessage.setText("Ringer is on");

            }
            else
            {
                textMessage.setText("Ringer is off");

            }

            btnOk.setText("Start Ring");
            btnCancel.setText("Stop Ring");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    Prefs.with(getApplicationContext()).save("IS_RING_ENABLE", "1");
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    Prefs.with(getApplicationContext()).save("IS_RING_ENABLE", "0");
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void collectCashServerCall() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            DialogPopup.showLoadingDialog(this, getString(R.string.loading));
            Log.i("access_token", Data.userData.accessToken);
            Log.i("engagement_id", "=="+Data.endRidesCustomerInfo.get(0).engagementId);


            RestClient.getApiService().collectCashPayment(Data.userData.accessToken, Data.endRidesCustomerInfo.get(0).engagementId, new Callback<String>() {
                @Override
                public void success(String responseString, Response response) {
                    Log.v("Server response", "response = " + responseString);

                    try {
                        JSONObject jObj = new JSONObject(responseString);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                HomeActivity.logoutUser(HomeActivity.this);

                            } else {
                                new DialogPopup().alertPopup(
                                        HomeActivity.this, "", errorMessage);

                            }

                        } else {
                            new DialogPopup().alertPopup(
                                    HomeActivity.this, "", jObj
                                            .getString("message"));
                            belowLayoutRating.setVisibility(View.VISIBLE);
                            startAutomaticReviewHandler();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(HomeActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(HomeActivity.this, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });


        } else {
            new DialogPopup().alertPopup(HomeActivity.this, "",
                    getString(R.string.check_internet_message));
        }
    }


    public Thread autoCompleteThread;
    public boolean refreshingAutoComplete = false;

    public synchronized void getSearchResults(final String searchText, final LatLng latLng) {
        try {
            if (!refreshingAutoComplete) {

                searchLocationProgress.setVisibility(View.VISIBLE);
                if (autoCompleteThread != null) {
                    autoCompleteThread.interrupt();
                }

                autoCompleteThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        refreshingAutoComplete = true;
//                        autoCompleteSearchResults.clear();
                        autoCompleteSearchResults.addAll(MapUtils.getAutoCompleteSearchResultsFromGooglePlaces(searchText, latLng,HomeActivity.this));
                        Log.i("setSearchResultsToList", "==");
                        refreshingAutoComplete = false;
                        recallLatestTextSearch(searchText, latLng);
                        setSearchResultsToList();
                        autoCompleteThread = null;


                    }
                });
                autoCompleteThread.start();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     *
     */
    public synchronized void setSearchResultsToList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                searchLocationProgress.setVisibility(View.GONE);
                if (etLocation.getText().toString().equalsIgnoreCase("")) {
                    autoCompleteSearchResults.clear();
                }
                if (autoCompleteSearchResults.size() == 0) {
                    autoCompleteSearchResults.add(new AutoCompleteSearchResult("No results found", "", ""));
                }

                searchListAdapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * @param searchText
     * @param latLng
     */
    public synchronized void recallLatestTextSearch(final String searchText, final LatLng latLng) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String currentText = etLocation.getText().toString().trim();
                    if (searchText.equalsIgnoreCase(currentText)) {

                    } else {

                        if (currentText.length() > 0) {
                            if (map != null) {
                                autoCompleteSearchResults.clear();
                                searchListAdapter.notifyDataSetChanged();
                                Log.i("currentText", "==" + currentText);
                                getSearchResults(currentText, latLng);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * @param placeId
     */
    public synchronized void getSearchResultFromPlaceId(final String placeId) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                SearchResult searchResult = MapUtils.getSearchResultsFromPlaceIdGooglePlaces(placeId,HomeActivity.this);

                Log.e("in getSearchResult",""+searchResult);
                setSearchResultToMapAndText(searchResult);
            }
        }).start();
    }

    /**
     * @param searchResult
     */
    public synchronized void setSearchResultToMapAndText(final SearchResult searchResult) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (map != null && searchResult != null) {
                        canChangeLocationText = false;
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(searchResult.latLng, 14), 1000, null);

                        Log.e("destinationLatitude mein mach gyi", "" + searchResult.latLng.latitude);
                    Log.e("destinationLongitude mein mach gyi", "" + searchResult.latLng.longitude);

                        Prefs.with(getApplicationContext()).save("destinationLatitude", searchResult.latLng.latitude);
                        Prefs.with(getApplicationContext()).save("destinationLongitude", searchResult.latLng.longitude);
                        markerOptionsCustomerPickupLocation = null;
                        Data.dDestinationLatLng = searchResult.latLng;
                        startDestinationPathUpdateTimer();

                    }                }

        });
    }

        class ViewHolderSearchItem {
        TextView textViewSearchName, textViewSearchAddress;
        LinearLayout relative;
        int id;
    }

    class SearchListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderSearchItem holder;

        public SearchListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return autoCompleteSearchResults.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderSearchItem();
                convertView = mInflater.inflate(R.layout.list_item_search_item, null);

                holder.textViewSearchName = (TextView) convertView.findViewById(R.id.textViewSearchName);
                holder.textViewSearchName.setTypeface(Data.getFont(HomeActivity.this));
                holder.textViewSearchAddress = (TextView) convertView.findViewById(R.id.textViewSearchAddress);
                holder.textViewSearchAddress.setTypeface(Data.getFont(HomeActivity.this));
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);

                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderSearchItem) convertView.getTag();
            }


            holder.id = position;

            try {
                holder.textViewSearchName.setText(autoCompleteSearchResults.get(position).name);
                holder.textViewSearchAddress.setText(autoCompleteSearchResults.get(position).address);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.relative.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder = (ViewHolderSearchItem) v.getTag();
                    Utils.hideSoftKeyboard(HomeActivity.this, etLocation);
                    AutoCompleteSearchResult autoCompleteSearchResult = autoCompleteSearchResults.get(holder.id);
                    if (!"".equalsIgnoreCase(autoCompleteSearchResult.placeId)) {
                            canChangeLocationText = false;
                            Prefs.with(getApplicationContext()).save("destinationAddress", autoCompleteSearchResult.name);
                            searchEtDestination.setText(autoCompleteSearchResult.name);

                        getSearchResultFromPlaceId(autoCompleteSearchResult.placeId);

                        searchStart = true;
                        driverScreenMode = DriverScreenMode.D_IN_RIDE;
                        switchDriverScreen(driverScreenMode);
                    }
                }
            });
            return convertView;
        }

        @Override
        public void notifyDataSetChanged() {
            try {

                if (autoCompleteSearchResults.size() > 1) {
                    if (autoCompleteSearchResults.contains(new AutoCompleteSearchResult("No results found", "", ""))) {
                        autoCompleteSearchResults.remove(autoCompleteSearchResults.indexOf(new AutoCompleteSearchResult("No results found", "", "")));
                    }
                }
                super.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

}